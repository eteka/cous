<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliberationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliberations', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->text('fichiers');
            $table->string('annee_academique');
            $table->string('faculte');
            $table->tinyInteger('annee');
            $table->string('type');
            $table->boolean('etat')->default(FALSE);
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deliberations');
    }
}
