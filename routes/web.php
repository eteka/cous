<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});
Route::get('/clear', function () {
  \Artisan::call('key:generate');
  \Artisan::call('config:clear');
  \Artisan::call('config:cache');
  \Artisan::call('view:clear');
  return 0;
});

\Carbon\Carbon::setLocale(config('app.locale'));

Route::get('/presentation/{slug}.html', array('as' => 'pageContentPresentation', 'uses' => 'NavigationController@getPageContent'));
Route::get('/services/{slug}.html', array('as' => 'pageContentServices', 'uses' => 'NavigationController@getPageContent'));

Route::get('/personnel/', 'NavigationController@getPersonnels');
Route::get('/objectifs-et-missions', 'NavigationController@getPageContent');
Route::get('/objectifs-et-missions', 'NavigationController@getPageContent');
Route::get('/publications-et-ressources/', 'NavigationController@getPageContent');
Route::get('/contact/', 'NavigationController@getContact');
Route::get('/seminaires/', 'NavigationController@getPageContent');
Route::get('/activites/', 'NavigationController@getActivites');
Route::get('/activite/{slug}', 'NavigationController@getActivite');
Route::get('/deliberations/', 'NavigationController@getDeliberations');
Route::get('/publications/', 'NavigationController@getPublications');
Route::get('/galerie/', 'NavigationController@getGalerie');
Route::get('/documents-administratifs/', 'NavigationController@getDossiers');
Route::get('/dossiers/', 'NavigationController@getDossiers');
Route::get('/contact/', 'NavigationController@getContact');
Route::get('/communiques/', 'NavigationController@getCommuniques');
Route::get('/communique/{slug}', 'NavigationController@getCommunique');
Auth::routes();
//search
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/search', 'NavigationController@search')->name('search');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/go', 'HomeController@index')->middleware('auth');
Route::group(['prefix' => '/', 'middleware' => ['auth']], function () {
    Route::get('admin', 'Admin\AdminController@index');
    Route::get('admin/give-role-permissions', 'Admin\AdminController@getGiveRolePermissions');
    Route::post('admin/give-role-permissions', 'Admin\AdminController@postGiveRolePermissions');
    Route::resource('admin/roles', 'Admin\RolesController');
    Route::resource('admin/permissions', 'Admin\PermissionsController');
    Route::resource('admin/users', 'Admin\UsersController');
    Route::get('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
    Route::post('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);
    ##################################################################################################################
    Route::resource('admin/page', 'Admin\PageController');
    Route::resource('admin/titre-page', 'Admin\TitrePageController');
    Route::resource('admin/personnel', 'Admin\PersonnelController');
    Route::resource('admin/activite', 'Admin\ActiviteController');
    Route::resource('admin/categories', 'Admin\CategorieController');
    Route::resource('admin/publications', 'Admin\PublicationsController');
    Route::resource('admin/slides', 'Admin\SlidesController');
    Route::resource('admin/etudes', 'Admin\EtudesController');
    Route::resource('admin/galerie', 'Admin\GalerieController');
    Route::resource('admin/deliberation', 'Admin\DeliberationController');
    Route::resource('admin/communique', 'Admin\CommuniqueController');
    Route::resource('admin/dossier', 'Admin\DossierController');
});



Route::resource('admin/faculte', 'Admin\\FaculteController');