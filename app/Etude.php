<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etude extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'etudes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['titre', 'photo', 'legende', 'detail', 'user_id'];

    
}
