<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activite extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'activites';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['titre', 'slug', 'chapeau', 'photo', 'legend', 'categorie_id', 'corps', 'vues', 'etat', 'user_id'];

    
}
