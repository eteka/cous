<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galerie extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'galeries';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['images', 'titre', 'description', 'user_id', 'etat'];

    
}
