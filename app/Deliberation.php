<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deliberation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'deliberations';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom', 'annee_academique', 'faculte', 'annee', 'type', 'etat','info','fichier', 'user_id'];

    public function faculte(){
        return $this->belongsTo('App\Faculte','faculte');
    }
}
