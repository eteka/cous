<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
                    'nom' => 'required|string|min:4|max:100',
                    'prenom' => 'required|string|min:4|max:100',
                    'telephone' => 'required|string|min:8|max:30',
                    'prenom' => 'required|string|min:4|max:255',
                    'sexe' => 'required|in:H,F',
                    'telephone' => 'required|min:8|max:30',
                    'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
        return User::create([
                    'nom' => $data['nom'],
                    'prenom' => $data['prenom'],
                    'username' => str_slug($data['prenom'] . ' ' . $data['nom'], "."),
                    'sexe' => $data['sexe'],
                    'telephone' => $data['telephone'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
        ]);
    }

}
