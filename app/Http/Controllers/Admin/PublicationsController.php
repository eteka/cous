<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Publication;
use Illuminate\Http\Request;
use Session;
use Auth;
class PublicationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $publications = Publication::where('titre', 'LIKE', "%$keyword%")
				->orWhere('auteur', 'LIKE', "%$keyword%")
				->orWhere('references', 'LIKE', "%$keyword%")
				->orWhere('annee', 'LIKE', "%$keyword%")
				->orWhere('user_id', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $publications = Publication::paginate($perPage);
        }

        return view('admin.publications.index', compact('publications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.publications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'auteur' => 'required',
			'annee' => 'required'
		]);
        $requestData = $request->all();
        $requestData['user_id']=0;
        if(Auth::user()){
            $requestData['user_id']=Auth::user()->id;
        }
        Publication::create($requestData);

        Session::flash('flash_message', 'Publication ajoutée!');

        return redirect('admin/publications');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $publication = Publication::findOrFail($id);

        return view('admin.publications.show', compact('publication'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $publication = Publication::findOrFail($id);

        return view('admin.publications.edit', compact('publication'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'auteur' => 'required',
			'annee' => 'required'
		]);
        $requestData = $request->all();
        
        $publication = Publication::findOrFail($id);
        $publication->update($requestData);

        Session::flash('flash_message', 'Publication updated!');

        return redirect('admin/publications');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Publication::destroy($id);

        Session::flash('flash_message', 'Publication deleted!');

        return redirect('admin/publications');
    }
}
