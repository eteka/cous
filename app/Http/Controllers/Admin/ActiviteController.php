<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Activite;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\Categorie;
class ActiviteController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 10;

//        $id = Auth::user()->id;

        if (!empty($keyword)) {

            if (1) {//Auth::user()->hasRole('admin')
                $activite = Activite::where('titre', 'LIKE', "%$keyword%")
                        ->orWhere('chapeau', 'LIKE', "%$keyword%")
                        ->orWhere('photo', 'LIKE', "%$keyword%")
                        ->orWhere('categorie_id', 'LIKE', "%$keyword%")
                        ->orWhere('corps', 'LIKE', "%$keyword%")
                        ->orWhere('vues', 'LIKE', "%$keyword%")
                        ->paginate($perPage);
            }
          //  dd('zss');
//            elseif (Auth::user()->hasRole('article')) {
//                 $activite = Article::where('id', $id)
//                        ->Where(function($query) use ($keyword) {
//                            $query->orWhere('chapeau', 'LIKE', "%$keyword%")
//                            ->orWhere('titre', 'LIKE', "%$keyword%")
//                            ->orWhere('photo', 'LIKE', "%$keyword%")
//                            ->orWhere('categorie_id', 'LIKE', "%$keyword%")
//                            ->orWhere('corps', 'LIKE', "%$keyword%")
//                            ->orWhere('vues', 'LIKE', "%$keyword%");
//                        })
//                        ->paginate($perPage);
//            }
        } else {

            //  if (Auth::user()->hasRole('admin')) {

            $activite = Activite::select('titre', 'slug', 'chapeau', 'photo', 'legend', 'categorie_id', 'corps', 'vues', 'etat', 'user_id')->paginate($perPage);
            // } else {
//                $activite = Article::where('user_id', '=', $id)->paginate($perPage);
            //}
        }

        return view('admin.activite.index', compact('activite'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        $categories = Categorie::all()->pluck('titre', 'id');

        $categories[0] = '-Sélectionnez-';
        return view('admin.activite.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
//        if (!(Auth::user()->hasRole('admin') || Auth::user()->hasRole('article'))) {
//            Session::flash('flash_message', 'Opération interdite!');
//            redirect()->back();
//        }
        $this->validate($request, [
            'titre' => 'required|min:10',
            'categorie_id' => 'required|integer',
            'corps' => 'required|min:100',
            'categorie_id' => 'required|min:0'
        ]);

        $requestData = $request->all();

        if ($request->hasFile('photo')) {
            $url = '/uploads/activites/';
            $uploadPath = public_path($url);
            if (is_dir($url)) {
                mkdir($url, 0777, true);
            }

            $extension = $request->file('photo')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('photo')->move($uploadPath, $fileName);
            $requestData['photo'] = $url . $fileName;
        }
//        if (!Auth::check()) {
//            abort(404);
//        }
        $requestData['user_id'] = 0;
        $requestData['etat'] = '1';
        if ($requestData['user_id'] > 0) {//Auth::user()->hasRole('admin') &&
            $requestData['user_id'] = $requestData['user_id'];
        } else {
            if (Auth::user()) {
                $requestData['user_id'] = Auth::user()->id;
            }
        }

//        dd($requestData);
        $requestData['slug'] = str_slug($requestData['titre']);
        Activite::create($requestData);


        Session::flash('flash_message', 'Activité ajoutée avec succès !');

        return redirect('admin/activite');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $activite = Activite::findOrFail($id);

        return view('admin.activite.show', compact('activite'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $activite = Activite::findOrFail($id);

        return view('admin.activite.edit', compact('activite'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request) {
        $this->validate($request, [
            'titre' => 'required',
//            'slug' => 'required',
//            'categorie_id' => 'required'
        ]);
        $requestData = $request->all();

        $activite = Activite::findOrFail($id);
        $activite->update($requestData);

        Session::flash('flash_message', 'Activité ajoutée avec succès !');

        return redirect('admin/activite');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Activite::destroy($id);

        Session::flash('flash_message', 'Activité Supprimée avec succès!');

        return redirect('admin/activite');
    }

}
