<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Faculte;
use Illuminate\Http\Request;
use Session;

class FaculteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $faculte = Faculte::where('nom_fac', 'LIKE', "%$keyword%")
				->orWhere('sigle', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $faculte = Faculte::paginate($perPage);
        }

        return view('admin.faculte.index', compact('faculte'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.faculte.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'nom_fac' => 'required',
			'sigle' => 'required'
		]);
        $requestData = $request->all();
        
        Faculte::create($requestData);

        Session::flash('flash_message', 'Faculte added!');

        return redirect('admin/faculte');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $faculte = Faculte::findOrFail($id);

        return view('admin.faculte.show', compact('faculte'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $faculte = Faculte::findOrFail($id);

        return view('admin.faculte.edit', compact('faculte'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'nom_fac' => 'required',
			'sigle' => 'required'
		]);
        $requestData = $request->all();
        
        $faculte = Faculte::findOrFail($id);
        $faculte->update($requestData);

        Session::flash('flash_message', 'Faculte updated!');

        return redirect('admin/faculte');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Faculte::destroy($id);

        Session::flash('flash_message', 'Faculte deleted!');

        return redirect('admin/faculte');
    }
}
