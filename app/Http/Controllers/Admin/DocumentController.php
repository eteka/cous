<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Document;
use Illuminate\Http\Request;
use Session;
use Auth;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $document = Document::where('titre', 'LIKE', "%$keyword%")
				->orWhere('numero', 'LIKE', "%$keyword%")
				->orWhere('date_emission', 'LIKE', "%$keyword%")
				->orWhere('objet', 'LIKE', "%$keyword%")
				->orWhere('type_doc', 'LIKE', "%$keyword%")
				->orWhere('url', 'LIKE', "%$keyword%")
				->orWhere('detail', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $document = Document::paginate($perPage);
        }

        return view('admin.document.index', compact('document'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.document.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required'
		]);
        $requestData = $request->all();
        
         if ($request->hasFile('url')) {
            $url = '/uploads/' . config('app.dossiers.document');
            $uploadPath = public_path($url);
            if (is_dir($url)) {
                mkdir($url, 0777, true);
            }

            $extension = $request->file('url')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('url')->move($uploadPath, $fileName);
            $requestData['url'] = $url . $fileName;
        }
        if (!Auth::check()) {
         abort(404);  
        }
         $requestData['user_id'] = Auth::user()->id;
        
        Document::create($requestData);

        Session::flash('flash_message', 'Document added!');

        return redirect('admin/document');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $document = Document::findOrFail($id);

        return view('admin.document.show', compact('document'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $document = Document::findOrFail($id);

        return view('admin.document.edit', compact('document'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'titre' => 'required'
		]);
        $requestData = $request->all();
        
        $document = Document::findOrFail($id);
        
         if ($request->hasFile('url')) {
            $url = '/uploads/' . config('app.dossiers.document');
            $uploadPath = public_path($url);
            if (is_dir($url)) {
                mkdir($url, 0777, true);
            }

            $extension = $request->file('url')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('url')->move($uploadPath, $fileName);
            $requestData['url'] = $url . $fileName;
        }
        $document->update($requestData);

        Session::flash('flash_message', 'Document updated!');

        return redirect('admin/document');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Document::destroy($id);

        Session::flash('flash_message', 'Document deleted!');

        return redirect('admin/document');
    }
}
