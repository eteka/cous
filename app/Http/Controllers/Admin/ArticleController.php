<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article;
use Illuminate\Http\Request;
use Session;
use App\CategorieArticle;
use Auth;
use Charts;

class ArticleController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 10;

        $id = Auth::user()->id;
        
        if (!empty($keyword)) {

            if (Auth::user()->hasRole('admin')) {
                $article = Article::where('titre', 'LIKE', "%$keyword%")
                        ->orWhere('chapeau', 'LIKE', "%$keyword%")
                        ->orWhere('photo', 'LIKE', "%$keyword%")
                        ->orWhere('categorie_id', 'LIKE', "%$keyword%")
                        ->orWhere('corps', 'LIKE', "%$keyword%")
                        ->orWhere('vues', 'LIKE', "%$keyword%")
                        ->paginate($perPage);
            } elseif (Auth::user()->hasRole('article')) {
                $article = Article::where('id', $id)
                        ->Where(function($query) use ($keyword) {
                            $query->orWhere('chapeau', 'LIKE', "%$keyword%")
                            ->orWhere('titre', 'LIKE', "%$keyword%")
                            ->orWhere('photo', 'LIKE', "%$keyword%")
                            ->orWhere('categorie_id', 'LIKE', "%$keyword%")
                            ->orWhere('corps', 'LIKE', "%$keyword%")
                            ->orWhere('vues', 'LIKE', "%$keyword%");
                        })
                        ->paginate($perPage);
            }
        } else {
            
            if (Auth::user()->hasRole('admin')) {
                $article = Article::select('titre', 'chapeau', 'vues', 'slug', 'etat', 'id')->paginate($perPage);
            }else{
                $article = Article::where('user_id','=', $id)->paginate($perPage);
            }
        }
        return view('admin.article.index', compact('article'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        $categories = CategorieArticle::all()->pluck('titre', 'id');

        $categories[0] = '-Sélectionnez-';
        return view('admin.article.create', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
//    public function validate($id) {
////        $article = Article::findOrFail($id);
////        $categories = CategorieArticle::all()->pluck('titre', 'id');
////        $categories[0] = '-Sélectionnez-';
////        return view('admin.article.edit', compact('article', 'categories'));
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        if (!(Auth::user()->hasRole('admin') || Auth::user()->hasRole('article'))) {
            Session::flash('flash_message', 'Opération interdite!');
            redirect()->back();
        }
        $this->validate($request, [
            'titre' => 'required|min:10',
            'categorie_id' => 'required|integer',
            'corps' => 'required|min:100',
            'categorie_id' => 'required|min:0'
        ]);

        $requestData = $request->all();

        if ($request->hasFile('photo')) {
            $url = '/uploads/' . config('app.dossiers.article');
            $uploadPath = public_path($url);
            if (is_dir($url)) {
                mkdir($url, 0777, true);
            }

            $extension = $request->file('photo')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('photo')->move($uploadPath, $fileName);
            $requestData['photo'] = $url . $fileName;
        }
        if (!Auth::check()) {
            abort(404);
        }


        if (Auth::user()->hasRole('admin') && $requestData['user_id'] > 0) {
            $requestData['user_id'] = $requestData['user_id'];
        } else {
            $requestData['user_id'] = Auth::user()->id;
        }

//        dd($requestData);
        $requestData['slug'] = str_slug($requestData['titre']);
        Article::create($requestData);

        Session::flash('flash_message', 'Article ajouté !');

        return redirect('admin/article');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $article = Article::findOrFail($id);

        return view('admin.article.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $article = Article::findOrFail($id);
        $categories = CategorieArticle::all()->pluck('titre', 'id');
        $categories[0] = '-Sélectionnez-';
        return view('admin.article.edit', compact('article', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request) {

        if (!(Auth::user()->hasRole('admin') || Auth::user()->hasRole('article'))) {
            Session::flash('flash_message', 'Opération interdite!');
            redirect()->back();
        }
        $this->validate($request, [
            'titre' => 'required|min:10',
            'categorie_id' => 'required|integer',
            'corps' => 'required|min:100',
            'categorie_id' => 'required|min:0'
        ]);

        $requestData = $request->all();
//        $requestData['categorie'] = '0';

        if ($request->hasFile('photo')) {
            $url = '/uploads/' . config('app.dossiers.article');
            $uploadPath = public_path($url);

            if (is_dir($url)) {
                mkdir($url, 0777, true);
            }
            $extension = $request->file('photo')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('photo')->move($uploadPath, $fileName);
            $requestData['photo'] = $url . $fileName;
        }

        if (Auth::check()) {
            if (Auth::user()->hasRole('admin') && $requestData['user_id'] > 0) {
                $requestData['user_id'] = $requestData['user_id'];
            } else {
                $requestData['user_id'] = Auth::user()->id;
            }
        }
        $requestData['slug'] = str_slug($requestData['titre']);
        $article = Article::findOrFail($id);
        $article->update($requestData);

        Session::flash('flash_message', 'Article mise à jour!');

        return redirect('admin/article');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        if (!(Auth::user()->hasRole('admin') || Auth::user()->hasRole('delete_article'))) {
            Session::flash('flash_message', 'Opération interdite!');
            redirect()->back();
        }
        Article::destroy($id);
        Session::flash('flash_message', 'Article supprimé!');

        return redirect('admin/article');
    }

}
