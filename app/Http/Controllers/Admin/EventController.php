<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Event;
use Illuminate\Http\Request;
use Session;
use Auth;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $event = Event::where('titre', 'LIKE', "%$keyword%")
				->orWhere('priorite', 'LIKE', "%$keyword%")
				->orWhere('date_event', 'LIKE', "%$keyword%")
				->orWhere('date_limit', 'LIKE', "%$keyword%")
				->orWhere('detail', 'LIKE', "%$keyword%")
				->orWhere('auteur', 'LIKE', "%$keyword%")
//				->orWhere('user_id', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $event = Event::paginate($perPage);
        }

        return view('admin.event.index', compact('event'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.event.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'date_event' => 'required',
			'date_limit' => 'required',
			'lieu' => 'required'
		]);
        $requestData = $request->all();
        $requestData['slug']=str_slug($requestData['titre']);
        if (Auth::check()) {
            $requestData['user_id'] = Auth::user()->id;
        }else{
            abort(404);
        }
        Event::create($requestData);

        Session::flash('flash_message', 'Event added!');

        return redirect('admin/event');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $event = Event::findOrFail($id);

        return view('admin.event.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);

        return view('admin.event.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'date_event' => 'required',
			'date_limit' => 'required',
//			'user_id' => 'required'
		]);
        $requestData = $request->all();
        if (Auth::check()) {
            $requestData['user_id'] = Auth::user()->id;
        }
        $requestData['slug']=str_slug($requestData['titre']);
//        if (Auth::check()) {
//            $requestData['user_id'] = Auth::user()->id;
//        }
        $event = Event::findOrFail($id);
        $event->update($requestData);

        Session::flash('flash_message', 'Event updated!');

        return redirect('admin/event');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Event::destroy($id);

        Session::flash('flash_message', 'Event deleted!');

        return redirect('admin/event');
    }
}
