<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Deliberation;
use Illuminate\Http\Request;
use Session;
use Auth;

class DeliberationController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $deliberation = Deliberation::where('nom', 'LIKE', "%$keyword%")
                    ->orWhere('annee_academique', 'LIKE', "%$keyword%")
                    ->orWhere('faculte', 'LIKE', "%$keyword%")
                    ->orWhere('annee', 'LIKE', "%$keyword%")
                    ->orWhere('type', 'LIKE', "%$keyword%")
                    ->orWhere('etat', 'LIKE', "%$keyword%")
                    ->orWhere('user_id', 'LIKE', "%$keyword%")
                    ->paginate($perPage);
        } else {
            $deliberation = Deliberation::paginate($perPage);
        }

        return view('admin.deliberation.index', compact('deliberation'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.deliberation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $this->validate($request, [
            'nom' => 'required|min:5|max:250',
            'annee_academique' => 'required|max:10',
            'faculte' => 'required',
            'annee' => 'required',
            'type' => 'max:800',
            'info' => 'max:800',
            'fichier' => "required|max:10000", //|mimes:pdf
        ]);
        $requestData = $request->except('_token');

        if ($request->hasFile('fichier')) {
            $url = '/uploads/deliberations/';
            $uploadPath = public_path($url);
            if (is_dir($url)) {
                @mkdir($url, 0777, true);
            }

            $extension = $request->file('fichier')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('fichier')->move($uploadPath, $fileName);
            $requestData['fichier'] = $url . $fileName;
        }
        if (Auth::user()) {
            $id = Auth::user()->id;
            $requestData['user_id'] = $id;
//            dd($requestData);
            Deliberation::create($requestData);
            Session::flash('flash_message', 'Délibération ajoutée!');
        }
        return redirect('admin/deliberation');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $deliberation = Deliberation::findOrFail($id);

        return view('admin.deliberation.show', compact('deliberation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $deliberation = Deliberation::findOrFail($id);

        return view('admin.deliberation.edit', compact('deliberation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request) {
        $this->validate($request, [
            'nom' => 'required|min:5|max:250',
            'annee_academique' => 'required|max:10',
            'faculte' => 'required|min:1',
            'annee' => 'required|min:1',
            'type' => 'max:800',
            'info' => 'max:800',
//            'fichier' => "required|max:10000", //|mimes:pdf
        ]);
        $requestData = $request->except('_token');

        if ($request->hasFile('fichier')) {
            $url = '/uploads/deliberations/';
            $uploadPath = public_path($url);
            if (is_dir($url)) {
                @mkdir($url, 0777, true);
            }

            $extension = $request->file('fichier')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('fichier')->move($uploadPath, $fileName);
            $requestData['fichier'] = $url . $fileName;
        }
        if (Auth::user()) {
            $uid = Auth::user()->id;
            $requestData['user_id'] = $uid;
//            dd($requestData);
            Deliberation::create($requestData);
            $deliberation = Deliberation::findOrFail($id);
            $deliberation->update($requestData);

            Session::flash('flash_message', 'Délibération mis à jour!');
        }
        return redirect('admin/deliberation');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Deliberation::destroy($id);

        Session::flash('flash_message', 'Délibération supprimée!');

        return redirect('admin/deliberation');
    }

}
