<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Categorie;
use Illuminate\Http\Request;
use Session;
use Auth;

class CategorieController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 15;
//        $id = Auth::user()->id;
        if (!empty($keyword)) {
//            if (Auth::user()->hasRole('admin')) {
                $categories = Categorie::where('titre', 'LIKE', "%$keyword%")
                        ->orWhere('description', 'LIKE', "%$keyword%")
                        ->orWhere('icone', 'LIKE', "%$keyword%")
                        ->orWhere('slug', 'LIKE', "%$keyword%")
                        ->paginate($perPage);
//            } elseif (Auth::user()->hasRole('categorie')) {
//                $categories = Categorie::where('id', $id)
//                ->Where(function($query) use ($keyword) {
//                $query->where('titre', 'LIKE', "%$keyword%")
//                        ->orWhere('description', 'LIKE', "%$keyword%")
//                        ->orWhere('icone', 'LIKE', "%$keyword%")
//                        ->orWhere('slug', 'LIKE', "%$keyword%");
//                })
//                ->paginate($perPage);
//            }
        } else {
            
            $categories = Categorie::paginate($perPage);
        }

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $this->validate($request, [
            'titre' => 'required|min:2|max:220',
            'icone' => 'image'
        ]);

        $requestData = $request->all();
        if ($request->hasFile('icone')) {
            $url = '/uploads/categoriess/' ;
            $uploadPath = public_path($url);

            $extension = $request->file('icone')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('icone')->move($uploadPath, $fileName);
            $requestData['icone'] = $url . $fileName;
        }
        $requestData['slug'] = str_slug($requestData['titre']);
        if (Auth::check()) {
            $requestData['user_id'] = Auth::user()->id;
        }
        Categorie::create($requestData);

        Session::flash('flash_message', 'Categorie added!');

        return redirect('admin/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $category = Categorie::findOrFail($id);

        return view('admin.categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $category = Categorie::findOrFail($id);

        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request) {
        $this->validate($request, [
            'titre' => 'required|min:2|max:220',
            'icone' => 'image'
        ]);
        $requestData = $request->all();

        $requestData['slug'] = str_slug($requestData['titre']);

        $categories = Categorie::findOrFail($id);
        if ($request->hasFile('icone')) {
            $url = '/uploads/' . config('app.dossiers.categorie_article');
            $uploadPath = public_path($url);

            $extension = $request->file('icone')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('icone')->move($uploadPath, $fileName);
            $requestData['icone'] = $url . $fileName;
        }
        $categories->update($requestData);


        Session::flash('flash_message', 'Categorie updated!');

        return redirect('admin/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Categorie::destroy($id);

        Session::flash('flash_message', 'Categorie deleted!');

        return redirect('admin/categories');
    }

}
