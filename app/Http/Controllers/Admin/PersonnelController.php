<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Personnel;
use Illuminate\Http\Request;
use Session;

class PersonnelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 15;

        if (!empty($keyword)) {
            $personnel = Personnel::where('nom_prenom', 'LIKE', "%$keyword%")
				->orWhere('fonction', 'LIKE', "%$keyword%")
				->orWhere('grade', 'LIKE', "%$keyword%")
				->orWhere('biographie', 'LIKE', "%$keyword%")
				->orWhere('adresse', 'LIKE', "%$keyword%")
				->orWhere('photo', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $personnel = Personnel::paginate($perPage);
        }

        return view('admin.personnel.index', compact('personnel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.personnel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'nom_prenom' => 'required',
//			'grade' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('photo')) {
            $url = '/uploads/activites/';
            $uploadPath = public_path($url);
            if (is_dir($url)) {
                mkdir($url, 0777, true);
            }

            $extension = $request->file('photo')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('photo')->move($uploadPath, $fileName);
            $requestData['photo'] = $url . $fileName;
        }
        Personnel::create($requestData);

        Session::flash('flash_message', 'Personnel ajouté !');

        return redirect('admin/personnel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $personnel = Personnel::findOrFail($id);

        return view('admin.personnel.show', compact('personnel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $personnel = Personnel::findOrFail($id);

        return view('admin.personnel.edit', compact('personnel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'nom_prenom' => 'required',
//			'grade' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('photo')) {
            $url = '/uploads/activites/';
            $uploadPath = public_path($url);
            if (is_dir($url)) {
                mkdir($url, 0777, true);
            }

            $extension = $request->file('photo')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('photo')->move($uploadPath, $fileName);
            $requestData['photo'] = $url . $fileName;
        }
        $personnel = Personnel::findOrFail($id);
        $personnel->update($requestData);

        Session::flash('flash_message', 'Personnel mis à jour avec succès!');

        return redirect('admin/personnel');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Personnel::destroy($id);

        Session::flash('flash_message', 'Personnel supprimé!');

        return redirect('admin/personnel');
    }
}
