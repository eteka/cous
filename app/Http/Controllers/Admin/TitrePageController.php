<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\TitrePage;
use Illuminate\Http\Request;
use Session;
use Auth;

class TitrePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $titrepage = TitrePage::where('titre', 'LIKE', "%$keyword%")
				->orWhere('slug', 'LIKE', "%$keyword%")
				->orWhere('user_id', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $titrepage = TitrePage::paginate($perPage);
        }

        return view('admin.titre-page.index', compact('titrepage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.titre-page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
		]);
        $requestData = $request->all();
        
        $requestData['slug']= str_slug($requestData['titre']);
        if(Auth::user()){
            $requestData['user_id']=Auth::user()->id;
        }
        TitrePage::create($requestData);

        Session::flash('flash_message', 'Page ajoutée!');

        return redirect('admin/titre-page');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $titrepage = TitrePage::findOrFail($id);

        return view('admin.titre-page.show', compact('titrepage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $titrepage = TitrePage::findOrFail($id);

        return view('admin.titre-page.edit', compact('titrepage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
		]);
        $requestData = $request->all();
        
        $requestData['slug']= str_slug($requestData['titre']);
        if(Auth::user()){
            $requestData['user_id']=Auth::user()->id;
        }
        $titrepage = TitrePage::findOrFail($id);
        $titrepage->update($requestData);

        Session::flash('flash_message', 'Page mise à jour avec succès !');

        return redirect('admin/titre-page');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        TitrePage::destroy($id);

        Session::flash('flash_message', 'Page supprimée !');

        return redirect('admin/titre-page');
    }
}
