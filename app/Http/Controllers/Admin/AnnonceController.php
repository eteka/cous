<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Annonce;
use Illuminate\Http\Request;
use Session;
use Auth;

class AnnonceController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $annonce = Annonce::where('titre', 'LIKE', "%$keyword%")
                    ->orWhere('sous_titre', 'LIKE', "%$keyword%")
                    ->orWhere('detail', 'LIKE', "%$keyword%")
                    ->orWhere('date_limit', 'LIKE', "%$keyword%")
                    ->orWhere('etat', 'LIKE', "%$keyword%")
                    ->orWhere('priorite', 'LIKE', "%$keyword%")
                    ->paginate($perPage);
        } else {
            $annonce = Annonce::paginate($perPage);
        }

        return view('admin.annonce.index', compact('annonce'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.annonce.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $this->validate($request, [
            'titre' => 'required'
        ]);
        $requestData = $request->all();
         $requestData['slug'] = str_slug($requestData['titre']);
         if (Auth::check()) {
            $requestData['user_id'] = Auth::user()->id;
        }
        Annonce::create($requestData);

        Session::flash('flash_message', 'Annonce added!');

        return redirect('admin/annonce');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $annonce = Annonce::findOrFail($id);

        return view('admin.annonce.show', compact('annonce'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $annonce = Annonce::findOrFail($id);

        return view('admin.annonce.edit', compact('annonce'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request) {
        $this->validate($request, [
            'titre' => 'required'
        ]);
        $requestData = $request->all();
        $requestData['slug'] = str_slug($requestData['titre']);
        
        $annonce = Annonce::findOrFail($id);
        $annonce->update($requestData);

        Session::flash('flash_message', 'Annonce updated!');

        return redirect('admin/annonce');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Annonce::destroy($id);

        Session::flash('flash_message', 'Annonce deleted!');

        return redirect('admin/annonce');
    }

}
