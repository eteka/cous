<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Dossier;
use Illuminate\Http\Request;
use Session;
use Auth;

class DossierController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $dossier = Dossier::where('titre', 'LIKE', "%$keyword%")
                    ->orWhere('slug', 'LIKE', "%$keyword%")
                    ->orWhere('references', 'LIKE', "%$keyword%")
                    ->orWhere('fichiers', 'LIKE', "%$keyword%")
                    ->paginate($perPage);
        } else {
            $dossier = Dossier::paginate($perPage);
        }

        return view('admin.dossier.index', compact('dossier'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.dossier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $this->validate($request, [
            'titre' => 'required',
            'references' => 'required'
        ]);

        $requestData = $request->all();
//        dd($requestData);
        if ($request->hasFile('fichiers')) {
            $url = '/uploads/dossiers/' . config('app.dossiers.article');
            $uploadPath = public_path($url);
            if (is_dir($url)) {
                mkdir($url, 0777, true);
            }
            
            $extension = $request->file('fichiers')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('fichiers')->move($uploadPath, $fileName);
            $requestData['fichiers'] = $url . $fileName;
        }
        $requestData['slug']= str_slug($requestData['titre'] ).'_'.rand(11111, 99999);
        if (!Auth::check()) {
            abort(404);
        }


//        if (Auth::user()->hasRole('admin') && $requestData['user_id'] > 0) {
//            $requestData['user_id'] = $requestData['user_id'];
//        } else {
//            $requestData['user_id'] = Auth::user()->id;
//        }
        Dossier::create($requestData);

        Session::flash('flash_message', 'Dossier ajouté!');

        return redirect('admin/dossier');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $dossier = Dossier::findOrFail($id);

        return view('admin.dossier.show', compact('dossier'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $dossier = Dossier::findOrFail($id);

        return view('admin.dossier.edit', compact('dossier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request) {
        $this->validate($request, [
            'titre' => 'required'
        ]);
        $requestData = $request->all();

        $dossier = Dossier::findOrFail($id);
        $dossier->update($requestData);

        Session::flash('flash_message', 'Dossier updated!');

        return redirect('admin/dossier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Dossier::destroy($id);

        Session::flash('flash_message', 'Dossier deleted!');

        return redirect('admin/dossier');
    }

}
