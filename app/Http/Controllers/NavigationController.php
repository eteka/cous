<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Personnel;
use \App\Publication;
use App\Activite;
use \App\Deliberation;

class NavigationController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
//        Session::flash('flash_message', 'Opération éffectuée avec succès . Merci!');
//        $nbslide = intval(config('app.param.nb_slide')) > 2 ? config('app.param.nb_slide') : 3;
//        $slides = Slide::where('etat', 1)->orderBy('created_at', 'DESC')->paginate($nbslide);
//        // dd($slides);
//        $actus = $mot = '';
//        return view('web.index', compact('actus', 'mot', 'slides'));
    }

    public function getDeliberations(Request $request) {
        $q = $request->input('query');
        $annee_acad = $request->input('annee_acad');
        $fac = $request->input('faculte');
        $type = $request->input('type');
        $annee = $request->input('annee');
        $ietabs = \App\Faculte::pluck('sigle', 'id');
        $itypes = Deliberation::where('etat', '1')->where('type', '!=', '0')->pluck('type', 'type');
//         $ianne= Deliberation::where('etat','1')->where('type','!=','0')->pluck('annee','annee');
        $iannee_acad = Deliberation::where('etat', '1')->where('type', '!=', '0')->pluck('annee_academique', 'annee_academique');

        if (empty($q)) {
            $datas = Deliberation::latest()->where('etat', '1')->paginate(20);
        } else {
            $datas = Deliberation::
//                            ->orWhere('annee_academique', 'LIKE', "%$annee_acad%")
                            Where('faculte', 'LIKE', "%$fac%")
                            ->orWhere('annee', 'LIKE', "%$annee%")
                            ->orWhere('type', 'LIKE', "%$type%")
                            ->Where('nom', 'LIKE', "%$q%")
                            ->where('etat', '1')->paginate(6);
//            dd("lss");
        }
        return view('web.deliberations', compact('datas', 'ietabs', 'itypes', 'iannee_acad'));
    }

    public function getPageContent($slug) {
//        dd($slug);
        $page = Page::where('slug', strtolower($slug))->first();
        if ($page == NULL) {
            return abort(404);
        }
        return view('web.content', compact('page'));
    }

    public function getPersonnels() {

        $personnels = Personnel::orderBy('nom_prenom', 'DESC')->paginate(10);

        return view('web.personnels', compact('personnels'));
    }

    public function search(Request $request) {
        $keyword = $request->get('query');
        $perPage = 10;

        $activites = NULL;

        if (!empty($keyword)) {


            $activites = Activite::where('id', 'LIKE', "%$keyword%")
                    ->orWhere('chapeau', 'LIKE', "%$keyword%")
                    ->orWhere('photo', 'LIKE', "%$keyword%")
                    ->orWhere('legend', 'LIKE', "%$keyword%")
                    ->orWhere('corps', 'LIKE', "%$keyword%")
                    ->paginate($perPage);
        }
//        dd($article);
        return view('web.search', compact('activites'));
    }

    public function getPublications() {

        $publications = Publication::orderBy('created_at', 'DESC')->paginate(8);
        return view('web.publications', compact('publications'));
    }

    public function getContact() {
        $page = Page::where('slug', 'contact')->first();
        return view('web.contact', compact('page'));
    }

    public function getDossiers() {
        $dossiers = \App\Dossier::latest()->paginate(20);
        return view('web.dossiers', compact('dossiers'));
    }

    public function getActivites() {
        $activites = Activite::orderBy('created_at', 'DESC')->paginate(8);
        return view('web.activites', compact('activites'));
    }

    public function getActivite($slug) {
        $actu = Activite::where('slug', $slug)->first();
        if (empty($actu)) {
            abort(404);
        }
        return view('web.activite', compact('actu'));
    }

    function getCommunique($slug) {
        $data = \App\Communique::where('slug', $slug)->first();
        if (empty($data)) {
            abort(404);
        }
        return view('web.communique', compact('data'));
    }

    function getCommuniques() {
        $datas = \App\Communique::latest()->paginate(8);
        return view('web.communiques', compact('datas'));
    }

    function getPage() {
        return view('web.page');
    }

    private function SaveSearch($data, $n) {
        $data['nb_result'] = $n;
//        if(!Search::created($data))            echo 'Désolé';
        try {
            Search::create($data);
        } catch (Exception $ex) {
            dd($ex);
        }
    }

    function getSearch(Request $request) {
        $type = strtolower($request->input('type', 'all'));

        $q = strtolower($request->input('q'));
        $uid = (int) (Auth::check()) ? Auth::user()->id : '0';
        $infos = (serialize($request->getLanguages()));
        $info = ('{ip:' . $request->getClientIp() . ',' . $request->getDefaultLocale() . ',info:' . $infos . '}');
        $session = serialize($request->session()->all());

        if ($request->session()->has('nbsearch')) {
            $n_search = intval(session('nbsearch')) + 1;
            $request->session()->put('nbsearch', $n_search);
        } else {
            $n_search = 1;
            $request->session()->put('nbsearch', $n_search);
        }
        $rech = [
            'ip' => $request->ip(),
            'text' => $q,
            'type' => $type,
            'info' => $info,
            'session' => $session,
            'nb_result' => 0,
            'count' => $type,
            'user_id' => $uid,
            'count' => $n_search
        ];
        if ($type == 'activite') {
            $activites = $this->searchActivite($q, 10, TRUE);
            $this->SaveSearch($rech, $activites->count());
            return view('web.search', compact('activites'));
        } elseif ($type == 'document') {
            $documents = $this->searchDocument($q, 10, TRUE);
            $this->SaveSearch($rech, $documents->count());
            return view('web.search', compact('documents'));
        } elseif ($type == 'annonce') {
            $annonces = $this->searchAnnonce($q, 10, TRUE);
            $this->SaveSearch($rech, $annonces->count());
            return view('web.search', compact('annonces'));
        } elseif ($type == 'event') {
            $events = $this->searchEvent($q, 10, TRUE);
            $this->SaveSearch($rech, $events->count());
            return view('web.search', compact('events'));
        } elseif ($type == 'autre') {
            $pages = $this->searchPage($q, 10, TRUE);
            $this->SaveSearch($rech, $pages->count());
            return view('web.search', compact('pages'));
        } else {

            $pages = $this->searchPage($q, 3, TRUE);
            $activites = $this->searchActivite($q, 3, TRUE);
            $annonces = $this->searchAnnonce($q, 3, TRUE);
            $events = $this->searchEvent($q, 3, TRUE);
            $documents = $this->searchDocument($q, 10, TRUE);
//            $this->SaveSearch($rech, "{page:".$pages->count().',activite:'.$activites->count().',annonce:'.$annonces->count().',event:'.$events->count().',document:'.$documents->count().'}');
            return view('web.search', compact('events', 'annonces', 'documents', 'activites', 'pages'));
        }
    }

    private function searchActivite($keyword, $nb = 0, $paginate = TRUE) {
        $data = null;
        if (!empty($keyword)) {
            $query = Article::where('titre', 'LIKE', "%$keyword%")
                            ->orWhere('chapeau', 'LIKE', "%$keyword%")
                            ->orWhere('photo', 'LIKE', "%$keyword%")
                            ->orWhere('categorie_id', 'LIKE', "%$keyword%")
                            ->orWhere('corps', 'LIKE', "%$keyword%")->orderBy('created_at', 'DESC');
            if ($paginate == TRUE && $nb > 0) {
                $data = $query->paginate($nb);
            } elseif ($paginate == FALSE && $nb > 0) {
                $data = $query->paginate($nb);
            } else {
                $data = $query->paginate(10);
            }
        }
        return $data;
    }

    private function searchDocument($keyword, $nb = 0, $paginate = TRUE) {
        $data = null;
        if (!empty($keyword)) {
            $query = Document::where('titre', 'LIKE', "%$keyword%")
                    ->orWhere('numero', 'LIKE', "%$keyword%")
                    ->orWhere('date_emission', 'LIKE', "%$keyword%")
                    ->orWhere('objet', 'LIKE', "%$keyword%")
                    ->orWhere('type_doc', 'LIKE', "%$keyword%")
                    ->orWhere('url', 'LIKE', "%$keyword%")
                    ->orWhere('detail', 'LIKE', "%$keyword%")
                    ->orderBy('created_at', 'DESC');
            if ($paginate == TRUE && $nb > 0) {
                $data = $query->paginate($nb);
            } elseif ($paginate == FALSE && $nb > 0) {
                $data = $query->paginate($nb);
            } else {
                $data = $query->paginate(10);
            }
        }
        return $data;
    }

    private function searchPage($keyword, $nb = 0, $paginate = TRUE) {
        $data = null;
        if (!empty($keyword)) {
            $query = Page::where('titre', 'LIKE', "%$keyword%")
                    ->orWhere('slug', 'LIKE', "%$keyword%")
                    ->orWhere('contenu', 'LIKE', "%$keyword%")
                    ->orderBy('created_at', 'DESC');
            if ($paginate == TRUE && $nb > 0) {
                $data = $query->paginate($nb);
            } elseif ($paginate == FALSE && $nb > 0) {
                $data = $query->paginate($nb);
            } else {
                $data = $query->paginate(10);
            }
        }
        return $data;
    }

    private function searchAnnonce($keyword, $nb = 0, $paginate = TRUE) {
        $data = null;
        if (!empty($keyword)) {
            $query = Annonce::where('titre', 'LIKE', "%$keyword%")
                    ->orWhere('sous_titre', 'LIKE', "%$keyword%")
                    ->orWhere('detail', 'LIKE', "%$keyword%")
                    ->orWhere('date_limit', 'LIKE', "%$keyword%");
            if ($paginate == TRUE && $nb > 0) {
                $data = $query->paginate($nb);
            } elseif ($paginate == FALSE && $nb > 0) {
                $data = $query->paginate($nb);
            } else {
                $data = $query->paginate(10);
            }
        }
        return $data;
    }

    private function searchEvent($keyword, $nb = 0, $paginate = TRUE) {
        $data = null;
        if (!empty($keyword)) {
            $query = Event::where('titre', 'LIKE', "%$keyword%")
                    ->orWhere('priorite', 'LIKE', "%$keyword%")
                    ->orWhere('date_event', 'LIKE', "%$keyword%")
                    ->orWhere('date_limit', 'LIKE', "%$keyword%")
                    ->orWhere('detail', 'LIKE', "%$keyword%")
                    ->orWhere('Auteur', 'LIKE', "%$keyword%");
            if ($paginate == TRUE && $nb > 0) {
                $data = $query->paginate($nb);
            } elseif ($paginate == FALSE && $nb > 0) {
                $data = $query->paginate($nb);
            } else {
                $data = $query->paginate(10);
            }
            $data = $query->paginate(10);
        }
        return $data;
    }

    function getFormation($slug) {
        return view('web.page');
    }

    function getProgramme($slug) {
        return view('web.page');
    }

    function getEmploi() {
        return view('web.emploi');
    }

    function readArticle($slug) {

        $actu = Article::where('slug', $slug)->where('etat', '1')->first();


        if (!$actu) {
            abort(404);
        }

        $blogKey = 'arts_' . $actu->id;
        if (!Session::has($blogKey)) {
            if (intval($actu->vues) == 0) {
                $dd = Article::where('id', $actu->id)->update([
                    'vues' => 1]);
            } else {
                $dd = Article::where('id', $actu->id)->update([
                    'vues' => DB::raw('vues + 1')]);
            }
            Session::put($blogKey, 1);
//            dd($dd);
        }

        $sugestions = Article::select('titre', 'chapeau', 'photo', 'legend', 'categorie_id', 'corps', 'vues', 'user_id', 'slug', 'etat')->where('id', '!=', $actu->id)->where('etat', '1')->orderBy('created_at')->orderBy('vues')->take(rand(3, 12))->get();
        $next = Article::select('titre', 'slug')->where('id', '>', $actu->id)->where('etat', '1')->first();
        $prev = Article::select('titre', 'slug')->where('id', '<', $actu->id)->where('etat', '1')->first();

        return view('web.activite', compact('actu', 'sugestions', 'prev', 'next'));
    }

    function getAnnonce($slug) {
        $annonce = Annonce::where('slug', $slug)->first();
        if (!$annonce) {
            abort(404);
        }
        $blogKey = 'annonce_' . $annonce->id;
        if (!Session::has($blogKey)) {
            Session::put($blogKey, 1);
            $dd = Annonce::where('id', $annonce->id)->update([
                'vues' => DB::raw('vues + 1')]);
        }
        return view('web.annonce', compact('annonce', 'actu'));
    }

    function getAnnonces() {
        $annonces = Annonce::select('titre', 'sous_titre', 'date_limit', 'etat', 'priorite', 'slug', 'user_id')->where('etat', '1')->paginate(10);

        return view('web.annonces', compact('annonces'));
    }

    public function postNewsletter(Request $request) {
        $validator = Validator::make($request->all(), [
                    'email' => 'required|email|unique:newsletters',
        ]);
        if ($validator->fails()) {
            Session::flash('flash_message', 'Votre email n\'est pas valide ou existe déja dans notre base de données . Réessayer !');
        } else {
            $data = $request->all();
            \App\Newsletter::created($data);
            Session::flash('flash_message', 'Opération éffectuée avec succès. Votre email a été recus, restez connectez pour plus d\'informations. Merci !');
        }
        return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
    }

    public function postAcceptcookies(Request $request) {
        $request->session()->put('ok_cookies', 1);
        //  Session::flash('flash_message', 'Opération éffectuée avec succès . Merci !');
        return redirect()->back();
    }

    public function getEvent($slug) {
        $event = Event::where('slug', $slug)->first();
        return view('web.event', compact('event'));
    }

    public function getEvents() {
        $events = Event::select('titre', 'priorite', 'slug', 'lieu', 'date_event', 'date_limit', 'detail', 'created_at', 'auteur', 'user_id')->paginate(10);
        return view('web.events', compact('events'));
    }

    function getDocuments() {
        $nb = config('pagination.nb_annoncelist_web') > 0 ? config('pagination.nb_annoncelist_web') : 10;
        $documents = Document::select('titre', 'numero', 'date_emission', 'objet', 'type_doc', 'url')->paginate($nb);
        return view('web.documents', compact('documents'));
    }

    function getGalerie() {
        $nb = 10;
        $images = \App\Galerie::where('etat', TRUE)->paginate($nb);
        return view('web.galerie', compact('images'));
    }

}
