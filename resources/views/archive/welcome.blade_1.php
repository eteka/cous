<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <link rel="icon" href="favicone.ico" type="image/x-icon" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title',"LERF | Laboratoire d'Etudes et de Recherches Forestières")</title>
        <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/css/bootstrap.css')}}">

        <link rel="stylesheet" href="{{asset('assets/css/template-cms.odace.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
        <!-- Styles -->
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <script src="{{asset('assets/js/jquery.min.js')}}"   ></script>

        <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"  ></script>
        <script src="{{asset('assets/plugins/pjax/jquery.pjax.js')}}"></script>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <!--        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
                <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">-->
        <!-- Styles -->
        <style>
            html, body {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
                /*background-color: #F7F8FB;*/
                height: 100%;
                -webkit-font-smoothing: antialiased;
                font-family:'Roboto', 'Open Sans', serif;
            }
            .pad0{padding: 0!important}
            body{
                background: url('{{asset("assets/images/app/body_bg2.jpg")}}') #fff  fixed;
                background-size: auto 100% 
            }
            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .container{max-width: 1045px}
            #top-img{
                min-height: 80px;
                height: 160px;
                background: url('{{asset("assets/images/app/baniere_LERF.png")}}') #333 0 50% no-repeat;
                background-size: 100% auto;
            }
            .logo{max-height: 100px;margin-top: 10%;}  
            #main {
                min-height: 650px;
                background: #fff;
                /*margin-bottom: 20px;*/

            }
            .baniere{width: 100%}
            #menu_mega > li:first-child {
                border-left: 1px solid #227393;
                border-left: 1px solid rgba(0, 0, 0, 0.3);
            }
            .navbar-inverse .navbar-nav > li > a:hover , .navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:hover, .navbar-inverse .navbar-nav > .active > a:focus{
                background-color: #13ef5c;
                /*padding-top: 20px;*/
            }
            #menu_mega > li {
                border-right: 1px solid #227393;
                border-right: 1px solid rgba(0, 0, 0, 0.3);
            }
            .navbar-inverse{

                line-height: 100%;
                /*                border-radius: 6px 6px 0 0;
                                -webkit-border-radius: 6px 6px 0 0;
                                -moz-border-radius: 6px 6px 0 0;*/
                box-shadow: 2px 2px 6px #666666;
                -webkit-box-shadow: 2px 2px 6px #666666;
                -moz-box-shadow: 2px 2px 6px #666666;
                /*                background: #8B8B8B;
                                background: linear-gradient(top,  #D6D6D6,  #7A7A7A);
                                background: -ms-linear-gradient(top,  #D6D6D6,  #7A7A7A);
                                background: -webkit-gradient(linear, left top, left bottom, from(#D6D6D6), to(#7A7A7A));
                                background: -moz-linear-gradient(top,  #D6D6D6,  #7A7A7A);*/
                border: dashed 0px #6D6D6D;
                /*position:relative;*/
                /*z-index:999;*/
                margin: 0 auto;
                background: #2d2d2d;
                box-shadow: inset -1px -5px 0px -1px #393939;
                min-height: 52px;
                border-bottom: 5px solid #13EF5C;

            }
            #footer a{color: #c0c0c0}
            #footer{
                padding: 15px 0 10px;
                font-size: 12px;
                background: #2d2d2d;
                color: #9c9c9c;
            }
            .bBotp{
                /*border-bottom:  1px solid #227393;*/
                border-bottom: 5px solid #13EF5C;
            }
            #inner-wrapper{
/*                box-shadow: 0 0 4px #aaaaaa;
                background: #ffffff;
                border: 1px solid #ddd
                position: relative;
                background: #FFF;
                width: 1045px;
                margin: 0 auto;*/
                /*                -webkit-box-shadow: 0 0 3px #CACACA;
                                -moz-box-shadow: 0 0 3px #cacaca;
                                box-shadow: 0 0 3px #CACACA;*/

            }
            #menu_mega ul::after{
                position: absolute;
                right: 7px;
                top: 50%;
                display: inline-block;
                content: '';
                width: 0;
                height: 0;
                border: 4px solid transparent;
                border-top-width: 4px;
                border-top-style: solid;
                border-top-color: transparent;
                border-top: 4px solid #bbb;
                border-top-color: rgb(187, 187, 187);

            }
            #menu_mega .dropdown ul a{color: #999;}
            #menu_mega .dropdown ul{
                border-top: 2px solid #13EF5C;
                border-top-color: #13EF5C;
                background: #2d2d2d;
                border-radius: 0;
                min-width: 215px;
                -webkit-box-shadow: 0 3px 4px 1px rgba(0, 0, 0, 0.2);
                box-shadow: 0 3px 4px 1px rgba(0, 0, 0, 0.2);

            }
            #top-link{border-top: 2px solid #2d2d2d;padding: 2px 0}
            #top-link .list-inline{
                padding: 2px 0 0;
                margin-bottom: 2px;


            }
            .mini-rs {
                height: 25px;
                border-radius: 100%;
            }
            #top-link{ 
                background: #ffffff;
                background: #fbfbfb;
                border-bottom: 1px solid #ececec;
                font-size: 13px;
            }
            .stylish-input-group {
                /*margin: 5px 0;*/

            }
            .stylish-input-group .input-group-addon{
                background: white !important;
                border-radius: 0;
                border-color:#eee;
            }
            .stylish-input-group .form-control{
                border-right:0; 
                box-shadow:0 0 0; 
                border-color:#eee;
                border-radius: 0;
            }
            .stylish-input-group button{
                border-radius: 0;
                border-color:#eee;
                border:0;
                background:transparent;
            }
            .stylish-input-group button{font-size: 12px}
            .pheader{
                font-weight: 300;
                text-transform: uppercase;
                color: #0ec523;
                font-size: 29px;
                border-bottom: 1px solid #ddd;
                padding-bottom: 10px
            }
            .actu_title{
                /*font-family:'Open Sans Light','Open Sans',"Segoe UI", 'Roboto Slab','Open Sans',Arial;*/
                font-size: 20px;
                font-weight: bold;
                font-weight: 100;
                margin:10px 0 5px ;
                font-size: 1.28571em;
                padding-top: 5px;
                color: forestgreen;

            }
            a:hover{color: #eb212e}
            a {
                -webkit-transition: color .3s ease;
                -o-transition: color .3s ease;
                transition: color .3s ease;
                color: #33475c;
                color: #4caf50;
                text-decoration: none;
            }
            .actu_line:hover{
                /*box-shadow: 0 2px 4px #ddd inset;*/
                transition: all ease .2s;
            }
            .actu_cover{

            }
            .actu_line{
                transition: all ease .2s;
                border: 1px solid #eee;
                padding: 10px 15px ;
                border-left: 0px;

                border-right: 0px;               
                border-top: 0px;
                /*box-shadow: 0 0 5px #eee*/
                /*height: 240px;*/

            }
            .Rpanel{background: #fefefe;
                    background: #f9f9f9;
                    box-shadow: 5px 1px 4px 0px #ddd inset;;
                    padding: 0px !important; 
                    background: red;
            }
            .hentete{
                font-size: 25px;
                line-height: 1.25;
                color: #333;
                color: #333;
                display: block;
                margin-top: 20px;
                padding: 0px 10px;
                text-transform: uppercase;
                /*border-left: 5px solid #13ef5c;*/ 
                border-left: 5px solid #4caf50;
                /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#b7e2c9+0,ffffff+100&1+0,0+100 */
                background: -moz-linear-gradient(top, rgb(216, 222, 217) 0%, rgba(255,255,255,0) 100%); /* FF3.6-15 */
                background: -webkit-linear-gradient(top, rgb(216, 222, 217) 0%,rgba(255,255,255,0) 100%); /* Chrome10-25,Safari5.1-6 */
                background: linear-gradient(to bottom, rgb(216, 222, 217) 0%,rgba(255,255,255,0) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d8ded9', endColorstr='#00ffffff',GradientType=0 ); /* IE6-9 */
            }
            .lcard{
                padding: 10px 15px;
                /*                border: 1px solid #ddd;
                                border-left: 4px solid #ddd;
                                border-right: 0px solid #ddd;*/
            }
            .scard{
                /*border: 1px solid #ddd;*/
                margin-top: 10px;
                margin-right: 5px;
                /*box-shadow: 0 0 5px #eee;*/
                padding-top: 15px !important;
                border-bottom: 1px solid #ddd
            }
            .scard .form-control{
                background: #fbfbfb;
                border-radius: 0;
                box-shadow: none;
            }
            .hentete h3{
                padding: 5px;margin: 0;font-size: 18px;
                font-weight: 900;
                font-family: 'Roboto Slab','Open Sans',Arial;
                /*min-height: 56px;*/
                color: #62655f;

            }
            .text-sm.text-muted.clearfix{
                clear: both;
                font-size: 15px;
            }
            .service-block-v4{
                padding: 15px  20px;
                margin: 30px 0;
                background: #f5f8f9
            }
            .service-desc .icone{
                font-size: 80px;
                display: block;
                text-align: center;
            }
            .service-desc h3{font-weight: bold}
        </style>
    </head>
    <body>
        <div id="inner-wrappers" class="container-fluid">
            <div class="">
                <div class="row" id="top-link">
                    <div class="col-sm-8" >
                        <ul class="list-inline">
                            <li><a href="#"><img class="mini-rs" src="{{asset("assets/images/rs/facebook.png")}}" alt="Facebook"></a></li>
                            <li><a href="#"><img class="mini-rs" src="{{asset("assets/images/rs/youtube.png")}}" alt="YouTube"></a></li>
                            <li><a href="#"><img class="mini-rs" src="{{asset("assets/images/rs/twitter.png")}}" alt="Twitter"></a></li>

                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <div id="imaginary_containers"> 
                            <div class="input-group stylish-input-group">
                                <input type="text" class="form-control input-sm"  placeholder="Rechercher" >
                                <span class="input-group-addon">
                                    <button type="submit">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>  
                                </span>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            <header>
                <div id="header" >
                    <div class="row">
                        <div class="col-sm-12"  id="top-img">


                            <div class="row">
                                <div class="col-sm">
                                    <!--<img class="baniere" src="{{asset("assets/images/app/baniere_LERF.png")}}">-->
                                </div>
                                <!--                            <div class="col-sm-2">
                                                                 </div>
                                                            <div class="col-sm-2 col-sm-offset-8 text-right">
                                                                <img  class="logo" src="{{asset('assets/images/app/logo_UP.png')}}">
                                                            </div>-->
                            </div>
                        </div>
                        <div class="col-sm-12 pad0">
                            <nav class="navbar-lefts _ navbar-inverse ">
                                <div class="">
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <!--<a class="navbar-brand" href="#">Bootstrap theme</a>-->
                                    </div>
                                    <div id="navbar" class="navbar-collapse collapse">
                                        <ul id="menu_mega" class="nav navbar-nav">
                                            <li class="active"><a href="#">Accueil</a></li>
                                            <li><a href="#about">Objectifs et missions</a></li>
                                            <li><a href="#contact">Personnels</a></li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Formations et Recherches <span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Unités de recherche</a></li>
                                                    <li><a href="#">Séminiares</a></li>                                                    
                                                    <li><a href="#">Activités de recherche</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#contact">Publications et Ressources</a></li>
                                            <li><a href="#contact">Contacts</a></li>
                                        </ul>
                                    </div><!--/.nav-collapse -->
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
            <div id="main" >
                <div class="row">
                    <div class="col-sm-12 pad0">
                        <!-- Add this css File in head tag-->
                        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" media="all">
                        <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet" media="all">


                        <!--  
                               If you want to change #bootstrap-touch-slider id then you have to change Carousel-indicators and Carousel-Control  #bootstrap-touch-slider slide as well
                               Slide effect: slide, fade
                               Text Align: slide_style_center, slide_style_left, slide_style_right
                               Add Text Animation: https://daneden.github.io/animate.css/
                        -->


                        <div id="bootstrap-touch-slider" class="carousel bs-slider fade  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="5000" >

                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#bootstrap-touch-slider" data-slide-to="0" class="active"></li>
                                <li data-target="#bootstrap-touch-slider" data-slide-to="1"></li>
                                <li data-target="#bootstrap-touch-slider" data-slide-to="2"></li>
                            </ol>

                            <!-- Wrapper For Slides -->
                            <div class="carousel-inner" role="listbox">

                                <!-- Third Slide -->
                                <div class="item active">

                                    <!-- Slide Background -->
                                    <img src="{{asset("assets/images/slides/LERF_PARAKOU_SLIDE2.png")}}"  alt="Bootstrap Touch Slider"  class="slide-image"/>
                                    <div class="bs-slider-overlay"></div>

                                    <div class="container">
                                        <div class="row">
                                            <!-- Slide Text Layer -->
                                            <div class="slide-text slide_style_left">
                                                <h1 data-animation="animated zoomInRight">Bootstrap Carousel</h1>
                                                <p data-animation="animated fadeInLeft">Bootstrap carousel now touch enable slide.</p>
                                                <a href="http://bootstrapthemes.co/" target="_blank" class="btn btn-default" data-animation="animated fadeInLeft">select one</a>
                                                <a href="http://bootstrapthemes.co/" target="_blank"  class="btn btn-primary" data-animation="animated fadeInRight">select two</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End of Slide -->

                                <!-- Second Slide -->
                                <div class="item">

                                    <!-- Slide Background -->
                                    <img src="{{asset("assets/images/slides/LERF_PARAKOU_SLIDE3.png")}}"  alt="Bootstrap Touch Slider"  class="slide-image"/>
                                    <div class="bs-slider-overlay"></div>
                                    <!-- Slide Text Layer -->
                                    <div class="slide-text slide_style_center">
                                        <h1 data-animation="animated flipInX">Bootstrap touch slider</h1>
                                        <p data-animation="animated lightSpeedIn">Make Bootstrap Better together.</p>
                                        <a href="http://bootstrapthemes.co/" target="_blank" class="btn btn-default" data-animation="animated fadeInUp">select one</a>
                                        <a href="http://bootstrapthemes.co/" target="_blank"  class="btn btn-primary" data-animation="animated fadeInDown">select two</a>
                                    </div>
                                </div>
                                <!-- End of Slide -->

                                <!-- Third Slide -->
                                <div class="item">

                                    <!-- Slide Background -->
                                    <img src="{{asset("assets/images/slides/LERF_PARAKOU_SLIDE4.png")}}" alt="LERF Slider"  class="slide-image"/>
                                    <div class="bs-slider-overlay"></div>
                                    <!-- Slide Text Layer -->
                                    <div class="slide-text slide_style_right">
                                        <h1 data-animation="animated zoomInLeft">Beautiful Animations</h1>
                                        <p data-animation="animated fadeInRight">Lots of css3 Animations to make slide beautiful .</p>
                                        <a href="http://bootstrapthemes.co/" target="_blank" class="btn btn-default" data-animation="animated fadeInLeft">select one</a>
                                        <a href="http://bootstrapthemes.co/" target="_blank" class="btn btn-primary" data-animation="animated fadeInRight">select two</a>
                                    </div>
                                </div>
                                <!-- End of Slide -->


                            </div><!-- End of Wrapper For Slides -->

                            <!-- Left Control -->
                            <a class="left carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="prev">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>

                            <!-- Right Control -->
                            <a class="right carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="next">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>

                        </div> <!-- End  bootstrap-touch-slider Slider -->

                    </div>
                    @yield('content')
                    <div class="col-sm-3 pad0 Rpanel">
                        @include ('partials.right')
                    </div>
                </div>
                <div class="col-sm-12">
                </div>
            </div>
            <div class="row">
                <div id="footer" class="row " >
                    <div class=" ">
                        <div class="col-sm-7 bBotp ">
                            <ul class="list-inline  text-uppercase">
                                <li>
                                    <a href="#">Objectifs et missions </a>
                                </li>
                                <li><a href="#">Unités de recherche</a></li>
                                <li><a href="#">Séminiares</a></li>
                                <li><a href="#">Activités de recherche</a></li>
                            </ul>

                        </div>
                        <div class="col-sm-5 bBotp text-right">
                            <ul class="list-inline ">
                                <li>
                                    <a href="mailto:">Email: </a>
                                </li>
                                <li>|</li>
                                <li>
                                    <a href="mailto:">Tél: </a>
                                </li>
                                <li>|</li>
                                <li>
                                    Réseaux sociaux  
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-12 text-xs text-center">
                            <br>
                            &copy; LERF-UP 2017, Tous droits réservés

                        </div>
                    </div>
                </div>
            </div>
    </body>
</html>
