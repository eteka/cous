<?php
$reseautage_slug = 'contribution-a-la-formation-des-ressources-humaines';
$collaboration_slug = 'collaboration-et-reseautage';
$recherche_slug = 'collaboration-et-reseautage';
$mot = App\Page::where('slug', 'mot-du-directeur')->first();
$recherche = App\Page::where('slug', $recherche_slug)->first();
$collaboration = App\Page::where('slug', $collaboration_slug)->first();
$reseautage = App\Page::where('slug', $reseautage_slug)->first();
$slides = \App\Slide::orderBy('ordre')->take(5)->get();
$activites = \App\Activite::orderBy('created_at', 'DESC')->take('4')->get();
$mat = App\Page::where("slug", 'attributions-et-missions')->first();
?>
<style>
.bs-slider > .carousel-inner > .item > img, .bs-slider > .carousel-inner > .item > a > img{
    margin-top: -30px !important;
}
</style>
<div class="container__">
    <div class="row_">
        <div class=" "> 
            <div class="pad0_15_">
                @if(isset($slides))
                <div id="bootstrap-touch-slider" class="carousel bs-slider fade  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="5000" >

                    <!-- Indicators -->

                    <ol class="carousel-indicators">
                        <?php $i = 0; ?>
                        @foreach($slides as $s)
                        @if($s->photo!='')
                        {{$i++==0?'active':$i}}
                        <li data-target="#bootstrap-touch-slider" data-slide-to="0" class="{{$i++==0?'active':''}}"></li>
                        @endif
                        @endforeach
                    </ol>


                    <!-- Wrapper For Slides -->
                    <div class="carousel-inner" role="listbox">

                        <?php $cmpt = 0; ?>
                        @foreach($slides as $s)
                        <!-- Third Slide -->
                        @if($s->photo!='')
                        <div class="item {{$cmpt++==1?'active':''}}">
                            <!-- Slide Background -->
                            <img src="{{asset($s->photo)}}"  alt="Bootstrap Touch Slider"  class="slide-image"/>
                            <div class="bs-slider-overlay"></div>

                            <div class="container">
                                <div class="row">
                                    <!-- Slide Text Layer -->
                                    <div class="slide-text slide-text container text-center slide_style_left_">
                                        <h1 data-animation="animated zoomInRight">{{$s->titre}}</h1>
                                        <p data-animation="animated fadeInLeft">{{$s->sous_titre}}</p>
                                        <!--                                <a href="http://bootstrapthemes.co/" target="_blank" class="btn btn-default" data-animation="animated fadeInLeft">select one</a>
                                            <a href="http://bootstrapthemes.co/" target="_blank"  class="btn btn-primary" data-animation="animated fadeInRight">select two</a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach
                            <!-- End of Slide -->

                            <!-- End of Slide -->


                        </div><!-- End of Wrapper For Slides -->

                        <!-- Left Control -->
                        <a class="left carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="prev">
                            <span class="fa fa-angle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>

                        <!-- Right Control -->
                        <a class="right carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="next">
                            <span class="fa fa-angle-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div> <!-- End  bootstrap-touch-slider Slider -->
                    @endif
                </div>
            </div>
        </div>
    </div>
    @if(isset($mot) && $mot->count())
    <div class="section  mtop10">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">

                </div>
                @if($mot->photo!=NULL)
                <div class="col-sm-6 text-center bg-s">
                    <img class=" img-thumbnail mtop10" style="height: auto;" src="{{asset($mot->photo)}}" >
                </div>
                <div class="col-sm-6">
                    <div class="headline mtop30 text-uppercase light rs"><h2 class="light">{{$mot->titre}}</h2></div>  
                    <h3 class="huge-h-5 light-3 ">{!! str_limit(strip_tags($mot->contenu),300) !!}</h3>
                    <a class="" href="{{route('pageContentPresentation',['slug'=>'mot-du-directeur'])}}">Lire la suite <i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
                </div>
                @else
                <div class="col-sm-8 col-sm-offset-2 light os light-3 text-center">
                    <h3 class="huge-h-5 ">{!! str_limit($mot->contenu,200) !!}</h3>
                    <a class="text-center" href="{{route('pageContentPresentation',['slug'=>'mot-du-directeur'])}}"> <i class="fa fa-angle-left"></i><i class="fa fa-angle-left"></i> Lire la suite <i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
                </div>
                @endif

            </div>
        </div>
    </div>
    @endif
    <div id="home-nav-tab">
        <div  class="container">
            <div class="col-sm-12 ">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-tabs-justified" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                        <i class="fa fa-info-circle data-icone "></i>
                        <span class="hidden-xs text-uppercase">Le COUS en Bref</span></a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
                            <i class="fa fa-cubes data-icone "></i>
                            <span class="hidden-xs text-uppercase">Attributions & Missions</span>
                        </a></li>
                <!--li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">
                        <i class="fa fa-question-circle data-icone "></i> 
                        <span class="hidden-xs text-uppercase"> Questions fréquentes </span>
                    </a></li-->
                </ul>

            </div>

        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 ">
                <!-- Tab panes -->

                <div class="tab-content panel-body__">

                    <div role="tabpanel" class="tab-pane active" id="home">

                        <div class="carousel slide"  data-ride="carousel" id="quote-carousel">


                            <!-- Carousel Slides / Quotes -->
                            <div class="carousel-inner">

                                <!-- Quote 1 -->
                                <div class="item active">
                                    <blockquote>
                                        <div class="row">
                                            <div class="col-sm-3 text-center">
                                                <img class="img-circle" src="{{asset('assets/images/logo_cous_parakou.png')}}" style="width: 100px;height:100px;">
                                                <!--<img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg" style="width: 100px;height:100px;">-->
                                            </div>
                                            <div class="col-sm-9">
                                                <p>Il est placé sous la tutelle du Ministre en charge de l'Enseignement Supérieur.</p>
                                                <small>AOF COUS Parakou</small>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                                <!-- Quote 2 -->
                                <div class="item">
                                    <blockquote>
                                        <div class="row">
                                            <div class="col-sm-3 text-center">
                                                <img class="img-circle" src="{{asset('assets/images/logo_cous_parakou.png')}}" style="width: 100px;height:100px;">
                                            </div>
                                            <div class="col-sm-9">
                                                <p>Le COUS-P est doté de la personnalité juridique et de l'autonomie financière. </p>
                                                <small>AOF COUS Parakou</small>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                                <!-- Quote 3 -->
                                <div class="item">
                                    <blockquote>
                                        <div class="row">
                                            <div class="col-sm-3 text-center">
                                                <img class="img-circle" src="{{asset('assets/images/logo_cous_parakou.png')}}" style="width: 100px;height:100px;">
                                            </div>
                                            <div class="col-sm-9">
                                                <p>Le COUS-P a pour mission d'assurer les prestations et les services propres à améliorer les conditions de vie et d'étude des étudiants</p>
                                                <small>AOF COUS Parakou</small>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>

                            </div>

                            <!-- Carousel Buttons Next/Prev -->
                            <div id="slider-control-homes hidden">
                                <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><img src="{{asset('assets/images/static/arrow_left.png')}}" alt="Left" class="img-responsive"></a>
                                <a data-slide="next" href="#quote-carousel" class="right carousel-control"><img src="{{asset('assets/images/static/arrow_right.png')}}" alt="Right" class="img-responsive"></a>
                            </div>
                        </div>  
                        <script>
                        /*********************************
                         * 
                         * #222222/#383838
                         */

                     </script>      
                 </div>
                 <div role="tabpanel" class="tab-pane" id="profile">
                    <div class="panel-body ">
                        <div class="huge-h-5 light-3">
                            <div class="col-sm-10 text-center col-sm-offset-1 ">
                                <div>
                                <h2>{!! str_limit(strip_tags($mat->contenu),200)!!}</h4>
                                </div>                 
                                <a class="btn btn-primary btn-links mtop30" href="{{url('presentation/attributions-et-missions.html')}}"><< Plus de détails >></a>
                            </div>
                        </div>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="messages">
                    <div  class="panel-group acc-v1 list-collapse" id="accordion-1">

                        <div class="panel panel-default">
                            <div class="panel-heading ">
                                <h4 class="panel-title rs">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-One" aria-expanded="false">
                                        Collapsible Group Item #1 <i class="fa fa-angle-right pull-right"></i>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-One" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <img class="img-responsive" src="assets/img/main/img12.jpg" alt="">
                                        </div>
                                        <div class="col-md-8">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                                            <i class="fa fa-angle-right pull-right"></i> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title rs">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-Two" aria-expanded="false">
                                            Collapsible Group Item #2
                                            <i class="fa fa-angle-right pull-right"></i></a>
                                        </h4>
                                    </div>
                                    <div id="collapse-Two" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.</p>
                                                    <ul class="list-unstyled">
                                                        <li><i class="fa fa-check color-green"></i> Donec id elit non mi porta gravida at eget metus..</li>
                                                        <li><i class="fa fa-check color-green"></i> Fusce dapibus, tellus ac cursus comodo egetine..</li>
                                                        <li><i class="fa fa-check color-green"></i> Food truck quinoa nesciunt laborum eiusmod runch..</li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <img class="img-responsive" src="assets/img/main/img9.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title rs">
                                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-Three" aria-expanded="false">
                                                Collapsible Group Item #3
                                                <i class="fa fa-angle-right pull-right"></i></a>
                                            </h4>
                                        </div>
                                        <div id="collapse-Three" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                            <div class="panel-body">
                                                Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Food truck quinoa nesciunt laborum eiusmodolf moon tempor, sunt aliqua put a bird. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title rs">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-Four" aria-expanded="true">
                                                    Collapsible Group Item #4
                                                    <i class="fa fa-angle-right pull-right"></i></a>
                                                </h4>
                                            </div>
                                            <div id="collapse-Four" class="panel-collapse collapse " aria-expanded="true" style="">
                                                <div class="panel-body">
                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                <!--                <div role="tabpanel" class="tab-pane" id="settings"><h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>
                                    </div>-->
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="bg_gradientbleu section">
                        <div class="container ">
                            <div class="row">
<style>
/***************************************************/
.carousel-inner>.item.active, .carousel-inner>.item.next.left, .carousel-inner>.item.prev.right{
padding-top: 30px;
}
a.carousel-control {
/*margin-left: 7%;*/
display: none
}
#slider-control-home .carousel-control{position: absolute;}
#home p{font-size: 35px;color: #0c2a44}
#slider-control-home .right{
/*margin-right:  -7%;*/
}
.list-line li{color: #eae70f}
.list-collapse{
/*box-shadow:0px 3px 5px #ddd;*/
padding: 0 15px 25px;
margin: 0
/*margin: 0 15px;*/
}
.llight{
font-family: 'Segoe UI Light','Open Sans Light','Arial';

}
#home-nav-tab .nav-tabs{border-bottom: 7px solid #0c2a44;margin-bottom: 5px}
.list-collapse .panel.panel-default{
border-radius: 0 !important;
border: 0px;
border: 1px solid #ccc;
/*background: #fff;*/
}
.panel-default>.panel-heading{background: #fff;}
/*.panel-default .panel-heading:nth-child(odd){background: #f5f5f5}*/
.panel-default>.panel-heading:hover{background: #fff}
.list-collapse .panel-heading1 {
border-radius: 5px;
background: #4d90fe;
padding: 25px 15px;
margin-bottom: 0;
color: #fff;
}
/*                .list-collapse .panel-default:nth-child(2n+1){
border-left: 3px solid #fbdf0c;
border-right:  3px solid #fbdf0c;
}
.list-collapse .panel-default:nth-child(2n+2){
border-left: 3px solid #0c2a44;
border-right: 3px solid #0c2a44;
}*/
.list-collapse .panel-default{
border: 0px solid #fff;
border-bottom: 1px solid #ddd;
margin: 0;
}
.list-collapse .panel-heading{
border-radius: 0px;
padding: 25px 15px;
margin-bottom: 0;

}
.list-collapse .panel-body{font-size:20px}
.list-collapse .panel-title rs a:hover{color: #0c2a44;text-decoration: none;}
.list-collapse .panel-title rs a{display:block}
.list-collapse .panel-title rs{text-transform: uppercases}
/**********************************************/
.section{
padding-top: 30px;
padding-bottom: 30px;
}
.headline {
display: block;
margin: 10px 0 25px 0;
border-bottom: 1px dotted #e4e9f0;
}
.headline h2, .headline h3, .headline h4 {
margin: 0 0 -2px 0;
padding-bottom: 5px;
display: inline-block;
border-bottom: 2px solid #ffe41b;
}
.headline h2 {
font-size: 22px;
}
.thumbnail-style {
padding: 0px;
margin-bottom: 20px;
height: 350px;
overflow: hidden;
border: 2px solid #ccc;

border-radius: 5px;
}
.thumbnail-style .thumbnail-img {
position: relative;
margin-bottom: 11px;
}
.overflow-hidden {
overflow: hidden;
}
.bg_gradientbleu{
     background: -moz-radial-gradient(center, ellipse cover, rgba(0,0,255,1) 0%, rgba(0,0,0,1) 100%); /* ff3.6+ */
background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, rgba(0,0,255,1)), color-stop(100%, rgba(0,0,0,1))); /* safari4+,chrome */
background:-webkit-radial-gradient(center, ellipse cover, rgba(0,0,255,1) 0%, rgba(0,0,0,1) 100%); /* safari5.1+,chrome10+ */
background: -o-radial-gradient(center, ellipse cover, rgba(0,0,255,1) 0%, rgba(0,0,0,1) 100%); /* opera 11.10+ */
background: -ms-radial-gradient(center, ellipse cover, rgba(0,0,255,1) 0%, rgba(0,0,0,1) 100%); /* ie10+ */
background:radial-gradient(ellipse at center, rgba(0,0,255,1) 0%, rgba(0,0,0,1) 100%); /* w3c */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0000ff', endColorstr='#000000',GradientType=1 ); /* ie6-9 */ 

}
.bg-light{
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ffffff+1,eeeeee+95 */
background: rgb(255,255,255); /* Old browsers */
background: -moz-linear-gradient(top, rgba(255,255,255,1) 1%, rgba(238,238,238,1) 95%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, rgba(255,255,255,1) 1%,rgba(238,238,238,1) 95%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, rgba(255,255,255,1) 1%,rgba(238,238,238,1) 95%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#eeeeee',GradientType=0 ); /* IE6-9 */
}
.thumbnail-kenburn img {
left: 10px;
margin-left: -10px;
position: relative;
-webkit-transition: all 0.8s ease-in-out;
-moz-transition: all 0.8s ease-in-out;
-o-transition: all 0.8s ease-in-out;
-ms-transition: all 0.8s ease-in-out;
transition: all 0.8s ease-in-out;
}
.thumbnail h3 a, .thumbnail-style h3 a {
color: #585f69;
font-size: 18px;
}
.thumbnail-style{
background: #ffffff;
}
.thumbnail-style:hover {
box-shadow: 0 0 8px #ddd;
-webkit-transition: box-shadow 0.2s ease-in-out;
-moz-transition: box-shadow 0.2s ease-in-out;
-o-transition: box-shadow 0.2s ease-in-out;
transition: box-shadow 0.2s ease-in-out;
}
.thumbnail-kenburn:hover img {
-webkit-transform: scale(1.2) rotate(2deg);
-moz-transform: scale(1.2) rotate(2deg);
-o-transform: scale(1.2) rotate(2deg);
-ms-transform: scale(1.2) rotate(2deg);
transform: scale(1.2) rotate(2deg);
}
.thumbnail-style:hover a.btn-more {
right: 10px;
transition: ease all .3s;
}
.thumbnail-style a.btn-more:hover {
text-decoration: none;
-moz-box-shadow: 0px 0px 4px #ddd;
-webkit-box-shadow: 0px 0px 4px #ddd;
box-shadow: 0px 0px 4px #ddd;
}
.new-caption{
padding: 0 10px 7px;
height: 120px;
}
.thumbnail-style a.btn-more {
right: 0px;
bottom: 10px;
transition: ease all .3s;
color: #fff;
padding: 1px 6px;
position: absolute;
background: #e5201a;
display: inline-block;
}

a, a:focus, a:hover, a:active {
outline: 0 !important;
}
.news-cover{
height: 130px;
border-bottom: #f8f8f8 0px solid;
}
/*Team-Social (Temporary)*/
.team-social {
margin-bottom: 0;
}

.team-social li {
padding: 0 2px;
}

.team-social li a {
font-size: 14px;
padding: 6px 4px;
text-align: center;
border-radius: 50% !important;
}

.team-social li i {
min-width: 20px;
}

.team-social li a.fb {
color: #4862a3;
border: 0px solid #4862a3;
}

.team-social li a.tw {
color: #159ceb;
border: 0px solid #159ceb;
}

.team-social li a.gp {
color: #dc4a38;
border: 0px solid #dc4a38;
}

.team-social li a:hover {
text-decoration: none;
}

.team-social li a.fb:hover {
color: #fff;
background: #4862a3;
}

.team-social li a.tw:hover {
color: #fff;
background: #159ceb;
}

.team-social li a.gp:hover {
color: #fff;
background: #dc4a38;
}
/*******************************************************/
.service-block-v4 .service-desc>div{
background-color: #fff;
border-radius: 10px 10px;
box-shadow: -1px 3px 12px #ddd;
font-size: 14px;
border: 1px solid #ddd;
padding: 0 14px 16px;
margin-bottom: 20px;
padding-top: 15px;
padding-bottom: 15px;
height: 280px;
}
</style>
                                <div class="col-sm-12">
                                    <div class="headline text-uppercase text-white"><h2 class="rs">Nos Activités</h2></div>
                                </div>
                                <div class="clearfix actu_cover ">

                                    <?php
                                    if (isset($activites) && $activites->count()) {
                                        ?>

                                        <div class="owl-carousels2rowsowl-loaded" >   
                                            <?php
                                            if (count($activites) > 0) {
                                                $in = -1;
                                                foreach ($activites as $cle => $actu) {
                                                    $in++;
                                                    if ($in == 5) {
                                                        $in = 0;
                                                    }
//                            $nc = "color" . $in;
//                            $date1 = time();
//                            $diff_date = abs($date1 - strtotime($actu->created_at)) / (60 * 60 * 24);
//                            $diff_update = abs($date1 - strtotime($actu->updated_at)) / (60 * 60 * 24);
                                                    ?>


                                                    <div class="col-md-3 col-sm-6">
                                                        <div class="thumbnails thumbnail-style thumbnail-kenburn">
                                                            <div class="thumbnail-img">
                                                                <div class="overflow-hidden news-cover">
                                                                    <?php
                                                                    if (isset($actu->photo) && !empty($actu->photo)) {
                                                                        ?>
                                                                        <a href="{{URL('activite/'.$actu->slug)}}">
                                                                            <img class="img-responsive"  src="{{asset($actu->photo)}}" alt="{{isset($actu->titre) ? (strlen($actu->titre) > 50) ? substr($actu->titre, 0, 50) . "..." : $actu->titre : ""}}">klklkllkkklkl</a>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <a href="{{URL('activite/'.$actu->slug)}}">
                                                                                <img class="img-responsive"  src="{{asset("assets/images/static/actu-newspaper.png")}}" alt="{{ isset($actu->titre) ? (strlen($actu->titre) > 50) ? substr($actu->titre, 0, 50) . "..." : $actu->titre : ""}}"></a>
<?php
}
?>
</div>
<a class="btn-more hover-effect text-sm"  href="{{URL('activite/'.$actu->slug)}}" >Consulter  +</a>					
</div>
<div class="new-caption ">
    <h3>
        <a  href="{{URL('activite/'.$actu->slug)}}">{{isset($actu->titre) ? str_limit($actu->titre, 102 ) : ""}}</a>
    </h3>
    <p class="   actu-content text-muted  text-sm ">
        <a href="{{URL('activite/'.$actu->slug)}}" class="text-muted">
            <!--{!! isset($actu->chapeau) ? str_limit(strip_tags($actu->chapeau), 150): strip_tags(substr($actu->contenu, 200))!!}-->
        </a>

    </p>

</div>
<div class=" text-center">
    <hr>
    <ul class="list-inline team-social">
        <li>
            <a data-placement="top" data-toggle="tooltip" class="fb tooltips" data-original-title="Facebook" href="#">
                <i class="fa fa-facebook"></i>
            </a>
        </li>
        <li>
            <a data-placement="top" data-toggle="tooltip" class="tw tooltips" data-original-title="Twitter" href="#">
                <i class="fa fa-twitter"></i>
            </a>
        </li>
        <li>
            <a data-placement="top" data-toggle="tooltip" class="gp tooltips" data-original-title="Google plus" href="#">
                <i class="fa fa-google-plus"></i>
            </a>
        </li>
    </ul>
</div>
</div>
</div>
<?php
}
} else {
    ?>
    <div class="well text-center margin-top-30 text-muted">
        <i class="fa fa-newspaper-o  huge-data-fa margin-top-30" ></i>
        <h2 class="light" >Aucune activité pour le moment</h2 >
    </div>
    <?php
}
?>
</div>
<div class=" row activite-link">

    <div class=" col-md-12 borderTopeborderBote  text-center clearfix mtop30">
        <a class="btn btn-default btn-lsg btn-b shadow2  text-primary rond3   " href="{{url('activites')}}"><i class="fa fa-newspaper-o light"></i> Consulter plus d'activités</a>
    </div>
</div>
<?php
}
?>

</div>
</div>
</div>
</div>

<div class="bg-lighte section">
    <div class="container ">
        <div class="row">
            <div class="col-sm-6">
                <div class="headline text-uppercase"><h2 class="rs">A LA DECOUVERTE DU COUS PARAKOU</h2></div>
                 <ul class="list-group">
                  <li class="list-group-item"><a href="{{url('presentation/historique.html')}}">Historique du COUS Parakou <i class="glyphicon glyphicon-chevron-right pull-right"></i></a></li>
                  <li class="list-group-item"><a href="{{url('presentation/attributions-et-missions.html')}}">Attributions et Missions<i class="glyphicon glyphicon-chevron-right pull-right"></i></a></li>
                  <li class="list-group-item"><a href="{{url('presentation/equipe-de-direction.html')}}">L'équipe de Direction<i class="glyphicon glyphicon-chevron-right pull-right"></i></a></li>
                  <li class="list-group-item"><a href="{{url('presentation/organigramme.html')}}">L'Organigramme<i class="glyphicon glyphicon-chevron-right pull-right"></i></a></li>
                  <li class="list-group-item"><a href="{{url('personnel')}}">Le Personnel<i class="glyphicon glyphicon-chevron-right pull-right"></i></a></li>
                  <li class="list-group-item"><a href="{{url('documents-administratifs')}}">Documents Administratifs<i class="glyphicon glyphicon-chevron-right pull-right"></i></a></li>
                  <li class="list-group-item"><a href="{{url('galerie')}}">Galerie Images<i class="glyphicon glyphicon-chevron-right pull-right"></i></a></li>
                </ul> 
            </div>
            <div class="col-sm-6">
                <div class="headline text-uppercase"><h2 class="rs">SERVICES ET ACTIVITES</h2></div>
                 <ul class="list-group">
                  <li class="list-group-item"><a href="{{url('activites')}}">Nos activités <i class="glyphicon glyphicon-chevron-right pull-right"></i></a></li>
                  <li class="list-group-item"><a href="{{url('services/hebergement-condition-generales.html')}}">Hébergement <i class="glyphicon glyphicon-chevron-right pull-right"></i></a></li>
                  <li class="list-group-item"><a href="{{url('services/restauration-conditions-generales.html')}}">Restauration <i class="glyphicon glyphicon-chevron-right pull-right"></i></a></li>
                  <li class="list-group-item"><a href="{{url('ervices/transport-materiels-roulants.html')}}">Transport <i class="glyphicon glyphicon-chevron-right pull-right"></i></a></li>
                  <li class="list-group-item"><a href="{{url('services/activites-sportives-objectifs.html')}}">Activités Sportives <i class="glyphicon glyphicon-chevron-right pull-right"></i></a></li>
                  <li class="list-group-item"><a href="{{url('services/sante-et-affaires-sociales-service-de-sante.html')}}">Santé et Affaires Sociales <i class="glyphicon glyphicon-chevron-right pull-right"></i></a></li>
                  <li class="list-group-item"><a href="{{url('services/activites-artistiques-et-culturelles-objectifs.html')}}">Activités Artistiques et Culturelles <i class="glyphicon glyphicon-chevron-right pull-right"></i></a></li>
                </ul> 
            </div>


        </div>
    </div>
</div>

<div class="section avis-section hidden">
    <div class="container">
        <div class="row">
            <div class="col-md-12" data-wow-delay="0.2s">
                <h1 class="huge-h-4 rs text-white text-center">AVIS DES ETUDIANTS</h1>
                <div class="carousel slide" data-ride="carousel" id="avis">
                    <!-- Bottom Carousel Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#avis" data-slide-to="0" class="active"><img class="img-responsive " src="{{asset('uploads/users/1.jpg')}}" alt="">
                        </li>
                        <li data-target="#avis" data-slide-to="1"><img class="img-responsive" src="{{asset('uploads/users/2.png')}}" alt="">
                        </li>
                        <li data-target="#avis" data-slide-to="2"><img class="img-responsive" src="{{asset('uploads/users/3.gif')}}" alt="">
                        </li>
                    </ol>



                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner text-center">

                        <!-- Quote 1 -->
                        <div class="item active">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. !</p>
                                        <small>Someone famous</small>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 2 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                        <small>Someone famous</small>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 3 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. .</p>
                                        <small>Someone famous</small>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                    </div>

                    <!-- Carousel Buttons Next/Prev -->
                    <a data-slide="prev" href="#avis" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                    <a data-slide="next" href="#avis" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                </div>
            </div>
            <style>
            /* Carousel */
            .avis-section{
                background:#0c2a44;
                color:white;

            }
            #avis{
                min-height: 350px;
            }
            #quote-carousel1, #avis {
                padding: 0 10px 30px 10px;
                margin-top: 30px;
                /* Control buttons  */
                /* Previous button  */
                /* Next button  */
                /* Changes the position of the indicators */
                /* Changes the color of the indicators */
            }
            #quote-carousel .carousel-control,#avis .carousel-control {
                background: none;
                color: #CACACA;
                font-size: 2.3em;
                text-shadow: none;
                margin-top: 80px;
            }

            #avis .carousel-indicators, #quote-carousel .carousel-indicators {
                right: 50%;
                top: auto;
                bottom: 0px;
                margin-right: -19px;
            }
            #avis .carousel-indicators li, #quote-carousel .carousel-indicators li {
                width: 50px;
                height: 50px;
                margin: 5px;
                cursor: pointer;
                border: 4px solid #CCC;
                border-radius: 50px;
                opacity: 0.4;
                overflow: hidden;
                transition: all 0.4s;
            }
            #avis .carousel-indicators .active {
                background: #333333;
                width: 130px;
                height: 130px;
                border-radius: 100px;
                border-color: #4d90fe;
                opacity: 1;
                overflow: hidden;
            }
            .carousel-inner {
                /*min-height: 100px;*/
            }
            .item blockquote {
                border-left: none;
                margin: 0;
            }
            .item blockquote p:before {
                content: "\f10d";
                font-family: 'Fontawesome';
                float: left;
                margin-right: 10px;
            }
        </style>
    </div>
</div>
</div>

<div class=" " style="" >
    <img src="{{asset('assets/images/app/presentation_cous.png')}}" class="img-responsive mauto">
    <a href="{{url('services/hebergement-condition-generales.html')}}" class="btn btn-primary abs-btn-center btn-lg shadow1  rond0" >En savoir plus <i class="fa fa-chevron-circle-right"></i></a>
</div>
</div>
<style>
/***********************************************************/
    /*AJOUT DE CSS
    /***********************************************************/
    .abs-btn-center{
        position: absolute;
        margin-top: -12%;
        left: 56%;
    }
    /*************************************************************/
    .bs-slider{
        overflow: hidden;
        max-height: 700px;
        position: relative;
        background: #000000;
    }
    .bs-slider:hover {
        cursor: -moz-grab;
        cursor: -webkit-grab;
    }
    .bs-slider:active {
        cursor: -moz-grabbing;
        cursor: -webkit-grabbing;
    }
    .bs-slider .bs-slider-overlay {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.140);
    }
    .bs-slider > .carousel-inner > .item > img,
    .bs-slider > .carousel-inner > .item > a > img {
        margin: auto;
        width: 100% !important;
    }

    /********************
    *****Slide effect
    **********************/

    .fade {
        opacity: 1;
    }
    .fade .item {
        top: 0;
        z-index: 1;
        opacity: 0;
        width: 100%;
        position: absolute;
        left: 0 !important;
        display: block !important;
        -webkit-transition: opacity ease-in-out 1s;
        -moz-transition: opacity ease-in-out 1s;
        -ms-transition: opacity ease-in-out 1s;
        -o-transition: opacity ease-in-out 1s;
        transition: opacity ease-in-out 1s;
    }
    .fade .item:first-child {
        top: auto;
        position: relative;
    }
    .fade .item.active {
        opacity: 1;
        z-index: 2;
        -webkit-transition: opacity ease-in-out 1s;
        -moz-transition: opacity ease-in-out 1s;
        -ms-transition: opacity ease-in-out 1s;
        -o-transition: opacity ease-in-out 1s;
        transition: opacity ease-in-out 1s;
    }






    /*---------- LEFT/RIGHT ROUND CONTROL ----------*/
    .control-round .carousel-control {
        top: 47%;
        opacity: 0;
        width: 45px;
        height: 45px;
        z-index: 100;
        color: #ffffff;
        display: block;
        font-size: 24px;
        cursor: pointer;
        overflow: hidden;
        line-height: 43px;
        text-shadow: none;
        position: absolute;
        font-weight: normal;
        background: transparent;
        -webkit-border-radius: 100px;
        border-radius: 100px;
    }
    .control-round:hover .carousel-control{
        opacity: 1;
    }
    .control-round .carousel-control.left {
        left: 1%;
    }
    .control-round .carousel-control.right {
        right: 1%;
    }
    .control-round .carousel-control.left:hover,
    .control-round .carousel-control.right:hover{
        color: #fdfdfd;
        background: rgba(0, 0, 0, 0.5);
        border: 0px transparent;
    }
    .control-round .carousel-control.left>span:nth-child(1){
        left: 45%;
    }
    .control-round .carousel-control.right>span:nth-child(1){
        right: 45%;
    }





    /*---------- INDICATORS CONTROL ----------*/
    .indicators-line > .carousel-indicators{
        right: 45%;
        bottom: 3%;
        left: auto;
        width: 90%;
        height: 20px;
        font-size: 0;
        overflow-x: auto;
        text-align: right;
        overflow-y: hidden;
        padding-left: 10px;
        padding-right: 10px;
        padding-top: 1px;
        white-space: nowrap;
    }
    .indicators-line > .carousel-indicators li{
        padding: 0;
        width: 15px;
        height: 15px;
        border: 1px solid rgb(158, 158, 158);
        text-indent: 0;
        overflow: hidden;
        text-align: left;
        position: relative;
        letter-spacing: 1px;
        background: #ffffff;
        -webkit-font-smoothing: antialiased;
        -webkit-border-radius: 50%;
        border-radius: 50%;
        margin-right: 5px;
        -webkit-transition: all 0.5s cubic-bezier(0.22,0.81,0.01,0.99);
        transition: all 0.5s cubic-bezier(0.22,0.81,0.01,0.99);
        z-index: 10;
        cursor:pointer;
    }
    .indicators-line > .carousel-indicators li:last-child{
        margin-right: 0;
    }
    .indicators-line > .carousel-indicators .active{
        margin: 1px 5px 1px 1px;
        box-shadow: 0 0 0 2px #fff;
        background-color: transparent;
        position: relative;
        -webkit-transition: box-shadow 0.3s ease;
        -moz-transition: box-shadow 0.3s ease;
        -o-transition: box-shadow 0.3s ease;
        transition: box-shadow 0.3s ease;
        -webkit-transition: background-color 0.3s ease;
        -moz-transition: background-color 0.3s ease;
        -o-transition: background-color 0.3s ease;
        transition: background-color 0.3s ease;

    }
    .indicators-line > .carousel-indicators .active:before{
        transform: scale(0.5);
        background-color: #fff;
        content:"";
        position: absolute;
        left:-1px;
        top:-1px;
        width:15px;
        height: 15px;
        border-radius: 50%;
        -webkit-transition: background-color 0.3s ease;
        -moz-transition: background-color 0.3s ease;
        -o-transition: background-color 0.3s ease;
        transition: background-color 0.3s ease;
    }



    /*---------- SLIDE CAPTION ----------*/
    .slide-text container text-center slide_style_left_ {
        /*text-align: left !important;*/
    }
    .slide_style_right {
        text-align: right !important;
    }
    .slide_style_center {
        text-align: center !important;
    }

    .slide-text {
        left: 0;
        top: 0%;
        right: 0;
        margin: auto;
        padding: 0px;
        width:100%;
        height: 100%;
        position: absolute;
        /*text-align: left;*/
        background: rgba(1, 24, 66, 0.6);
        padding: 10px 85px;

    }
    .carousel-inner,.carousel-inner.item >img{
        min-height: 280px;
    }

    .slide-text > h1 {

        padding: 0;
        color: #ffffff;
        font-size: 50px;
        font-style: normal;
        line-height: 84px;
        margin-bottom: 30px;
        letter-spacing: 1px;
        display: inline-block;
        font-weight:1200;
        text-shadow: 0 0 5px #333333;
        -webkit-animation-delay: 0.7s;
        animation-delay: 0.7s;
        font-family: "Roboto", Helvetica, Arial, sans-serif;
        font-size: 6.5rem !important;
        text-transform: uppercase;
    }
    .slide-text > p {
        padding: 0;
        color: #ffffff;
        font-size: 130%;
        line-height: 44px;
        font-weight: 300;
        margin-bottom: 40px;
        letter-spacing: 1px;
        -webkit-animation-delay: 1.1s;
        animation-delay: 1.1s;
        padding:0 20%;
        margin-top:-20px;
        font-weight: 300;
    }
    .slide-text > a.btn-default{
        color: #000;
        font-weight: 400;
        font-size: 13px;
        line-height: 15px;
        margin-right: 10px;
        text-align: center;
        padding: 17px 30px;
        white-space: nowrap;
        letter-spacing: 1px;
        display: inline-block;
        border: none;
        text-transform: uppercase;
        -webkit-animation-delay: 2s;
        animation-delay: 2s;
        -webkit-transition: background 0.3s ease-in-out, color 0.3s ease-in-out;
        transition: background 0.3s ease-in-out, color 0.3s ease-in-out;

    }
    .lcard .panel-body{
        background: #eee;
        border-radius: 0 40px;
    }
    .slide-text > a.btn-primary{
        color: #ffffff;
        cursor: pointer;
        font-weight: 400;
        font-size: 13px;
        line-height: 15px;
        margin-left: 10px;
        text-align: center;
        padding: 17px 30px;
        white-space: nowrap;
        letter-spacing: 1px;
        background: #00bfff;
        display: inline-block;
        text-decoration: none;
        text-transform: uppercase;
        border: none;
        -webkit-animation-delay: 2s;
        animation-delay: 2s;
        -webkit-transition: background 0.3s ease-in-out, color 0.3s ease-in-out;
        transition: background 0.3s ease-in-out, color 0.3s ease-in-out;
    }
    .slide-text > a:hover,
    .slide-text > a:active {
        color: #ffffff;
        background: #222222;
        -webkit-transition: background 0.5s ease-in-out, color 0.5s ease-in-out;
        transition: background 0.5s ease-in-out, color 0.5s ease-in-out;
    }






    /*------------------------------------------------------*/
    /* RESPONSIVE
    /*------------------------------------------------------*/

    @media (max-width: 991px) {
        body{
            overflow-x: hidden;
        }
        #avis .carousel-indicators{display: none}
        #home p{font-size: 25px;text-align: center}
        .slide-text h1 {
            font-size: 100% !important;
            line-height: 50px;
            margin-bottom: 20px;

        }
        .slide-text h1{
            margin:5px auto 10px;
        }
        .carousel-indicators{display: none}
        .carousel-inner, .carousel-inner.item > img{
            height: 100%;
            min-height:auto;
        }
        .bs-slider > .carousel-inner > .item > img, .bs-slider > .carousel-inner > .item > a > img{
            min-height: 150px;
        }
        .abs-btn-center{
            position: relative;
            display: block;
            margin:0;
            left: 0
        }
        .slide-text > p {

            font-size: 18px;
        }
    }


    /*---------- MEDIA 480px ----------*/
    @media  (max-width: 768px) {
        .slide-text {
            padding: 10px 50px;
        }
        .slide-text h1 {
            font-size: 30px;
            line-height: 40px;
            margin-bottom: 10px;
        }
        .slide-text > p {
            font-size: 14px;
            line-height: 20px;
            margin-top: 2px;
        }
        .control-round .carousel-control{
            display: none;
        }

    }
    @media  (max-width: 480px) {
        .slide-text {
            padding: 10px 30px;
        }
        .slide-text h1 {
            font-size: 20px;
            line-height: 25px;
            margin-bottom: 5px;
        }
        .slide-text > p {
            font-size: 12px;
            line-height: 18px;
            margin-bottom: 10px;
        }
        .slide-text > a.btn-default, 
        .slide-text > a.btn-primary {
            font-size: 10px;
            line-height: 10px;
            margin-right: 10px;
            text-align: center;
            padding: 10px 15px;
        }
        .indicators-line > .carousel-indicators{
            display: none;
        }

    }

</style>
@section('script')
<script>
    /*Bootstrap Carousel Touch Slider.
     
     http://bootstrapthemes.co
     
     Credits: Bootstrap, jQuery, TouchSwipe, Animate.css, FontAwesome
     
     */


     (function (a) {
        if (typeof define === "function" && define.amd && define.amd.jQuery) {
            define(["jquery"], a)
        } else {
            if (typeof module !== "undefined" && module.exports) {
                a(require("jquery"))
            } else {
                a(jQuery)
            }
        }
    }(function (f) {
        var y = "1.6.15", p = "left", o = "right", e = "up", x = "down", c = "in", A = "out", m = "none", s = "auto", l = "swipe", t = "pinch", B = "tap", j = "doubletap", b = "longtap", z = "hold", E = "horizontal", u = "vertical", i = "all", r = 10, g = "start", k = "move", h = "end", q = "cancel", a = "ontouchstart" in window, v = window.navigator.msPointerEnabled && !window.navigator.pointerEnabled && !a, d = (window.navigator.pointerEnabled || window.navigator.msPointerEnabled) && !a, C = "TouchSwipe";
        var n = {fingers: 1, threshold: 75, cancelThreshold: null, pinchThreshold: 20, maxTimeThreshold: null, fingerReleaseThreshold: 250, longTapThreshold: 500, doubleTapThreshold: 200, swipe: null, swipeLeft: null, swipeRight: null, swipeUp: null, swipeDown: null, swipeStatus: null, pinchIn: null, pinchOut: null, pinchStatus: null, click: null, tap: null, doubleTap: null, longTap: null, hold: null, triggerOnTouchEnd: true, triggerOnTouchLeave: false, allowPageScroll: "auto", fallbackToMouseEvents: true, excludedElements: "label, button, input, select, textarea, a, .noSwipe", preventDefaultEvents: true};
        f.fn.swipe = function (H) {
            var G = f(this), F = G.data(C);
            if (F && typeof H === "string") {
                if (F[H]) {
                    return F[H].apply(this, Array.prototype.slice.call(arguments, 1))
                } else {
                    f.error("Method " + H + " does not exist on jQuery.swipe")
                }
            } else {
                if (F && typeof H === "object") {
                    F.option.apply(this, arguments)
                } else {
                    if (!F && (typeof H === "object" || !H)) {
                        return w.apply(this, arguments)
                    }
                }
            }
            return G
        };
        f.fn.swipe.version = y;
        f.fn.swipe.defaults = n;
        f.fn.swipe.phases = {PHASE_START: g, PHASE_MOVE: k, PHASE_END: h, PHASE_CANCEL: q};
        f.fn.swipe.directions = {LEFT: p, RIGHT: o, UP: e, DOWN: x, IN: c, OUT: A};
        f.fn.swipe.pageScroll = {NONE: m, HORIZONTAL: E, VERTICAL: u, AUTO: s};
        f.fn.swipe.fingers = {ONE: 1, TWO: 2, THREE: 3, FOUR: 4, FIVE: 5, ALL: i};
        function w(F) {
            if (F && (F.allowPageScroll === undefined && (F.swipe !== undefined || F.swipeStatus !== undefined))) {
                F.allowPageScroll = m
            }
            if (F.click !== undefined && F.tap === undefined) {
                F.tap = F.click
            }
            if (!F) {
                F = {}
            }
            F = f.extend({}, f.fn.swipe.defaults, F);
            return this.each(function () {
                var H = f(this);
                var G = H.data(C);
                if (!G) {
                    G = new D(this, F);
                    H.data(C, G)
                }
            })
        }
        function D(a5, au) {
            var au = f.extend({}, au);
            var az = (a || d || !au.fallbackToMouseEvents), K = az ? (d ? (v ? "MSPointerDown" : "pointerdown") : "touchstart") : "mousedown", ax = az ? (d ? (v ? "MSPointerMove" : "pointermove") : "touchmove") : "mousemove", V = az ? (d ? (v ? "MSPointerUp" : "pointerup") : "touchend") : "mouseup", T = az ? (d ? "mouseleave" : null) : "mouseleave", aD = (d ? (v ? "MSPointerCancel" : "pointercancel") : "touchcancel");
            var ag = 0, aP = null, a2 = null, ac = 0, a1 = 0, aZ = 0, H = 1, ap = 0, aJ = 0, N = null;
            var aR = f(a5);
            var aa = "start";
            var X = 0;
            var aQ = {};
            var U = 0, a3 = 0, a6 = 0, ay = 0, O = 0;
            var aW = null, af = null;
            try {
                aR.bind(K, aN);
                aR.bind(aD, ba)
            } catch (aj) {
                f.error("events not supported " + K + "," + aD + " on jQuery.swipe")
            }
            this.enable = function () {
                aR.bind(K, aN);
                aR.bind(aD, ba);
                return aR
            };
            this.disable = function () {
                aK();
                return aR
            };
            this.destroy = function () {
                aK();
                aR.data(C, null);
                aR = null
            };
            this.option = function (bd, bc) {
                if (typeof bd === "object") {
                    au = f.extend(au, bd)
                } else {
                    if (au[bd] !== undefined) {
                        if (bc === undefined) {
                            return au[bd]
                        } else {
                            au[bd] = bc
                        }
                    } else {
                        if (!bd) {
                            return au
                        } else {
                            f.error("Option " + bd + " does not exist on jQuery.swipe.options")
                        }
                    }
                }
                return null
            };
            function aN(be) {
                if (aB()) {
                    return
                }
                if (f(be.target).closest(au.excludedElements, aR).length > 0) {
                    return
                }
                var bf = be.originalEvent ? be.originalEvent : be;
                var bd, bg = bf.touches, bc = bg ? bg[0] : bf;
                aa = g;
                if (bg) {
                    X = bg.length
                } else {
                    if (au.preventDefaultEvents !== false) {
                        be.preventDefault()
                    }
                }
                ag = 0;
                aP = null;
                a2 = null;
                aJ = null;
                ac = 0;
                a1 = 0;
                aZ = 0;
                H = 1;
                ap = 0;
                N = ab();
                S();
                ai(0, bc);
                if (!bg || (X === au.fingers || au.fingers === i) || aX()) {
                    U = ar();
                    if (X == 2) {
                        ai(1, bg[1]);
                        a1 = aZ = at(aQ[0].start, aQ[1].start)
                    }
                    if (au.swipeStatus || au.pinchStatus) {
                        bd = P(bf, aa)
                    }
                } else {
                    bd = false
                }
                if (bd === false) {
                    aa = q;
                    P(bf, aa);
                    return bd
                } else {
                    if (au.hold) {
                        af = setTimeout(f.proxy(function () {
                            aR.trigger("hold", [bf.target]);
                            if (au.hold) {
                                bd = au.hold.call(aR, bf, bf.target)
                            }
                        }, this), au.longTapThreshold)
                    }
                    an(true)
                }
                return null
            }
            function a4(bf) {
                var bi = bf.originalEvent ? bf.originalEvent : bf;
                if (aa === h || aa === q || al()) {
                    return
                }
                var be, bj = bi.touches, bd = bj ? bj[0] : bi;
                var bg = aH(bd);
                a3 = ar();
                if (bj) {
                    X = bj.length
                }
                if (au.hold) {
                    clearTimeout(af)
                }
                aa = k;
                if (X == 2) {
                    if (a1 == 0) {
                        ai(1, bj[1]);
                        a1 = aZ = at(aQ[0].start, aQ[1].start)
                    } else {
                        aH(bj[1]);
                        aZ = at(aQ[0].end, aQ[1].end);
                        aJ = aq(aQ[0].end, aQ[1].end)
                    }
                    H = a8(a1, aZ);
                    ap = Math.abs(a1 - aZ)
                }
                if ((X === au.fingers || au.fingers === i) || !bj || aX()) {
                    aP = aL(bg.start, bg.end);
                    a2 = aL(bg.last, bg.end);
                    ak(bf, a2);
                    ag = aS(bg.start, bg.end);
                    ac = aM();
                    aI(aP, ag);
                    be = P(bi, aa);
                    if (!au.triggerOnTouchEnd || au.triggerOnTouchLeave) {
                        var bc = true;
                        if (au.triggerOnTouchLeave) {
                            var bh = aY(this);
                            bc = F(bg.end, bh)
                        }
                        if (!au.triggerOnTouchEnd && bc) {
                            aa = aC(k)
                        } else {
                            if (au.triggerOnTouchLeave && !bc) {
                                aa = aC(h)
                            }
                        }
                        if (aa == q || aa == h) {
                            P(bi, aa)
                        }
                    }
                } else {
                    aa = q;
                    P(bi, aa)
                }
                if (be === false) {
                    aa = q;
                    P(bi, aa)
                }
            }
            function M(bc) {
                var bd = bc.originalEvent ? bc.originalEvent : bc, be = bd.touches;
                if (be) {
                    if (be.length && !al()) {
                        G(bd);
                        return true
                    } else {
                        if (be.length && al()) {
                            return true
                        }
                    }
                }
                if (al()) {
                    X = ay
                }
                a3 = ar();
                ac = aM();
                if (bb() || !am()) {
                    aa = q;
                    P(bd, aa)
                } else {
                    if (au.triggerOnTouchEnd || (au.triggerOnTouchEnd == false && aa === k)) {
                        if (au.preventDefaultEvents !== false) {
                            bc.preventDefault()
                        }
                        aa = h;
                        P(bd, aa)
                    } else {
                        if (!au.triggerOnTouchEnd && a7()) {
                            aa = h;
                            aF(bd, aa, B)
                        } else {
                            if (aa === k) {
                                aa = q;
                                P(bd, aa)
                            }
                        }
                    }
                }
                an(false);
                return null
            }
            function ba() {
                X = 0;
                a3 = 0;
                U = 0;
                a1 = 0;
                aZ = 0;
                H = 1;
                S();
                an(false)
            }
            function L(bc) {
                var bd = bc.originalEvent ? bc.originalEvent : bc;
                if (au.triggerOnTouchLeave) {
                    aa = aC(h);
                    P(bd, aa)
                }
            }
            function aK() {
                aR.unbind(K, aN);
                aR.unbind(aD, ba);
                aR.unbind(ax, a4);
                aR.unbind(V, M);
                if (T) {
                    aR.unbind(T, L)
                }
                an(false)
            }
            function aC(bg) {
                var bf = bg;
                var be = aA();
                var bd = am();
                var bc = bb();
                if (!be || bc) {
                    bf = q
                } else {
                    if (bd && bg == k && (!au.triggerOnTouchEnd || au.triggerOnTouchLeave)) {
                        bf = h
                    } else {
                        if (!bd && bg == h && au.triggerOnTouchLeave) {
                            bf = q
                        }
                    }
                }
                return bf
            }
            function P(be, bc) {
                var bd, bf = be.touches;
                if (J() || W()) {
                    bd = aF(be, bc, l)
                }
                if ((Q() || aX()) && bd !== false) {
                    bd = aF(be, bc, t)
                }
                if (aG() && bd !== false) {
                    bd = aF(be, bc, j)
                } else {
                    if (ao() && bd !== false) {
                        bd = aF(be, bc, b)
                    } else {
                        if (ah() && bd !== false) {
                            bd = aF(be, bc, B)
                        }
                    }
                }
                if (bc === q) {
                    if (W()) {
                        bd = aF(be, bc, l)
                    }
                    if (aX()) {
                        bd = aF(be, bc, t)
                    }
                    ba(be)
                }
                if (bc === h) {
                    if (bf) {
                        if (!bf.length) {
                            ba(be)
                        }
                    } else {
                        ba(be)
                    }
                }
                return bd
            }
            function aF(bf, bc, be) {
                var bd;
                if (be == l) {
                    aR.trigger("swipeStatus", [bc, aP || null, ag || 0, ac || 0, X, aQ, a2]);
                    if (au.swipeStatus) {
                        bd = au.swipeStatus.call(aR, bf, bc, aP || null, ag || 0, ac || 0, X, aQ, a2);
                        if (bd === false) {
                            return false
                        }
                    }
                    if (bc == h && aV()) {
                        clearTimeout(aW);
                        clearTimeout(af);
                        aR.trigger("swipe", [aP, ag, ac, X, aQ, a2]);
                        if (au.swipe) {
                            bd = au.swipe.call(aR, bf, aP, ag, ac, X, aQ, a2);
                            if (bd === false) {
                                return false
                            }
                        }
                        switch (aP) {
                            case p:
                            aR.trigger("swipeLeft", [aP, ag, ac, X, aQ, a2]);
                            if (au.swipeLeft) {
                                bd = au.swipeLeft.call(aR, bf, aP, ag, ac, X, aQ, a2)
                            }
                            break;
                            case o:
                            aR.trigger("swipeRight", [aP, ag, ac, X, aQ, a2]);
                            if (au.swipeRight) {
                                bd = au.swipeRight.call(aR, bf, aP, ag, ac, X, aQ, a2)
                            }
                            break;
                            case e:
                            aR.trigger("swipeUp", [aP, ag, ac, X, aQ, a2]);
                            if (au.swipeUp) {
                                bd = au.swipeUp.call(aR, bf, aP, ag, ac, X, aQ, a2)
                            }
                            break;
                            case x:
                            aR.trigger("swipeDown", [aP, ag, ac, X, aQ, a2]);
                            if (au.swipeDown) {
                                bd = au.swipeDown.call(aR, bf, aP, ag, ac, X, aQ, a2)
                            }
                            break
                        }
                    }
                }
                if (be == t) {
                    aR.trigger("pinchStatus", [bc, aJ || null, ap || 0, ac || 0, X, H, aQ]);
                    if (au.pinchStatus) {
                        bd = au.pinchStatus.call(aR, bf, bc, aJ || null, ap || 0, ac || 0, X, H, aQ);
                        if (bd === false) {
                            return false
                        }
                    }
                    if (bc == h && a9()) {
                        switch (aJ) {
                            case c:
                            aR.trigger("pinchIn", [aJ || null, ap || 0, ac || 0, X, H, aQ]);
                            if (au.pinchIn) {
                                bd = au.pinchIn.call(aR, bf, aJ || null, ap || 0, ac || 0, X, H, aQ)
                            }
                            break;
                            case A:
                            aR.trigger("pinchOut", [aJ || null, ap || 0, ac || 0, X, H, aQ]);
                            if (au.pinchOut) {
                                bd = au.pinchOut.call(aR, bf, aJ || null, ap || 0, ac || 0, X, H, aQ)
                            }
                            break
                        }
                    }
                }
                if (be == B) {
                    if (bc === q || bc === h) {
                        clearTimeout(aW);
                        clearTimeout(af);
                        if (Z() && !I()) {
                            O = ar();
                            aW = setTimeout(f.proxy(function () {
                                O = null;
                                aR.trigger("tap", [bf.target]);
                                if (au.tap) {
                                    bd = au.tap.call(aR, bf, bf.target)
                                }
                            }, this), au.doubleTapThreshold)
                        } else {
                            O = null;
                            aR.trigger("tap", [bf.target]);
                            if (au.tap) {
                                bd = au.tap.call(aR, bf, bf.target)
                            }
                        }
                    }
                } else {
                    if (be == j) {
                        if (bc === q || bc === h) {
                            clearTimeout(aW);
                            clearTimeout(af);
                            O = null;
                            aR.trigger("doubletap", [bf.target]);
                            if (au.doubleTap) {
                                bd = au.doubleTap.call(aR, bf, bf.target)
                            }
                        }
                    } else {
                        if (be == b) {
                            if (bc === q || bc === h) {
                                clearTimeout(aW);
                                O = null;
                                aR.trigger("longtap", [bf.target]);
                                if (au.longTap) {
                                    bd = au.longTap.call(aR, bf, bf.target)
                                }
                            }
                        }
                    }
                }
                return bd
            }
            function am() {
                var bc = true;
                if (au.threshold !== null) {
                    bc = ag >= au.threshold
                }
                return bc
            }
            function bb() {
                var bc = false;
                if (au.cancelThreshold !== null && aP !== null) {
                    bc = (aT(aP) - ag) >= au.cancelThreshold
                }
                return bc
            }
            function ae() {
                if (au.pinchThreshold !== null) {
                    return ap >= au.pinchThreshold
                }
                return true
            }
            function aA() {
                var bc;
                if (au.maxTimeThreshold) {
                    if (ac >= au.maxTimeThreshold) {
                        bc = false
                    } else {
                        bc = true
                    }
                } else {
                    bc = true
                }
                return bc
            }
            function ak(bc, bd) {
                if (au.preventDefaultEvents === false) {
                    return
                }
                if (au.allowPageScroll === m) {
                    bc.preventDefault()
                } else {
                    var be = au.allowPageScroll === s;
                    switch (bd) {
                        case p:
                        if ((au.swipeLeft && be) || (!be && au.allowPageScroll != E)) {
                            bc.preventDefault()
                        }
                        break;
                        case o:
                        if ((au.swipeRight && be) || (!be && au.allowPageScroll != E)) {
                            bc.preventDefault()
                        }
                        break;
                        case e:
                        if ((au.swipeUp && be) || (!be && au.allowPageScroll != u)) {
                            bc.preventDefault()
                        }
                        break;
                        case x:
                        if ((au.swipeDown && be) || (!be && au.allowPageScroll != u)) {
                            bc.preventDefault()
                        }
                        break
                    }
                }
            }
            function a9() {
                var bd = aO();
                var bc = Y();
                var be = ae();
                return bd && bc && be
            }
            function aX() {
                return !!(au.pinchStatus || au.pinchIn || au.pinchOut)
            }
            function Q() {
                return !!(a9() && aX())
            }
            function aV() {
                var bf = aA();
                var bh = am();
                var be = aO();
                var bc = Y();
                var bd = bb();
                var bg = !bd && bc && be && bh && bf;
                return bg
            }
            function W() {
                return !!(au.swipe || au.swipeStatus || au.swipeLeft || au.swipeRight || au.swipeUp || au.swipeDown)
            }
            function J() {
                return !!(aV() && W())
            }
            function aO() {
                return((X === au.fingers || au.fingers === i) || !a)
            }
            function Y() {
                return aQ[0].end.x !== 0
            }
            function a7() {
                return !!(au.tap)
            }
            function Z() {
                return !!(au.doubleTap)
            }
            function aU() {
                return !!(au.longTap)
            }
            function R() {
                if (O == null) {
                    return false
                }
                var bc = ar();
                return(Z() && ((bc - O) <= au.doubleTapThreshold))
            }
            function I() {
                return R()
            }
            function aw() {
                return((X === 1 || !a) && (isNaN(ag) || ag < au.threshold))
            }
            function a0() {
                return((ac > au.longTapThreshold) && (ag < r))
            }
            function ah() {
                return !!(aw() && a7())
            }
            function aG() {
                return !!(R() && Z())
            }
            function ao() {
                return !!(a0() && aU())
            }
            function G(bc) {
                a6 = ar();
                ay = bc.touches.length + 1
            }
            function S() {
                a6 = 0;
                ay = 0
            }
            function al() {
                var bc = false;
                if (a6) {
                    var bd = ar() - a6;
                    if (bd <= au.fingerReleaseThreshold) {
                        bc = true
                    }
                }
                return bc
            }
            function aB() {
                return !!(aR.data(C + "_intouch") === true)
            }
            function an(bc) {
                if (!aR) {
                    return
                }
                if (bc === true) {
                    aR.bind(ax, a4);
                    aR.bind(V, M);
                    if (T) {
                        aR.bind(T, L)
                    }
                } else {
                    aR.unbind(ax, a4, false);
                    aR.unbind(V, M, false);
                    if (T) {
                        aR.unbind(T, L, false)
                    }
                }
                aR.data(C + "_intouch", bc === true)
            }
            function ai(be, bc) {
                var bd = {start: {x: 0, y: 0}, last: {x: 0, y: 0}, end: {x: 0, y: 0}};
                bd.start.x = bd.last.x = bd.end.x = bc.pageX || bc.clientX;
                bd.start.y = bd.last.y = bd.end.y = bc.pageY || bc.clientY;
                aQ[be] = bd;
                return bd
            }
            function aH(bc) {
                var be = bc.identifier !== undefined ? bc.identifier : 0;
                var bd = ad(be);
                if (bd === null) {
                    bd = ai(be, bc)
                }
                bd.last.x = bd.end.x;
                bd.last.y = bd.end.y;
                bd.end.x = bc.pageX || bc.clientX;
                bd.end.y = bc.pageY || bc.clientY;
                return bd
            }
            function ad(bc) {
                return aQ[bc] || null
            }
            function aI(bc, bd) {
                bd = Math.max(bd, aT(bc));
                N[bc].distance = bd
            }
            function aT(bc) {
                if (N[bc]) {
                    return N[bc].distance
                }
                return undefined
            }
            function ab() {
                var bc = {};
                bc[p] = av(p);
                bc[o] = av(o);
                bc[e] = av(e);
                bc[x] = av(x);
                return bc
            }
            function av(bc) {
                return{direction: bc, distance: 0}
            }
            function aM() {
                return a3 - U
            }
            function at(bf, be) {
                var bd = Math.abs(bf.x - be.x);
                var bc = Math.abs(bf.y - be.y);
                return Math.round(Math.sqrt(bd * bd + bc * bc))
            }
            function a8(bc, bd) {
                var be = (bd / bc) * 1;
                return be.toFixed(2)
            }
            function aq() {
                if (H < 1) {
                    return A
                } else {
                    return c
                }
            }
            function aS(bd, bc) {
                return Math.round(Math.sqrt(Math.pow(bc.x - bd.x, 2) + Math.pow(bc.y - bd.y, 2)))
            }
            function aE(bf, bd) {
                var bc = bf.x - bd.x;
                var bh = bd.y - bf.y;
                var be = Math.atan2(bh, bc);
                var bg = Math.round(be * 180 / Math.PI);
                if (bg < 0) {
                    bg = 360 - Math.abs(bg)
                }
                return bg
            }
            function aL(bd, bc) {
                var be = aE(bd, bc);
                if ((be <= 45) && (be >= 0)) {
                    return p
                } else {
                    if ((be <= 360) && (be >= 315)) {
                        return p
                    } else {
                        if ((be >= 135) && (be <= 225)) {
                            return o
                        } else {
                            if ((be > 45) && (be < 135)) {
                                return x
                            } else {
                                return e
                            }
                        }
                    }
                }
            }
            function ar() {
                var bc = new Date();
                return bc.getTime()
            }
            function aY(bc) {
                bc = f(bc);
                var be = bc.offset();
                var bd = {left: be.left, right: be.left + bc.outerWidth(), top: be.top, bottom: be.top + bc.outerHeight()};
                return bd
            }
            function F(bc, bd) {
                return(bc.x > bd.left && bc.x < bd.right && bc.y > bd.top && bc.y < bd.bottom)
            }
        }
    }
    ));
!function (n) {
    "use strict";
    n.fn.bsTouchSlider = function (i) {
        var a = n(".carousel");
        return this.each(function () {
            function i(i) {
                var a = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
                i.each(function () {
                    var i = n(this), t = i.data("animation");
                    i.addClass(t).one(a, function () {
                        i.removeClass(t)
                    })
                })
            }
            var t = a.find(".item:first").find("[data-animation ^= 'animated']");
            a.carousel(), i(t), a.on("slide.bs.carousel", function (a) {
                var t = n(a.relatedTarget).find("[data-animation ^= 'animated']");
                i(t)
            }), n(".carousel .carousel-inner").swipe({swipeLeft: function (n, i, a, t, e) {
                this.parent().carousel("next")
            }, swipeRight: function () {
                this.parent().carousel("prev")
            }, threshold: 0})
        })
    }
}(jQuery);
    //Initialise Bootstrap Carousel Touch Slider
    // Curently there are no option available.

    $('#bootstrap-touch-slider').bsTouchSlider();
</script>
@endsection