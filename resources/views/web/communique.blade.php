@extends('layouts.web')
<?php
$sugestions = App\Communique::latest()->where('id', '!=', $data->id)->take(7)->get();
?>
@section('description')
{{ isset($data->titre) ? ($data->titre) : $data->contenu}}
@endsection
@section('plugin')
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.9&appId=1214967528615548";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
@endsection
@section('script')
<!--<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">-->
<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/js/bootstrap.min.js"></script>

<script src="{{asset('assets/plugins/photo-gallery/photo-gallery.js')}}"></script>
<script>
    !function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
        }
    }(document, 'script', 'twitter-wjs');
</script>
@endsection
@section('content')
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">         
            <div class="modal-body">                
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="container margin-top-5">

    <div class="row">
        <div class="pad15">
            <div class="  col-lg-8   margin-bottom-20   text-justify" >

                <?php
                if (isset($data)) {
                    ?>
                    <div>
                        <div class="headline"><h2 class="light rs  mtop20">{{ isset($data->titre) ? ($data->titre) : ""}}</h2></div>

                     </div>
                    
                    <div class="pad50 bga">

                    </div>
                    <!--hh '.($data->user()?$data->user->prenom.' '.$data->user->nom:'').' |-->
                    {!!isset($data->chapeau)?'<blockquote class="text-sm"><p class=" "  >'.$data->chapeau.'</p><small> </small></blockquote cl>':''!!}
                    <ul class="list-inline m0 pad0 text-sm   text-muted f12">

                        <li>
                            <i class="glyphicon glyphicon-calendar"></i> Publié {{$data->created_at->diffForHumans()}}
                        </li>

                        @if($data->created_at!=$data->updated_at)
                        <li><i class="glyphicon glyphicon-refresh"></i>  Dernière mise à jour {{$data->updated_at->diffForHumans()}} </li>
                        @endif
                    </ul>
                    <div class="actu-content-body" style="font-size: 20px !important"  id="activites">
                        {!!isset($data->contenu)?($data->contenu):''!!}

                    </div>

                    <div class="clearfix">
                        <div class="col-sm-12">
                            <div class="pad15">
                                <div class="fb-like" data-href="{!!Request::url()!!}" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                            </div>
                            <div class="pad15">

                                <div class="fb-comments" width="100%" data-href="{!!Request::url()!!}" data-numposts="4"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix ">
                        <div class="col-sm-6 col-xs-6">
                            @if(isset($prev)&& $prev!=NULL)

                            <div title="{{$prev->titre}}" class="text-left pull-left actu_nv" >
                                <a class="text-white" href="{{url('activite/'.$prev->slug)}}">
                                    <div class="rowbg">
                                        <div class="col-xs-3 hidden-xs col-md-1  text-left  pad0 ">
                                            <i  class="glyphicon  glyphicon-arrow-left actu_icone"></i>
                                        </div>
                                        <div class="col-xs-12 col-md-10 actu_nvc" >
                                            {{$prev->titre}}
                                        </div>
                                    </div>
                                </a>

                            </div>

                            @endif
                        </div>

                        <div class="col-sm-6 col-xs-6">
                            @if(isset($next) && $next!=NULL)
                            <div title="{{$next->titre}}" class="text-left pull-right actu_nv" >
                                <a class="text-white" href="{{url('activite/'.$next->slug)}}">
                                    <div class="rowbg">
                                        <div class="col-xs-9 col-md-10 actu_nvc" >
                                            {{$next->titre}}
                                        </div>
                                        <div class="col-xs-3 col-md-1  ">
                                            <i  class="glyphicon glyphicon-arrow-right actu_icone"></i>
                                        </div>
                                    </div>
                                </a>

                            </div>
                            @endif
                        </div>
                    </div>
                    <hr class="hr-xs">
                    <!--                <ul class="up-post-shares list-inline pull-right">
                                        <li><a href="#"><i class="rounded-x icon-share"></i></a></li>
                                        <li><a href="#"><i class="rounded-x icon-like"></i></a></li>
                                    </ul>-->

                    <?php
                }
                $in = 0;
                ?>


                <div class="col-sm-">
                    @if(isset($sugestions) && $sugestions->count())
                    <div class="headline light "><h4>Autres communiqués à lire</h4> </div> 
                    <div class="row margin-bottom-20 ">

                        @foreach($sugestions as $data)
                        <div class="" > 
                            <div class=" item margin-top-15  col-xs-1 col-sm-12 actu_height">
                                <div class="news-v1-ins   ">
                                   
                                    <div class="pad5_0  ">
                                        <div class="actu-titres">
                                            <div class="m0 pad0">
                                                <a class="text-primary text-md " href="{{URL('communique/'.$data->slug)}}">-  {{isset($data->titre) ? substr($data->titre,0, 142 ).'...' : ""}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <br>
                        <br>
                    </div>
                    @endif
                    <br>
                </div>
            </div>
        </div>
        <div class="col-md-4 Rpanel">
            @include('partials.right')
        </div>

    </div>
</div>


</div>
<script>

    $(function () {
        var UPWA = {

            fontSizePlus: function (id, maxsize, pas) {
                if (pas === undefined) {
                    pas = 2;
                }
                ;
                var $this = $(id), max = parseInt(maxsize),
                        fsn = parseInt($this.css("fontSize"));
                if (fsn <= max) {
                    var p = fsn + pas;
                    $this.css("fontSize", p + "px");
                }
            },
            fontSizeMoins: function (id, minsize, pas) {
                if (pas === undefined) {
                    pas = 2;
                }
                ;
                var $this = $(id), min = parseInt(minsize),
                        fsn = parseInt($this.css("fontSize"));
                if (fsn >= min) {
                    var p = fsn - pas;
                    $this.css("fontSize", p + "px");
                }
            }
        };
        $(".fontsize-moins").click(function (e) {

            e.preventDefault();
            UPWA.fontSizeMoins($(this).attr('rel'), $(this).attr('data-min'));
            return false;
        });
        $(".fontsize-plus").click(function (e) {
            e.preventDefault();
            UPWA.fontSizePlus($(this).attr('rel'), $(this).atr('data-max'), 2);
            return false;
        });
//       
    })
</script>

@endsection
