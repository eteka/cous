@extends('layouts.web')
@section('title')
Emplois
@endsection
@section('content')

<div class="container bgwhite margin-top-5  ">
    <div class="row">
        <div class="col-lg-8 col-sm-8">
            <h3 class="light  page-header up-page-header"><?= isset($pcontenu['titre']) ? $this->escape($pcontenu["titre"]) : ''; ?></h3>
            <div class="pad10 bgwhite shadow1 main-content">
                <div class="menu-html-content mtop-10">
                    <?= isset($pcontenu['contenu']) ? ($pcontenu["contenu"]) : ''; ?>

                </div> 
            </div>
        </div>
        <div class="col-lg-4 col-sm-4   ">
            @include("includes/infosright");
            

        </div>
    </div>
</div>

@endsection
