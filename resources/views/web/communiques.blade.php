@extends('layouts.web')
@section('title')
Nos activités
@endsection
@section('content')

<div class=" container">
    <div class="row">
        <div class="pad15">
            <div class="col-lg-8 col-sm-8">
                <div class="headline"><h2 class="light rs   text-uppercase mtop20">Communiqués du COUS Parakou</h2></div>
                <form method="get" class="mtop10 hidden" action="{{url('search')}}">
                    <div class="input-group">
                        <input type="hidden" name="type" value="activite">
                        <input type="text" name="q" class="form-control  search-control" placeholder="En quoi pouvons nous vous aider... ?">
                        <span class="input-group-btn">
                            <button class="btn btn-default search-btn"><i class="icon-magnifier"></i></button>
                        </span>
                    </div>
                </form>
                <?php
                if (isset($datas)) {
                    ?>

                    <div class="owl-carousels2 rows oswl-loaded" >   
                        <?php
                        if (count($datas) > 0) {
                            $in = -1;
                            foreach ($datas as $cle => $data) {
                                ?>
                                <div class=" items cous-actu col-xs-12 col-md-12 margin-top-20 ">
                                    <div class="row news-v1-ins borderBote   ">

                                        <div class="col-sm-12">

                                            <!--<div class="news-categorie bgcol1">Campus</div>-->
                                            <div class="pad5">
                                                <div class="actu-titre">
                                                    <h4 class="">
                                                        <a class="color1 light-3  text-success"  href="{{URL('communique/'.$data->slug)}}">{{isset($data->titre) ? str_limit($data->titre, 65 ) : ""}}</a></h4>
                                                </div>

                                                <p class="text-sm">                                                    
                                                    <i> <a class="text-muted" href="{{URL('communique/'.$data->slug)}}">  {!! isset($data->contenu) && strlen($data->contenu)  ? str_limit(strip_tags($data->contenu), 300): str_limit(substr($data->contenu, 300))!!}</a></i>
                                                </p>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        
                                        <div class="col-sm-4 ">
                                            <date class="text-muted mtop10 btn-sm text-center  btn-block btn-default borderc rond30"><i class="fa fa-calendar"></i> Ajouté  {{$data->created_at->diffForHumans()}}</date>
                                        </div>
                                        <div class=" col-sm-4  actu-content text-sm    ">
                                            @if($data->fichier)
                                           <a href="{{asset($data->fichier)}}" class="text-muted mtop10 btn-sm text-center  btn-block btn-success rond30 text-xss"><i class="fa fa-download"></i>  <span class="text boldtext-info"> Télécharger le fichier </span>
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <div class="well text-center margin-top-30 text-muted">
                                <i class="fa fa-newspaper-o  huge-data-fa margin-top-30" ></i>
                                <h2 class="light mtop20" >Aucun document pour le moment</h2 >
                                <br><br>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="clearfix row activite-link">
                        <div class="pad text-right ">
                            {{$datas->links()}}
                        </div>
                    </div>
                    <?php
                }
                ?>

            </div>

            <div class="col-md-4 Rpanel">
                @include('partials.right')
            </div>
        </div>
    </div>
</div>

@endsection
