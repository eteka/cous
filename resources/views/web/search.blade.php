@extends('layouts.web')
@section('title')
Nos activités
@endsection
@section('content')

<div class=" container">
    <div class="row">
        <div class="pad15">
            <div class="col-lg-8 col-sm-8">
                <div class="headline"><h2 class="light rs   text-center mtop20">Résultat de la rechercher </h2></div>
                <form method="get" class="mtop10 " action="{{url('search')}}">
                    <div class="input-group">
                        <input type="hidden" name="type" value="activite">
                        <input type="text" name="query" class="form-control  search-control" value='{{isset($_GET['query'])? $_GET['query']:NULL}}' placeholder="Rechercher... ?">
                        <span class="input-group-btn">
                            <button class="btn btn-default search-btn"><i class="fa fa-search"></i> <span class="hidden-sm hidden-xs">Rechercher</span></button>
                        </span>
                    </div>
                </form>
                <?php
                
                if (isset($activites)) {
                    ?>

                    <div class="owl-carousels2 rows oswl-loaded" >   
                        <?php
                        
                        if (count($activites) > 0) {
                            
                            $in = -1;
                            foreach ($activites as $cle => $actu) {
                                $in++;
                                if ($in == 5) {
                                    $in = 0;
                                }
                                
//                            $nc = "color" . $in;
//                            $date1 = time();
//                            $diff_date = abs($date1 - strtotime($actu->created_at)) / (60 * 60 * 24);
//                            $diff_update = abs($date1 - strtotime($actu->updated_at)) / (60 * 60 * 24);
                                ?>
                                <div class=" items cous-actu col-xs-12 col-md-12 margin-top-20 ">
                                    <div class="row news-v1-ins borderBote   ">

                                        <div class="col-sm-12">

                                            <!--<div class="news-categorie bgcol1">Campus</div>-->
                                            <div class="pad5">
                                                <div class="actu-titre">
                                                    <h4 class="bold">
                                                        <a class="color1 light-3"  href="{{URL('activite/'.$actu->slug)}}">{{isset($actu->titre) ? str_limit($actu->titre, 65 ) : ""}}</a></h4>
                                                </div>
                                                <div class="cactu_image" >
                                                    <?php
                                                    if (isset($actu->photo) && !empty($actu->photo)) {
                                                        ?>
                                                        <a href="{{URL('activite/'.$actu->slug)}}">
                                                            <img class="img-responsives pull-left news-image " src="{{asset($actu->photo)}}" alt="{{isset($actu->titre) ? (strlen($actu->titre) > 50) ? substr($actu->titre, 0, 50) . "..." : $actu->titre : ""}}"></a>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <a href="{{URL('activite/'.$actu->slug)}}">
                                                            <img class="img-  pull-left news-image " src="{{asset("assets/images/static/actu-newspaper.jpg")}}" alt="{{ isset($actu->titre) ? (strlen($actu->titre) > 50) ? substr($actu->titre, 0, 50) . "..." : $actu->titre : ""}}"></a>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                                <p class="  actu-content text-sm  ">                                                    
                                                    {!! isset($actu->chapeau) && strlen($actu->chapeau)  ? str_limit(strip_tags($actu->chapeau), 300): str_limit(substr($actu->contenu, 300))!!}
                                                    <br><a href="{{URL('activite/'.$actu->slug)}}" class="text-muted mtop10 btn-xs pad5 btn btn-default rond30 text-xss"><span class="text bold text-info">Lire la suite >></span>
                                                    </a>

                                                </p>
                                                <ul class="list-inline text-muted text-xs pull- news-v1-infos ">

                                                                        <!--<li><span></span> <a class="text-sm f11 text-muted" href="#"><author></author></a></li>-->
                                                    <!--<li>|</li>-->
                                                    <li><date class="text-sm f11 text-muted">Posté {{$actu->created_at->diffForHumans()}}</date></li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <div class="well text-center mtop10 margin-top-30 text-muted">
                                <i class="fa fa-newspaper-o  huge-data-fa margin-top-30" ></i>
                                <h2 class="light mtop20" >Aucune information trouvée<br><small class="text-sm "><i class="fa fa-repeat"></i> Réessayez avec un nouveau mot clé</small></h2 >
                                <br><br>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
                <div class="clearfix row activite-link">
                    <div class="pad text-right ">
                        {{$activites->links()}}
                    </div>
                </div>
            </div>

            <div class="col-md-4 Rpanel">
                @include('partials.right')
            </div>
        </div>
    </div>
</div>

@endsection
