@extends('layouts.web')
<?php
$sugestions = App\Activite::latest()->where('id', '!=', $actu->id)->take(5)->get();
?>
@section('description')
{{ isset($actu->titre) ? ($actu->titre) : $actu->corps}}
@endsection
@section('plugin')
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.9&appId=1214967528615548";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
@endsection
@section('script')
<!--<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">-->
<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/js/bootstrap.min.js"></script>

<script src="{{asset('assets/plugins/photo-gallery/photo-gallery.js')}}"></script>
<script>
    !function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
        }
    }(document, 'script', 'twitter-wjs');
</script>
@endsection
@section('content')
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">         
            <div class="modal-body">                
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="container margin-top-5">

    <div class="row">
        <div class="pad15">
            <div class="  col-lg-8   margin-bottom-20   text-justify" >

                <?php
                if (isset($actu)) {
                    ?>
                    <h3  class=" pad10_0 color1 huge-up light-3 m0 page-headers">
                        {{ isset($actu->titre) ? ($actu->titre) : ""}}</h3 >

                    <div>
                        @if (isset($actu->photo) && $actu->photo != NULL) 

                        <img  class="img-responsive news-image " src="<?php echo asset($actu->photo); ?>" alt="<?= $actu->titre ? (strlen($actu->titre) > 50) ? substr($actu->titre, 0, 50) . "..." : $actu->titre : ""; ?>">

                        @else 
                        <img class="img-responsive news-image " src="<?php echo asset("assets/images/static/actu-newspaper.jpg"); ?>" alt="<?= $actu->titre ? (strlen($actu->titre) > 50) ? substr($actu->titre, 0, 50) . "..." : $actu->titre : ""; ?>">
                        @endif

                        <div class="pad50"><small class=" text-muted "><h5 class=""><i>{{isset($actu->legend)?'['.($actu->legend).']':''}}</i></h5></small></div>

                    </div>
                    <div class="row hidden">
                        <div class="col-sm-12">
                            <!--                                                <ul class="list-inline">
                                                                                <li> <script src="https://apis.google.com/js/platform.js" async defer>
                                                        {
                                                            lang: 'fr'
                                                        }
                                                                                    </script><div class="g-plus" data-action="share"></div>    
                                                                                </li>
                                                                                <li>
                                                                                    <div class="fb-share-button" data-href="{{url('activite/'.$actu->slug)}}" data-layout="button_count" data-mobile-iframe="true"></div>
                                                    
                                                                                </li>
                                                                                <li>
                                                                                    <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
                                                                                </li>
                                                                                <li >
                                                                                    <a href="{{ url('activite/preview/',$actu->slug)}}" target="_blanck" class="print-dialog"><i class="glyphicon glyphicon-print"></i> </a>
                                                                                </li>
                                                                                <li id="policeh1" class="police11"><a href="#policeh1" rel="#activites" data-min="10"  data-max="45" class="fontsize-moins"><i class="glyphicon glyphicon-font"></i> </a></li>
                                                                                <li id="policeh2" class="police15"><a href="#policeh1" rel="#activites" data-min="12"  data-max="45" class="fontsize-plus"><i class="glyphicon glyphicon-font"></i> </a></li>
                                                    
                                                                            </ul>-->
                        </div>
                    </div>
                    <div class="pad50 bga">
                        <?php
                        $categorie = $actu->categorie ? $actu->categorie->nom : '';
                        $bg = '#000';
//                    $bg = App\Helpers\DataHelper::stringToColorCode($categorie);
                        ?>



                    </div>
                    <!--hh '.($actu->user()?$actu->user->prenom.' '.$actu->user->nom:'').' |-->
                    {!!isset($actu->chapeau)?'<blockquote class="text-sm"><p class=" "  >'.$actu->chapeau.'</p><small> </small></blockquote cl>':''!!}
                    <ul class="list-inline m0 pad0 text-sm   text-muted f12">

                        <li>
                            <i class="glyphicon glyphicon-calendar"></i> Publié le {{date('d/m/Y à H:i',strtotime($actu->created_at))}} 
                        </li>

                        @if($actu->created_at!=$actu->updated_at)
                        <li><i class="glyphicon glyphicon-refresh"></i>  Dernière mise à jour {{date('d/m/Y',strtotime($actu->updated_at))}} </li>
                        @endif

                        <li>
                            <!--<i class="glyphicon glyphicon-eye-open"></i> Vue {{intval($actu->vues)}} fois-->

                        </li>
                    </ul>
                    <div class="actu-content-body" style="font-size: 20px !important"  id="activites">
                        {!!isset($actu->corps)?($actu->corps):''!!}

                    </div>

                    <div class="clearfix">
                        <div class="col-sm-12">
                            <div class="pad15">
                                <div class="fb-like" data-href="{!!Request::url()!!}" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                            </div>
                            <div class="pad15">

                                <div class="fb-comments" width="100%" data-href="{!!Request::url()!!}" data-numposts="4"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix ">
                        <div class="col-sm-6 col-xs-6">
                            @if(isset($prev)&& $prev!=NULL)

                            <div title="{{$prev->titre}}" class="text-left pull-left actu_nv" >
                                <a class="text-white" href="{{url('activite/'.$prev->slug)}}">
                                    <div class="rowbg">
                                        <div class="col-xs-3 hidden-xs col-md-1  text-left  pad0 ">
                                            <i  class="glyphicon  glyphicon-arrow-left actu_icone"></i>
                                        </div>
                                        <div class="col-xs-12 col-md-10 actu_nvc" >
                                            {{$prev->titre}}
                                        </div>
                                    </div>
                                </a>

                            </div>

                            @endif
                        </div>

                        <div class="col-sm-6 col-xs-6">
                            @if(isset($next) && $next!=NULL)
                            <div title="{{$next->titre}}" class="text-left pull-right actu_nv" >
                                <a class="text-white" href="{{url('activite/'.$next->slug)}}">
                                    <div class="rowbg">
                                        <div class="col-xs-9 col-md-10 actu_nvc" >
                                            {{$next->titre}}
                                        </div>
                                        <div class="col-xs-3 col-md-1  ">
                                            <i  class="glyphicon glyphicon-arrow-right actu_icone"></i>
                                        </div>
                                    </div>
                                </a>

                            </div>
                            @endif
                        </div>
                    </div>

                    <?php
                }
                $in = 0;
                ?>
                <div class="col-sm-">
                    @if(isset($sugestions) && $sugestions->count())
                    <div class="headline text-center">
                        <h2 class="light rs   text-center mtop20">
                            <i class="fa fa-newspaper-o"></i> Autres articles à lire
                        </h2>
                    </div>
                    <div class="row margin-bottom-20 ">

                        @foreach($sugestions as $actu)
                        <div class="" > 
                            <?php
                            $in++;
                            if ($in == 5) {
                                $in = 0;
                            }
                            $date1 = time();
                            $diff_date = abs($date1 - strtotime($actu->created_at)) / (60 * 60 * 24);
                            $diff_update = abs($date1 - strtotime($actu->updated_at)) / (60 * 60 * 24);
                            $nc = "color" . $in;
                            //$actu = $lactu;
                            ?>
                            <div class=" item margin-top-15  col-sm-12 actu_height">
                                <div class="news-v1-ins">

                                    <div class="pad5_0  ">
                                        <div class="actu-titres text-muted">
                                            <div class="m0 pad0">
                                                <a class="text-muted  " href="{{URL('activite/'.$actu->slug)}}"> {{isset($actu->titre) ? substr($actu->titre,0, 142 ) : ""}} <span class="text-primary text-sm">Lire l'activité [...]</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <br>
                        <br>
                    </div>
                    @endif
                    <br>
                </div>
            </div>
        </div>
        <div class="col-md-4 Rpanel">
            @include('partials.right')
        </div>

    </div>
</div>


</div>
<script>

    $(function () {
        var UPWA = {

            fontSizePlus: function (id, maxsize, pas) {
                if (pas === undefined) {
                    pas = 2;
                }
                ;
                var $this = $(id), max = parseInt(maxsize),
                        fsn = parseInt($this.css("fontSize"));
                if (fsn <= max) {
                    var p = fsn + pas;
                    $this.css("fontSize", p + "px");
                }
            },
            fontSizeMoins: function (id, minsize, pas) {
                if (pas === undefined) {
                    pas = 2;
                }
                ;
                var $this = $(id), min = parseInt(minsize),
                        fsn = parseInt($this.css("fontSize"));
                if (fsn >= min) {
                    var p = fsn - pas;
                    $this.css("fontSize", p + "px");
                }
            }
        };
        $(".fontsize-moins").click(function (e) {

            e.preventDefault();
            UPWA.fontSizeMoins($(this).attr('rel'), $(this).attr('data-min'));
            return false;
        });
        $(".fontsize-plus").click(function (e) {
            e.preventDefault();
            UPWA.fontSizePlus($(this).attr('rel'), $(this).atr('data-max'), 2);
            return false;
        });
//       
    })
</script>

@endsection
