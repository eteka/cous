@extends('layouts.web')
@section('title')
Nos activités
@endsection
@section('content')

<div class=" container">
    <div class="row">
        <div class="pad15">
            <div class="col-lg-8 col-sm-8">
                <div class="headline"><h2 class="light rs   text-center mtop20">Délibération des allocations universitaires</h2></div>
                <form method="get" class="mtop10 " action="{{url('deliberations')}}">
                    <div class="input-group">
                        <input type="text" name="query" value="{{isset($_GET['query'])?$_GET['query']:NULL}}" class="form-control rond0   search-control" placeholder="Rechercher...">
                        <span class="input-group-btn">
                            <button class="btn btn-primary search-btn rond0"><i class="fa fa-search"></i> Rechercher</button>
                        </span>
                    </div>                    
                    <div class="clearfix text-xs mtop10 bgf7">
                        
                        <div class="col-sm-3">
                            <div class="pad10">
                                Type <br>
                                {!! Form::select('type', isset($itypes)?$itypes:[],0, ['class' => 'form-control input-sm']) !!}
                            
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="pad10">
                                Année académique<br>
                                {!! Form::select('annee_acad', isset($iannee_acad)?$iannee_acad:[],0, ['class' => 'form-control input-sm']) !!}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="pad10">
                                Etablissement <br>
                                {!! Form::select('etab', isset($ietabs)?$ietabs:[],0, ['class' => 'form-control input-sm']) !!}
                
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="pad10">
                                Année d'étude <br>
                                <select class="form-control input-sm" name="annee">
                                <option value="0">Toutes les années</option>
                                <option value="1">1ière Année</option>
                                <option value="2">2ième Année</option>
                                <option value="3">3ième Année</option>
                            </select>
                            </div>
                        </div>
                    </div>
                </form>
                <?php
                if (isset($datas)) {
                    ?>

                    <div class="owl-carousels2 rows oswl-loaded" >   
                        <?php
                        if (count($datas) > 0) {
                            $in = -1;
                            foreach ($datas as $cle => $actu) {
                                $in++;
                                if ($in == 5) {
                                    $in = 0;
                                }
//                            $nc = "color" . $in;
//                            $date1 = time();
//                            $diff_date = abs($date1 - strtotime($actu->created_at)) / (60 * 60 * 24);
//                            $diff_update = abs($date1 - strtotime($actu->updated_at)) / (60 * 60 * 24);
                                ?>
                                <?php
                                $fac = ($actu->faculte()->first()->toArray());
                                $anneeAll = [0 => 'Toutes les Années', 1 => '1ière Année', 2 => '2ième Année', 3 => '3ière Année']
                                ?>
                                <div class=" items cous-actu col-xs-12 col-md-12 margin-top-20 ">
                                    <div class="row news-v1-ins borderBote   ">

                                        <div class="col-sm-12">

                                            <!--<div class="news-categorie bgcol1">Campus</div>-->
                                            <div class="pad5">
                                                <div class="actu-titre">
                                                    <h4 class="bold">
                                                        <a class="color1 light-3"  href="{{URL('delibrration/'.$actu->slug)}}">{{isset($actu->nom) ? str_limit($actu->nom, 65 ) : ""}}</a></h4>
                                                </div>
                                                <div class="">
                                                    <ul class="list-inline  text-sm">
                                                        <li>Type:  {{ $actu->type }}</li><li>|</li>
                                                        <li>Année Académique : {{ $actu->annee_academique }}</li><li>|</li>
                                                        <li>Etablissement : {{ $fac['nom_fac'] or '' }}</li><li>|</li>
                                                        <li>Année : {{ $anneeAll[$actu->annee] or '' }}</li>
                                                    </ul>
                                                    @if($actu->info)
                                                    <blockquote class="text-xs">
                                                        {!! isset($actu->info) && strlen($actu->info)  ? str_limit(strip_tags($actu->info), 300): ''!!}
                                                    </blockquote>
                                                    @endif
                                                </div>

                                                <div class="row  text-sm mbottom20  ">
                                                    <div class="col-sm-6">
                                                        <ul class="list-inline text-muted text-xs pull- news-v1-infos ">
                                                            <li><date class="text-sm f11 text-muted"><b><i class="fa fa-calendar"></i> {{$actu->created_at->diffForHumans()}}</date></b></li>

                                                        </ul>
                                                    </div>
                                                    <div class="col-sm-6 text-right">
                                                        <a href="{{asset($actu->fichier)}}" class=" btn btn-primary btn-xs" target="_blanck"><i class=" fa fa-download"></i> <span class="">Télécharger le fichier</span>
                                                        </a>
                                                    </div>


                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <div class="well text-center margin-top-30 text-muted">
                                <i class="fa fa-money  huge-data-fa margin-top-30" ></i>
                                <h2 class="light mtop20" >Aucune délibération trouvée</h2 >
                                <br><br>
                            </div>
                            <?php
                        }
                        ?>
                    </div>

                    <div class="clearfix row activite-link">
                        <div class="pad text-right ">
                            {{$datas->links()}}
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>

            <div class="col-md-4 Rpanel">
                @include('partials.right')
            </div>
        </div>
    </div>
</div>

@endsection
