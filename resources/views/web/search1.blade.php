@extends('layouts.web')
@section('title')
Recherches
@endsection
@section('content')

<div class="container bgwhite margin-top-5  ">
    <div class="row">
        <div class="col-lg-8 col-sm-8">
            <h3 class="light  page-header up-page-header">Recherche<small class="text-muted margin-top-20str_limit f11 pull-right">Résultat de la recherche</small></h3>
            <form method="get" class="pad0" action="{{URL('search')}}">
                <?php
                $type = isset($_GET['type']) ? strtolower(trim($_GET['type'])) : "";
                $query = isset($_GET['q']) ? trim($_GET['q']) : NULL;
                $ac_activite = $activite = $tout = $ac_tout = $document = $ac_document = $annonce = $ac_annonce = $event = $ac_event = '';
                if (!isset($type) || ((isset($type) && $type == 'all') || $type == '')) {
                    $tout = 'checked="checked"';
                    $ac_tout = 'active';
                }

                if (isset($type) && $type == 'activite') {
                    $activite = 'checked="checked"';
                    $ac_activite = 'active';
                }
                if (isset($type) && $type == 'document') {
                    $document = 'checked="checked"';
                    $ac_document = 'active';
                }
                if (isset($type) && $type == 'annonce') {
                    $annonce = 'checked="checked"';
                    $ac_annonce = 'active';
                }

                if (isset($type) && $type == 'event') {
                    $event = 'checked="checked"';
                    $ac_event = 'active';
                }
                ?>
                <div class="input-group">

                    <input type="text" name="q" class="form-control  search-control" value="{{$query}}" placeholder="En quoi pouvons nous vous aider... ?">
                    <span class="input-group-btn">
                        <button class="btn btn-default search-btn"><i class="icon-magnifier"></i> Rechercher</button>
                    </span>
                </div>
                <div class="text-center row">
                    <div class="col-sm-4">
                        <div class="text-xs  f11 pull-left pad50">

                            @if(isset($query) && $query!='')
                            <div class="  chip alert-dismissible" role="alert">

                                <a href="" class="f11" >{{ucfirst($type)}}</a>
                                <button type="button" class="close" ><a href="{{url('search?q=&type=all')}}"><span aria-hidden="true">&times;</span></a></button>

                            </div>
                            @else
                            Choisissez une catégorie
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="btn-group pad50 pull-right" data-toggle="buttons">
                            <label class="btn btn-xs btn-default {{ $ac_tout }}">

                                <input type="radio" name="type" id="t_tout" value="all" autocomplete="off" {{ $tout }} >Tout</label>
                            <label class="btn btn-xs btn-default {{ $ac_activite }}">
                                <input type="radio" name="type" id="t_activites" value="activite" {{ $activite }} autocomplete="off"> Activités</label>
                            <label class="btn btn-xs btn-default {{ $ac_document }}">
                                <input type="radio" name="type" id="t_documents" value="document" {{ $document }} autocomplete="off">Documents
                            </label>
                            <label class="btn btn-xs btn-default {{ $ac_annonce }}">
                                <input type="radio" name="type" id="t_annonces" value="annonce" {{ $annonce }} autocomplete="off">Annonces
                            </label>
                            <label class="btn btn-xs btn-default {{ $ac_event }}">
                                <input type="radio" name="type" id="t_annonces" value="event" {{ $event }} autocomplete="off">Evènements
                            </label>
                        </div>
                    </div>
                </div>
            </form>
            <div class="pad10 bgwhite shadow1 main-content">
                <div class="menu-html-content mtop-10">


                    <div class="tab-v2">
                        <ul class="nav f12 nav-tabs">
                            @if(isset($type) && ($type=='all' || $type=='activite' ))
                            <li class="{{($type=='all' || $type=='activite') ?'active':''}}"><a href="#activite" data-toggle="tab">Activités</a></li>
                            @endif
                            @if(isset($type) && ($type=='all' || $type=='document' ))
                            <li class="{{$type=='document'?'active':''}}"><a href="#document" data-toggle="tab">Documents</a></li>
                            @endif
                            @if(isset($type) && ($type=='all' || $type=='annonce' ))
                            <li class="{{$type=='annonce'?'active':''}}"><a href="#annonce" data-toggle="tab">Annonces</a></li>
                            @endif
                            @if(isset($type) && ($type=='all' || $type=='event' ))
                            <li class="{{$type=='event'?'active':''}}"><a href="#event" data-toggle="tab">Evènements</a></li>
                            @endif
                            @if(isset($type) && ($type=='all' || $type=='autre' ))
                            <li class="{{$type=='autre'?'active':''}}"><a href="#autre" data-toggle="tab">Autres</a></li>
                            @endif
                        </ul>                
                        <div class="tab-content">
                            <div class="tab-pane fade  {{($type=='all' || $type=='activite')?'in active':''}}" id="activite">
                                @include('includes.search_activite')
                            </div>

                            <div class="tab-pane fade {{$type=='document'?'in active':''}}" id="document">
                                 @include('includes.search_document')
                            </div>
                            <div class="tab-pane fade  {{$type=='annonce'?'in active':''}}" id="annonce">
                                @include('includes.search_annonce')
                            </div>
                            <div class="tab-pane fade {{$type=='event'?'in active':''}}" id="event">
                                 @include('includes.search_event')
                            </div>
                            <div class="tab-pane fade  {{$type=='autre'?'in active':''}}" id="autre">
                                @include('includes.search_autre')
                            </div>
                            <!-- End Masking Forms -->
                        </div>
                    </div>



                </div> 
            </div>
        </div>
        <div class="col-lg-4 col-sm-4   ">
            @include("includes/infosright");
        </div>
    </div>
</div>

@endsection
