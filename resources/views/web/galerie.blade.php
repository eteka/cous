@extends('layouts.web')
@section('title')
{!! isset($page->titre) ? $page->titre : 'Galerie image | COUS Parakou' !!}
@endsection
@section('content')

<style>
    .bg2f{background: #e2e2e2}
    @media (max-width: 1200px) {
        #photos {
            -moz-column-count:    5;
            -webkit-column-count: 5;
            column-count:         5;
        }
    }
    @media (max-width: 1000px) {
        #photos {
            -moz-column-count:    4;
            -webkit-column-count: 4;
            column-count:         4;
        }
    }
    @media (max-width: 800px) {
        #photos {
            -moz-column-count:    3;
            -webkit-column-count: 3;
            column-count:         3;
        }
    }
    @media (max-width: 400px) {
        #photos {
            -moz-column-count:    1;
            -webkit-column-count: 1;
            column-count:         1;
        }
    }
    #photos {
        /* Prevent vertical gaps */
        line-height: 0;

        -webkit-column-count: 4;
        -webkit-column-gap:   0px;
        -moz-column-count:    4;
        -moz-column-gap:      0px;
        column-count:         4;
        column-gap:           0px;  
    }

    #photos img {
        /* Just in case there are inline attributes */
        width: 98% !important;
        height: auto !important;
        margin-bottom: 3px;
    }

    .carousel-control .glyphicon,
    .carousel-control .icon-prev,
    .carousel-control .icon-next {
        position: absolute;
        top: 50%;
        left: 50%;
        z-index: 5;
        display: inline-block;
        width: 20px;
        height: 20px;
        margin-top: -10px;
        margin-left: -10px;
        /*font-family: serif;*/
    }
    h4.div_img_titles {
        position: absolute;
        color: #ffffff;
        text-shadow: 0 1px 1px #222;
        margin: 5px 10px;
    }
    .bas_titre,a:hover{text-decoration: none;}
    .bas_titre{
        background: #000000;
        background: -moz-linear-gradient(top, rgba(255,255,255,.1), rgba(0,0,0,.91));
        background: -ms-linear-gradient(top, rgba(255,255,255,.1), rgba(0,0,0,.91));
        background: -o-linear-gradient(top, rgba(255,255,255,.1), rgba(0,0,0,.91));
        background: -webkit-linear-gradient(top, rgba(255,255,255,.1), rgba(0,0,0,.91));
        background: linear-gradient(top, rgba(255,255,255,.1), rgba(0,0,0,.91));
        font-size: 12px;
        padding: 3px 5px;
        text-decoration: none;
        color: #ffe41b;
        color: #ffffff;
        /*font-weight: bold;*/
        padding-top: 15px;
        height: 150px;
        text-shadow: 0 1px 1px #222;
        /*position: absolute;*/
        /*background: #000;*/
        z-index: 11110;
        overflow: hidden;
    }
    .pad2{padding: 1px}
</style>


<div class=" container ">
    <div class="row">
        <div class="col-sm-12 main-contents">
            @if(isset($images))
            <div class="col-lg-8 col-sm-8 ">
                <div class="headline"><h2 class="light rs   text-center mtop20"> Galerie images</h2></div>                         

                <!--<h2 class="light   m0 rss page-header "></h2>-->
                <div class=" bgwhite">
                    <div class="menu-html-content mtop-10 margin-bottom-2">
                        <div id="photos_0" class="rows">


                            @foreach($images as $image)
                            <div class="divbloc_img">
                                <!--<h4 class="div_img_titles ">{{$image->titre}}</h4>-->
                                <?php
                                $tab_img = explode(';', $image->images);

                                foreach ($tab_img as $img) {
                                    if (!empty($img)) {
                                        ?>
                                        <div class="col-sm-3 pad2">
                                            <a href="{{asset($img)}}" data-lightbox="roadtrip" data-lightbox="image-{{$image->id}}" data-title="{{ isset($image->titre) ? $image->titre : '' }}">
                                                <!--<img  src="{{asset($img)}}" class="img-responsive z-in" alt="{{$image->titre}}"/>-->
                                                <div class="" style="height: 150px; background: url({{asset($img)}}) #000000 50% 50% no-repeat;background-size:auto 150% ">
                                                    <div class="bas_titre">{{$image->titre}}</div>
                                                </div>

                                            </a>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>

                            </div>
                            <!--                            <div class="clearfix"><hr></div>-->
                            @endforeach

                        </div>
                        
                    </div> 
                    
                </div>
                <div class="clearfix"></div>
                <br> <br>
            </div>
            @endif
            <div class="col-lg-4 col-sm-4 Rpanel">

                @include('partials.right')
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('*').find('img').addClass('img-responsive');
        $('body').find('img').attr('data-lightbox', 'hh');
    })
</script>
@endsection
