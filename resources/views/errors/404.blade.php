@extends('layouts.web')
@section('css')
<style>
    #main-app{margin-top: 0}
    .nb-error {
        margin: 0 auto;
        text-align: center;
        max-width: 480px;
        padding: 25px 0px;
    }

    .nb-error .error-code {
        color: #2d353c;
        font-size: 200px;
        line-height: 200px;
    }

    .nb-error .error-desc {
        font-size: 12px;
        color: #647788;
    }
    .nb-error .input-group{
        margin: 30px 0;
    }
</style>
@endsection
@section('content')
<div class="nb-error">
    <div class="circle-user-login bgfb"> <img class="mauto" src="{{asset('assets/images/upforum-logo-mini.png')}}" /></div>
    <div class="error-code">404</div>
    <h4 class="font-bold bold">Nous ne pouvons pas afficher cette page...</h4>

    <div class="error-desc">
        Désolé, la page que vous demandez n'existe pas ou a été déplacée. <br/>
        Veuillez recharger la page ou cliquez sur les liens ci-dessous.
        <form  action="{{route('search')}}" method="GET" role="search">
            <div class="input-group" >
                <input type="text" name="query" class="form-control" placeholder="Rechercher un sujet ..." name="q">
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
        </form>
        <div class="text-center">
            <div class="buttons">
                <div class="row mtop20">
                    <div class="col-md-6">
                        <button onclick="history.back();" class="btn btn-primary rond3 btn-block"><i class="fa fa-angle-left pull-left"></i> RETOUR</button>
                    </div>
                    <div class="col-md-6">
                        <a href="{{url('/')}}" class="btn btn-danger btn-block rond3 ">ACCUEIL <i class="fa fa-angle-right pull-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <ul class="list-inline text-center text-sm">

            <li class="list-inline-item"><a href="{{route('login')}}" class="text-muted">Se connecter</a>
            </li>
            <li class="list-inline-item"><a href="{{route('register')}}" class="text-muted">S'inscrire</a>
            </li>
        </ul>

    </div>
</div>

@endsection