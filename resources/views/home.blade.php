@extends('layouts.web')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-info mtop20">
                <div class="panel-heading">COUS Parakou</div>

                <div class="panel-body text-center">
                    <a href="{url('/')}}"> 
                        <img width="" id="logos" class="img-responsive mauto" src="{{asset('assets/images/logo_cous_parakou.png')}}" alt="COUS Parakou">
                    </a>
                    <h3 class="text-center text-success-4"> <i class="fa fa-check-circle"></i> {!!Auth::user()?'Vous êtes connecté, <br><small>'.Auth::user()->prenom.' !</small>':NULL!!} </h3>
               </div>
                <ul class="list-group text-md">
                    <li class="list-group-item">
                        <a href="{{url('admin')}}"> <i class="fa fa-cog"></i> Page d'administration</a>
                    </li>
                     <li class="list-group-item">
                        <a href="{{url('/activites')}}"> <i class="fa fa-signal"></i> Nos activités</a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{url('/personnel')}}"> <i class="fa fa-users"></i> Le personnel</a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{url('/deliberations')}}"> <i class="fa fa-sheqel"></i> Délibérations</a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{url('/galerie')}}"> <i class="fa fa-picture-o"></i> Galerie images</a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{url('/')}}"> <i class="fa fa-backward"></i> Retour à l'accueil</a>
                    </li>
                </ul>
                     
            </div>
        </div>
    </div>
</div>
@endsection
