@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script>
    $('.select2').select2({
        placeholder: "Choisir ...",
        minimumInputLength: 0
    });
</script>                  
@endsection

<div class="form-group {{ $errors->has('nom') ? ' has-error' : ''}}">
    {!! Form::label('nom', 'Nom: ', ['class' => 'col-md-4  control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nom', null, ['class' => 'form-control text-uppercase', 'required' => 'required']) !!}
        {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('prenom') ? ' has-error' : ''}}">
    {!! Form::label('prenom', 'Prénom: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('prenom', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('prenom', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('photo') ? ' has-error' : ''}}">
    {!! Form::label('photo', 'Photo: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::file('photo',  ['class' => 'form-control']) !!}
        {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('sexe') ? ' has-error' : ''}}">
    {!! Form::label('sexe', 'Sexe: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('sexe',[''=>'-Sélectionnez-','H'=>'Homme','F'=>'Femme'], null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('sexe', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('telephone') ? ' has-error' : ''}}">
    {!! Form::label('telephone', 'Téléphone: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('telephone', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('telephone', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', 'Email: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if(isset($submitButtonText) && $submitButtonText !="Mise à jour")
<div class="form-group {{ $errors->has('password') ? ' has-error' : ''}}">
    {!! Form::label('password', 'Mot de passe: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password('password', ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <label for="password-confirm" class="col-md-4 control-label">Confirmation de mot de passe:</label>

    <div class="col-md-6">
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
    </div>
</div>
@endif


<div class="form-group {{ $errors->has('roles') ? ' has-error' : ''}}">
    {!! Form::label('role', 'Role: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('roles[]', $roles, isset($user_roles) ? $user_roles : [], ['class' => 'form-control select2', 'multiple' => true]) !!}
    </div>
</div>
<!--@if(isset($submitButtonText) && $submitButtonText =="Mise à jour")
<div class="form-group {{ $errors->has('upassword') ? ' has-error' : ''}}">
    {!! Form::label('upassword', 'Votre mot de passe: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password('upassword', ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('upassword', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif-->
<div class="form-group ">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
