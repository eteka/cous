@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-9">
            <header class="pageheader hidden">
                <h3><i class="fa fa-users"></i> Utilisateurs </h3>
            </header>
            <div class="panel">
                <div class="panel-heading"><h3 class="panel-title">Utilisateurs</h3></div>
                <div class="btn-toolbar pad-all hidden">
                    <div class="btn-group">
                        <button class="btn btn-sm btn-default" type="button"><i class="fa fa-mail-reply"></i></button>
                    </div>
                    <div class="btn-group">
                        <button class="btn btn-sm btn-default" type="button"><i class="fa fa-refresh"></i></button>
                    </div>
                    <div class="btn-group">
                        <button class="btn btn-sm btn-default" type="button"><i class="fa fa-archive"></i></button>
                        <button class="btn btn-sm btn-default" type="button"><i class="fa fa-ban"></i></button>
                        <button class="btn btn-sm btn-default" type="button"><i class="fa fa-trash-o"></i></button>
                    </div>
                    <div class="btn-group">
                        <button class="btn btn-sm btn-default dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button" aria-expanded="false"> <i class="fa fa-folder-open"></i> <i class="dropdown-caret fa fa-caret-down"></i> </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a> </li>
                            <li><a href="#">Another action</a> </li>
                            <li><a href="#">Something else here</a> </li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a> </li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <button class="btn btn-sm btn-default dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button" aria-expanded="false"> <i class="fa fa-tags"></i> <i class="dropdown-caret fa fa-caret-down"></i> </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a> </li>
                            <li><a href="#">Another action</a> </li>
                            <li><a href="#">Something else here</a> </li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a> </li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <button class="btn btn-sm btn-default dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button" aria-expanded="false"> More <i class="dropdown-caret fa fa-caret-down"></i> </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a> </li>
                            <li><a href="#">Another action</a> </li>
                            <li><a href="#">Something else here</a> </li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a> </li>
                        </ul>
                    </div>
                    <div class="btn-group pull-right">
                        <button class="btn btn-sm btn-default" type="button"><i class="fa fa-chevron-left"></i></button>
                        <button class="btn btn-sm btn-default" type="button"><i class="fa fa-chevron-right"></i></button>
                    </div>
                    <h6 class="pull-right"> Showing 1 - 15 of 2368 </h6>
                </div>
                <!-------------------------------------------------->
                <div class="panel-body">
                    <a href="{{ url('/admin/users/create') }}" class="btn btn-success btn-sm" title="Add New User">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>

                    {!! Form::open(['method' => 'GET', 'url' => '/admin/users', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                    {!! Form::close() !!}

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>ID</th><th>Name</th><th>Email</th><th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td><a href="{{ url('/admin/users', $item->id) }}">{{ $item->nom.' '.$item->prenom }}</a></td>
                                    <td>{{ $item->email }}</td>
                                    <td>
                                        <a href="{{ url('/admin/users/' . $item->id) }}" title="View User"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                        <a href="{{ url('/admin/users/' . $item->id . '/edit') }}" title="Edit User"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                        {!! Form::open([
                                        'method' => 'DELETE',
                                        'url' => ['/admin/users', $item->id],
                                        'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete User',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination"> {!! $users->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
