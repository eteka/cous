<div class="form-group {{ $errors->has('nom_prenom') ? 'has-error' : ''}}">
    {!! Form::label('nom_prenom', 'Nom Prenom', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nom_prenom', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('nom_prenom', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
    {!! Form::label('photo', 'Photo', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::file('photo', ['class' => 'form-control']) !!}
        {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('fonction') ? 'has-error' : ''}}">
    {!! Form::label('fonction', 'Fonction', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('fonction', null, ['class' => 'form-control']) !!}
        {!! $errors->first('fonction', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('categorie') ? 'has-error' : ''}}">
    {!! Form::label('categorie', 'Catégorie', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('categorie', null, ['class' => 'form-control']) !!}
        {!! $errors->first('categorie', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('specialite') ? 'has-error' : ''}}">
    {!! Form::label('specialite', 'Spécialité', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('specialite', null, ['class' => 'form-control']) !!}
        {!! $errors->first('specialite', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('grade') ? 'has-error' : ''}}">
    {!! Form::label('grade', 'Grade', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('grade', null, ['class' => 'form-control']) !!}
        {!! $errors->first('grade', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('adresse') ? 'has-error' : ''}}">
    {!! Form::label('adresse', 'Adresse/Tél', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('adresse', null, ['class' => 'form-control','rows'=>2]) !!}
        {!! $errors->first('adresse', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('biographie') ? 'has-error' : ''}}">
    {!! Form::label('biographie', 'Biographie', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('biographie', null, ['class' => 'form-control','rows'=>8]) !!}
        {!! $errors->first('biographie', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-0 col-md-6">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Ajouter aux personnels', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
