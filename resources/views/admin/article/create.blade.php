@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')
        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('article'))
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Ajout d'un article</div>
                <div class="panel-body">
                    <a href="{{ url('admin/article') }}" title="Retour"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <br />
                    <br />

                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif

                    {!! Form::open(['url' => '/admin/article', 'class' => 'form-horizontal', 'files' => true]) !!}

                    @include ('admin.article.form')

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection
