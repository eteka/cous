<a href="{{ url('/admin/article') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>

@if (Auth::user()->hasRole('admin') || Auth::user()->can('edit_article'))
<a href="{{ url('/admin/article/' . $article->id . '/edit') }}" title="Edit Article"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
@endif
@if (Auth::user()->hasRole('admin') || Auth::user()->can('delete_article'))
{!! Form::open([
'method'=>'DELETE',
'url' => ['admin/article', $article->id],
'style' => 'display:inline'
]) !!}
{!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
'type' => 'submit',
'class' => 'btn btn-danger btn-xs',
'title' => 'Delete Article',
'onclick'=>'return confirm("Confirm delete?")'
))!!}
{!! Form::close() !!}
@endif
@if (Auth::user()->hasRole('admin') || Auth::user()->can('validate_article'))
@if(intval($article->etat)==1)
<a href="{{ url('/admin/article/' . $article->id . '/unvalidate') }}" title="Annuler la validation"><button class="btn btn-default btn-xs"><i class="fa fa-remove" aria-hidden="true"></i> Annuler</button></a>
@else
<a href="{{ url('/admin/article/' . $article->id . '/validate') }}" title="Valider Article"><button class="btn btn-success btn-xs"><i class="fa fa-save" aria-hidden="true"></i> Valider</button></a>
@endif
@endif
<hr>
