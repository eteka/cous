@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Gestion des pages</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/page/create') }}" class="btn btn-success btn-sm" title="Add New Page">
                            <i class="fa fa-plus" aria-hidden="true"></i> Nouvelle page
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/page', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" value="{{isset($_GET['search'])?trim($_GET['search']):NULL}}" placeholder="Rechercher ici...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Page</th>
                                        <th >Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1;?>
                                @foreach($page as $item)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td><b>{{ $item->titre }}</b><br>
                                            <small class="text-info">{{ substr($item->slug,0,50) }}</small><br>
                                            <div class="text-sm text-muted">
                                                {{  str_limit(strip_tags($item->contenu,50)) }}</div>
                                        </td>
                                        <td width='25%' nowrap='nowrap'>
                                            <a href="{{ url('/admin/page/' . $item->id) }}" title="Voir Page"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> Voir</button></a>
                                            <a href="{{ url('/admin/page/' . $item->id . '/edit') }}" title="Editer Page"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editer</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/page', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Supprimer Page',
                                                        'onclick'=>'return confirm("Confirmer Suppression ?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $page->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
