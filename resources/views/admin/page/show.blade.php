@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Page {{ $page->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('/admin/page') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/page/' . $page->id . '/edit') }}" title="Edit Page"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/page', $page->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Page',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    
                                    <tr><td><span class="bold"> Titre : </span> {{ $page->titre }} </td></tr>
                                    <tr>
                                        <td><span class="bold"> Photo: </span>
                                            <?php ($page->photo)?>
                                            @if($page->photo!=NULL)
                                            <img src="{{ $page->photo }}" class="img-responsive img-thumbnail" alt="{{ $page->titre }}" /> 
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"> <h4 class="bold">Contenu </h4 cl>
                                            <br>
                                            {!! $page->contenu !!} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
