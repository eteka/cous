@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Deliberation</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/deliberation/create') }}" class="btn btn-success btn-sm" title="Add New Deliberation">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/deliberation', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th width="5%">N°</th>
                                        <th>Délibérations</th>
                                        <th width="5%">Etat</th>
                                        <th width="10%" >Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1;?>
                                @foreach($deliberation as $item)
                                 <?php $fac=($item->faculte()->first()->toArray());
                                 $types=['0'=>"Bourse"]
                                 ?>
                                
                                    <tr>
                                        <td>{{$i++ }}</td>
                                        <td><b>{{ $item->nom }}</b> <br>
                                                
                                                <a href="{{ asset($item->fichier) }}" target="_blanck" class="btnbtn-block ">Consulter le fichier</a>
                                               <div class="">
                                                   <ul class="list-inline">
                                                        <li><i class="fa fa-linode"></i> {{ $item->type }}</li>
                                                       <li><i class="fa fa-calendar"></i> {{ $item->annee_academique }}</li>
                                                       <li><i class="fa fa-graduation-cap"></i> {{ $fac['nom_fac'] or '' }}</li>
                                                   </ul>
                                                   <br>
                                                
                                                </div>
                                        </td>
                                        <td>
                                            @if($item->etat=='1')
                                            <span class="label label-success rond30">Activé</span>
                                            @else
                                            <span class="label label-danger rond30">Désactivé</span>
                                            @endif
                                        </td>
                                        <td nowrap='nowrap'>
                                            <a href="{{ url('/admin/deliberation/' . $item->id) }}" title="View Deliberation"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/admin/deliberation/' . $item->id . '/edit') }}" title="Edit Deliberation"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/deliberation', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete Deliberation',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $deliberation->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
