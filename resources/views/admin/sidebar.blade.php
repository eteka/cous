<div class="col-md-3">
<div class="panel panel-default panel-flush">
        
        <a class="btn btn-primary rond0 btn-block" href="{{ url('/') }}">
            <span class="glyphicon glyphicon-globe"></span>  Retour sur le site Web
                </a>
            
    </div>
    @foreach($laravelAdminMenus->menus as $section)
    @if($section->items)
    <?php
    $tab_section = [
        'Gestion des utilisateurs',
        'Outils'
    ];
    $r = '/' . Request::path();
    $murl = Request::path();
    $tburl = explode('/', $murl);
    if (count($tburl) > 1) {
        $r = '/' . $tburl[1];
    }
    $r= str_ireplace('/admin', '', $r);
    ?>
    

    @if(Auth::user()->is_admin==1 && (in_array($section->section,$tab_section)))
    <div class="panel panel-default panel-flush">

        <div class="panel-heading">
            {{ $section->section }}
        </div>

        @if(Auth::check())
        <div class="panel-body__">
            <ul class="nav" role="tablist">

                <?php
                $tab_admin = [
                    '/admin/users',
                    '/admin/roles',
                    '/admin/permissions',
                    '/admin/give-role-permissions',
                    '/admin/generator',
                ];
                ?>
                @foreach($section->items as $menu)
                @if(Auth::user()->is_admin==1 && (in_array($menu->url,$tab_admin)))
                <?php
                 $menu_url = str_ireplace('/admin', '', $menu->url);
                ?>
                <li class="<?= ($r == $menu_url) ? 'active' : '' ?>" role="presentation">
                    <a href="{{ url($menu->url) }}">
                        {{ $menu->title }} <i class="pull-right fa fa-angle-right mtop2"></i>
                    </a>
                </li>
                @elseif(Auth::user()->is_admin!=1 && (!in_array($menu->url,$tab_admin)))
                <?php
                $menu_url = str_ireplace('/admin', '', $menu->url);
                ?>
                <li class="<?= ($r == $menu_url) ? 'active' : '' ?>" role="presentation">
                    <a href="{{ url($menu->url) }}">
                        {{ $menu->title }} <i class="pull-right fa fa-angle-right mtop2"></i>
                    </a>
                </li>
                @endif

                @endforeach
            </ul>
        </div>
    </div>
    @endif

    @elseif((Auth::user()->is_editor==1 |Auth::user()->is_admin==1) && (!in_array($section->section,$tab_section)))
    <div class="panel panel-default panel-flush">
        <div class="panel-heading">
            {{ $section->section }} 
        </div>
        @if(Auth::check())
        <div class="panel-body____">
            <ul class="nav" role="tablist">
                <?php
                $tab_admin = [
                    '/admin/users',
                    '/admin/roles',
                    '/admin/permissions',
                    '/admin/give-role-permissions',
                    '/admin/generator',
                ];
                ?>
                @foreach($section->items as $menu)
                @if(!in_array($menu->url,$tab_admin))
                <?php
                $menu_url = str_ireplace('/admin', '', $menu->url);
                //$r= str_ireplace('/admin', '', $r);
                
                ?>
                <li class="<?= ($r == $menu_url) ? 'active' : '' ?>" role="presentation">
                    <a href="{{ url($menu->url) }}">
                        {{ $menu->title }} <i class="pull-right fa fa-angle-right mtop2"></i>
                    </a>
                </li>
                @elseif(Auth::user()->is_admin!=1 && (!in_array($menu->url,$tab_admin)))
                <li class="<?= ($r == $menu_url) ? 'active' : '' ?>" role="presentation">
                    <a href="{{ url($menu->url) }}">
                        {{ $menu->title }} <i class="pull-right fa fa-angle-right mtop2"></i>
                    </a>
                </li>
                @endif

                @endforeach
            </ul>
        </div>
    </div>
    @endif
    @endif
    @endif
    @endforeach
</div>
