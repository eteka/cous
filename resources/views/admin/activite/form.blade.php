@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link href="{{asset('assets/plugins/summernote/dist/summernote.css')}}" rel="stylesheet">
@endsection
@section('script')
<script src="{{asset('assets/plugins/summernote/dist/summernote.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">
var $editor = $('.summernote');

$editor.summernote({
    disableDragAndDrop: true,
    callbacks: {
        // Clear all formatting of the pasted text
        onPaste: function (e) {
            var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
            e.preventDefault();
            setTimeout(function () {
                document.execCommand('insertText', false, bufferText);
            }, 10);
        }
    },
    toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
//    ['height', ['height']],
        ['fontsize', ['fontsize']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'hr']],
        ['view', ['fullscreen']],
                //=>, 'superscript', 'subscript'
//    ['help', ['help']]
    ],
//    height: 250,
    minheight: 100,
    placeholder: 'Rédigez votre article',
    dialogsInBody: true,
    lang: 'fr-FR',
});


$('.select2').select2({
    placeholder: "Choisir ...",
    minimumInputLength: 0,
    tags:true
});
</script>
@endsection

<div class="form-group {{ $errors->has('titre') ? 'has-error' : ''}}">
    {!! Form::label('titre', 'Titre', ['class' => 'col-md-12 control-labels']) !!}
    <div class="col-md-12">
        {!! Form::text('titre', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('titre', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('chapeau') ? 'has-error' : ''}}">
    {!! Form::label('chapeau', 'Chapeau', ['class' => 'col-md-12 control-labels']) !!}
    <div class="col-md-12 ">
        {!! Form::textarea('chapeau', null, ['class' => 'form-control','rows'=>3]) !!}
        {!! $errors->first('chapeau', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
            {!! Form::label('photo', 'Photo', ['class' => 'col-md-12 control-labels']) !!}
            
            <div class="col-md-12 ">
                {!! Form::file('photo',  ['class' => 'form-control']) !!}
                <p class="help-block f11">Dimension maxi recomandée : 800x500 pixels</p>
                {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('categorie_id') ? 'has-error' : ''}}">
            {!! Form::label('categorie_id', 'Categorie', ['class' => 'col-md-12 control-labels']) !!}
            <div class="col-md-12 ">
                {!! Form::select('categorie_id', isset($categories)?$categories:[],0, ['class' => 'form-control ']) !!}
                {!! $errors->first('categorie_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group {{ $errors->has('legend') ? 'has-error' : ''}}">
            {!! Form::label('legend', 'Légende de la photo', ['class' => 'col-md-12 control-labels']) !!}
            <div class="col-md-12 ">
                {!! Form::textarea('legend',null,  ['class' => 'form-control','rows'=>2]) !!}
                {!! $errors->first('legend', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>    
</div>
<div id="cover_summernote">
    <div class="form-group {{ $errors->has('corps') ? 'has-error' : ''}}">
        <div class="col-md-12 ">
            {!! Form::textarea('corps', null, ['class' => 'form-control summernote', 'required' => 'required','rows'=>10]) !!}
            {!! $errors->first('corps', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
 
                                        
<div class="form-group {{ $errors->has('etat') ? 'has-error' : ''}}">
    {!! Form::label('etat', 'Etat', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        <div class="checkbox">
    <label>{!! Form::radio('etat', '1') !!} Yes</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('etat', '0', true) !!} No</label>
</div>
        {!! $errors->first('etat', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<!--<div class="form-group {{ $errors->has('mots_cles') ? ' has-error' : ''}}">
    {!! Form::label('mots_cles', 'Mots clés: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('mots_cles[]', [], isset($mots_cles) ?$mots_cles  : [], ['class' => 'form-control select2', 'multiple' => true]) !!}
    </div>
</div>-->

<?php
$users=[];
$Allusers= \App\User::select('id','nom','prenom')->orderBy('prenom')->get();
foreach ($Allusers as $key => $v) {
    $users[$v['id']]=$v['prenom'].' '.$v['nom'];
}
?>
<div class="form-group {{ $errors->has('mots_cles') ? ' has-error' : ''}}">
    {!! Form::label('mots_cles', 'Rédacteur : ', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-4">
        {!! Form::select('user_id',  isset($users) ?$users  : [],null, ['class' => 'form-control select2']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-0 col-md-8">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Ajouter aux activités ', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
