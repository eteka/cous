@extends('layouts.backend')

@section('content')
<style>
    .mwell{background: #ffffff !important;min-height: 450px;}
</style>
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-9">
            <div class="panels well mwell ">
                <h2 class="panel-heading bold text-center">Administration du site</h2>

                <div class="panel-body ">
                    <img width="" id="logo" class="img-responsives" src="http://localhost:8000/assets/images/logo_cous_parakou.png" alt="">
                    <h3 class='text-center light'>Bienvenue, <a href="{{url('profile')}}">{{Auth::user()->prenom}}</a> !</h3>
                    <hr>
                    <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 alert  alert-info">
                    <div class="">
                        <small class="text-muted">Cette page vous permet de gérer la plupart des fonctionnalités du site Web du <b>COUS - Parakou</b>
                        </small><br>
                            <b>Les grandes rubriques à gérer sont :</b>
                        <ul>
                            <li>Le contenu des différents pages</li>
                            <li>Les délibérations</li>
                            <li>Les activités</li>
                            <li>La Galerie Image</li>
                            <li>Les communiqués</li>
                            <li>Et biens d'autres</li>
                        </ul>
                            Vous pouvez utiliser le menu de gauche pour parcourrir les différente fonctionnalités.<h5>Merci !<br><small>L'Administrateur</small></h5>
                    </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
