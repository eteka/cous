<div class="row">


    <div class="form-group col-sm-9 {{ $errors->has('photo') ? 'has-error' : ''}}">
        {!! Form::label('photo', 'Photo', ['class' => 'col-md-12 control-labels']) !!}
        <div class="col-md-12">
            {!! Form::file('photo',  ['class' => 'form-control']) !!}
            {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

</div>
<div class="row">

    <div class="form-group col-sm-5  {{ $errors->has('ordre') ? 'has-error' : ''}}">
        {!! Form::label('ordre', "Ordre d'affichage", ['class' => 'col-md-12 control-labels']) !!}
        <div class="col-md-12">
            {!! Form::number('ordre', null, ['class' => 'form-control']) !!}
            {!! $errors->first('ordre', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group col-sm-12 {{ $errors->has('titre') ? 'has-error' : ''}}">
        {!! Form::label('titre', 'Titre', ['class' => 'col-md-12 control-labels']) !!}
        <div class="col-md-9">
            {!! Form::text('titre', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('titre', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group col-sm-9 {{ $errors->has('sous_titre') ? 'has-error' : ''}}">
        {!! Form::label('sous_titre', 'Sous Titre', ['class' => 'col-md-12 control-labels']) !!}
        <div class="col-md-12">
            {!! Form::text('sous_titre', null, ['class' => 'form-control']) !!}
            {!! $errors->first('sous_titre', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group col-md-9 {{ $errors->has('detail') ? 'has-error' : ''}}">
        {!! Form::label('detail', 'Detail', ['class' => 'col-md-12 control-labels']) !!}
        <div class="col-md-12">
            {!! Form::textarea('detail', null, ['class' => 'form-control','rows'=>5]) !!}
            {!! $errors->first('detail', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

</div> 


<div class="form-group">
    <div class="col-md-offset-0 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Ajouter aux slides', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
