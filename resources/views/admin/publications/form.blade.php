<div class="form-group {{ $errors->has('titre') ? 'has-error' : ''}}">
    {!! Form::label('titre', 'Titre', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('titre', null, ['class' => 'form-control', 'required' => 'required','rows'=>'2']) !!}
        {!! $errors->first('titre', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('auteur') ? 'has-error' : ''}}">
    {!! Form::label('auteur', 'Auteur(s)', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('auteur', null, ['class' => 'form-control', 'required' => 'required','rows'=>'2']) !!}
        {!! $errors->first('auteur', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
    {!! Form::label('annee', "Type", ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('type', null, ['class' => 'form-control']) !!}
        {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('annee') ? 'has-error' : ''}}">
    {!! Form::label('annee', "Année d'édition", ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('annee', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('annee', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('references') ? 'has-error' : ''}}">
    {!! Form::label('references', 'Références', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('references', null, ['class' => 'form-control']) !!}
        {!! $errors->first('references', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<!--<div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::label('user_id', 'User Id', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>-->

<div class="form-group">
    <div class="col-md-offset-3 col-md-3">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Ajouter aux publications', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
