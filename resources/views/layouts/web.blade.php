<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <link rel="icon" href="favicone.ico" type="image/x-icon" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title'," Centre des Oeuvres Universitaire et Sociales de Parakou") | COUS-UP </title>
        <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/css/template-cms.odace.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
        <link rel="stylesheet" href="{{asset('assets/vendor/font-awesome/css/font-awesome.min.css')}}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Styles -->
        <!-- Fonts -->
        <!--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">-->
        @yield('css')
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,700%7CUbuntu:400,500,700%7CPlayfair+Display:400,700,700i,900i">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">        
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif" rel="stylesheet">
        <!--<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">-->
        <!--<link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">-->
        <!--        
                <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">-->

        <!-- Styles -->
        <!--<link href="https://fonts.googleapis.com/css?family=Josefin+Slab|Work+Sans" rel="stylesheet">-->
        <!--<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">-->
        <style>
            /*
Code snippet by maridlcrmn for Bootsnipp.com
Follow me on Twitter @maridlcrmn
            */

            
            .headline h2 {
                font-family:'oswald',  'impact','Times New Roman',sans-serif,serif;
                font-size: 25px;
                font-weight: bold;
            }
            #main,.menu-html-content{
                font-size: 20px;
                /*font-family: 'Roboto', serif;*/
            }
            #footer .headline h2{
                font-size: 30px;
            }
        </style>
    </head>
    <body id="body">
        <div id="inner-wrapper" class="container-fluids">
            <nav id="top-link" class="topBar">
                <div class="container">
                    <ul class="list-inline pull-left ">
                        <li class="hidden-sm hidden-xs"><span class="text-red_ text-uppercase">Centre des Oeuvres Universtaires et Sociales de Parakou</li>
                            <li class="visible-sm visible-xs"><span class="text-red_ text-uppercase">COUS-Parakou</li>
                    </ul>
                    <ul class="list-inline pull-right">
                        <li>
                            <i class="fa fa-facebook"></i>
                        </li>
                        <li>
                            <i class="fa fa-twitter "></i>
                        </li>
                        <li>
                            <i class="fa fa-rss "></i>
                        </li>
                    </ul>
                    <ul class="topBarNav pull-right">
                        @if(Auth::guest())
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="false"> <span class="hidden-xs">Mon compte <i class="fa fa-angle-down ml-5"></i></span> </a>
                            <ul class="dropdown-menu w-150" role="menu">
                                <li><a href="{{url('login')}}">Se connecter</a>
                                </li>
                                <li><a href="{{url('register')}}">Créer un compte</a>
                                </li>
                            </ul>
                        </li>
                        @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="false"> <span class="hidden-xs">Mon compte <i class="fa fa-angle-down ml-5"></i></span> </a>
                            <ul class="dropdown-menu w-150" role="menu">
                                <li><a href="{{url('profile')}}">Mon profil</a></li>
                                <li><a href="{{url('/profile/information')}}">Mes informations</a></li>
                                <li><a href="{{url('/profile/activity')}}">Activités du compte</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{ route('logout') }}"
                                       class=""
                                       onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                        {{ trans('profileLang::profile.sidebar.logout') }}
                                    </a></li>
                            </ul>
                        </li>
                        @endif

                    </ul>
                </div>
            </nav><!--=========-TOP_BAR============-->

            <!--=========MIDDEL-TOP_BAR============-->
            <?php
            $r = Request::path();
            ?>
            <div  class="middleBar">
                <div class="container">
                    <div class="row display-table">
                        <div class="col-sm-3 col-xs-4 vertical-align text-left">
                            <a href="javascript:void(0);"> <img width="" id="logo" class="img-responsive" src="{{asset('assets/images/logo_cous_parakou.png')}}" alt=""></a>
                        </div>
                        <!-- end col -->
                        <div class="col-sm-6 col-md-7 col-xs-8 vertical-align text-center">
                            <form action='{{url('search')}}' me thod='GET'>
                                <div class="row grid-space-1">

                                    <div class="col-sm-6 col-md-5 col-xs-8  col-xs-offset-0  col-md-offset-1">
                                        <input type="text" name="query" value="{{old('query')}}" class="form-control input-lg rond0" placeholder="Rechercher ...">
                                    </div>

                                    <div class="col-sm-4 col-md-3 text-right  col-xs-4">
                                        <button type="submit" class="btn btn-default btn-block btn-lg"><span class="fa fa-search visible-xs"></span><span class="hidden-xs">Rechercher</span></button>

                                    </div>
                                    <!-- end col -->
                                </div>
                                <!-- end row -->
                            </form>
                        </div>
                        <!-- end col -->
                        <div class="col-sm-4 col-md-3 vertical-align header-items hidden-xs">
                            <a class="pad10 borderb  btn bgf rond3 btn-block btn-defaults" href="{{url('deliberations')}}"> <i class="fa fa-file-powerpoint-o"></i> <b class="text-uppercase">Les délibérations</b><br><small>des allocations universitaires</small> </a>
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end  row -->
                </div>
            </div>
            <nav class="navbar navbar-main navbar-default" role="navigation" style="opacity: 1;">
                <div class="container">
                    <!-- Brand and toggle -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-1">
                            <span class="sr-only">Menu</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>             
                    </div>

                    <!-- Collect the nav links,  -->
                    <div class="collapse navbar-collapse navbar-1" style="margin-top: 0px;margin-left: 0">            
                        <ul class="nav navbar-nav">
                            <li class=" <?= ($r == '/') ? 'active' : '' ?>"><a href="{{URL('/')}}" ><i class="fa fa-home hidden-xs"></i><span class="visible-xs">Accueil</span></a></li>
                            <li class="dropdown  <?= ($r == 'presentation') ? 'active' : '' ?>"">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="false">
                                    Présentation  <i class="fa fa-angle-down ml-5"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-left">
                                    <li><a href="{{route('pageContentPresentation',['slug'=>'historique'])}}">Historique</a></li>
                                    <li><a href="{{route('pageContentPresentation',['slug'=>'attributions-et-missions'])}}">Attributions, missions</a></li>
                                    <li><a href="{{route('pageContentPresentation',['slug'=>'organigramme'])}}">Organigramme</a></li>
                                    <li><a href="{{route('pageContentPresentation',['slug'=>'equipe-de-direction'])}}">Equipe de direction</a></li>                                        
                                    <!--<li><a href="{{route('pageContentPresentation',['slug'=>'conditions-generales-pour-beneficier-des-oeuvres'])}}">Conditions générales pour bénéficier des Oeuvres</a></li>-->
                                    
                                    <!--<li><a href="#">Le Personnel administratif</a></li>-->
                                </ul>

                            </li>
                            <li class="dropdown megaDropMenu  <?= ($r == 'services') ? 'active' : '' ?>">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="false">Services  <i class="fa fa-angle-down ml-5"></i></a>
                                <ul class="dropdown-menu row">
                                    <li class="col-sm-3">
                                        <ul>
                                            <li class="dropdown-header text-uppercases rs">Hébergement</li>
                                            <i class="fa fa-building-o pull-right dicone" ></i>

                                            <li><a href="{{route('pageContentServices',['slug'=>'hebergement-condition-generales'])}}">Conditions générales</a></li>
                                            <li><a href="{{route('pageContentServices',['slug'=>'hebergement-cout-des-residences'])}}">Coût des résidences</a></li>
                                            <li><a href="{{route('pageContentServices',['slug'=>'hebergement-pieces-constitutives'])}}">Pièces constitutives</a></li>

                                            <li class="divider"></li>

                                            <li class="dropdown-header text-uppercases rs">Restauration</li>
                                            <i class="fa fa-cutlery pull-right dicone" ></i>
                                            <li><a href="{{route('pageContentServices',['slug'=>'restauration-conditions-generales'])}}">Conditions générales</a></li>
                                            <li><a href="{{route('pageContentServices',['slug'=>'restauration-tarifs-des-repas'])}}">Tarifs des repas</a></li>
                                            <li><a href="{{route('pageContentServices',['slug'=>'restauration-dispositions-particulieres'])}}">Dispositions particulières</a></li>
                                            <li><a href="{{route('pageContentServices',['slug'=>'restauration-menus-hebdomadaire'])}}">Menus hebdomadaire</a></li>
                                        </ul>
                                    </li>
                                    <li class="col-sm-3">

                                        <ul>
                                            <li class="dropdown-header text-uppercases rs">Transport</li>
                                            <i class="fa fa-bus pull-right dicone" ></i>
                                            <li><a href="{{route('pageContentServices',['slug'=>'transport-materiels-roulants'])}}">Matériels roulants</a></li>
                                            <li><a href="{{route('pageContentServices',['slug'=>'transport-conditions-dattribution'])}}">Conditions d'attribution</a></li>
                                            <li><a href="{{route('pageContentServices',['slug'=>'transport-gestion-du-materiel-roulant'])}}">Gestion du matériel roulant </a></li>
                                            <!--<li><a href="{{route('pageContentServices',['slug'=>'transport-gestion-des-subventions'])}}">Gestion des subventions</a></li>-->
                                            <li class="divider"></li>
                                            <i class="fa fa-futbol-o pull-right dicone" ></i>
                                            <li class="dropdown-header text-uppercases rs">Activités sportives</li>

                                            <li><a href="{{route('pageContentServices',['slug'=>'activites-sportives-objectifs'])}}">Objectifs</a></li>
                                            <li><a href="{{route('pageContentServices',['slug'=>'activites-sportives-activites-proposees'])}}">Activités proposées</a></li>
                                        </ul>
                                    </li>
                                    <li class="col-sm-3">
                                        <ul>
                                            <li class="dropdown-header text-uppercases rs">Santé et affaires sociales</li>
                                            <i class="fa fa-street-view pull-right dicone" ></i>
                                            <li><a href="{{route('pageContentServices',['slug'=>'activites-artistiques-et-culturelles-objectifs'])}}">Service de santé</a></li>
                                            <li><a href="{{route('pageContentServices',['slug'=>'sante-et-affaires-sociales-prestations-offertes-dans-le-domaine-des-affaires-sociales'])}}">Prestations offertes dans le domaine des affaires sociales</a></li>
                                            <li><a href="{{route('pageContentServices',['slug'=>'conditions-generales-pour-beneficier-des-oeuvres'])}}">Conditions générales d'une aide</a></li>
                                            <li class="divider"></li>

                                            <li class="dropdown-header text-uppercases rs">Activités artistiques & culturelles </li>
                                            <i class="fa fa fa-american-sign-language-interpreting pull-right dicone" ></i>
                                            <li><a href="{{route('pageContentServices',['slug'=>'activites-artistiques-et-culturelles-objectifs'])}}">Objectifs</a></li>
                                            <li><a href="{{route('pageContentServices',['slug'=>'activites-artistiques-et-culturelles-activites-proposees'])}}">Activités proposées</a></li>
                                        </ul>
                                    </li>
                                    <li class="col-sm-3">
                                        <ul>
                                            <li class="dropdown-header rs">Allocations Universitaires<br>
                                                <small class="text-muted text-xs" title="Bourses, Secours, frais d'équippement, de stage, de mémoire et de thèse">Bourses, Secours, frais d'équippement,...</small>
                                            </li>
                                            <i class="fa fa-handshake-o pull-right dicone" ></i>
                                            <li><a href="{{route('pageContentServices',['slug'=>'allocations-universitaires-contexte-actuel'])}}">Contexte actuel</a></li>
                                            <li><a href="{{route('pageContentServices',['slug'=>'allocations-universitaires-roles-des-acteurs'])}}">Rôles des acteurs</a></li>
                                            <li><a href="{{route('pageContentServices',['slug'=>'allocations-universitaires-criteres-dattributions'])}}">Critères d'attributions</a></li>
                                            <li><a href="{{route('pageContentServices',['slug'=>'allocations-universitaires-criteres-de-renouvellement-et-de-retablissement'])}}">Critères de renouvellement et de rétablissement</a></li>
                                            <!--<li><a href="{{route('pageContentServices',['slug'=>'allocations-universitaires-criteres-de-retablissement'])}}">Critères de rétablissement</a></li>-->

                                            <!--<li><a href="{{url('allocations/reclammations')}}">Réclammations</a></li>-->
                                            <li><a  class="btn btn-info btn-block text-white" href="{{url('deliberations/')}}">Délibérations </a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li  class="<?= ($r == 'personnel') ? 'active' : '' ?>"><a href="{{url('personnel')}}"> Le personnel</a></li> 
                            <li class="<?= ($r == 'activites') ? 'active' : '' ?>"><a href="{{url('activites')}}">Activités</a></li> 

                            <li class="<?= ($r == 'documents-administratifs') ? 'active' : '' ?>"><a href="{{url('documents-administratifs')}}">Documents administratifs</a></li> 
                            <li class="<?= ($r == 'galerie') ? 'active' : '' ?>"><a href="{{url('galerie')}}">Galerie images</a></li>
                            <li class="<?= ($r == 'contact') ? 'active' : '' ?>"><a href="{{url('contact')}}">Contact</a></li>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
            </nav>














            <div id="top-link hidden">
                <div class="container">
                    <div class="row hidden" >
                        <div class="col-sm-7 " >
                            <ul class="list-inline hidden-xs " id="hmenu">
                                <li><a href="#"><i class="fa fa-sellsy"></i> Bourses d'études</a></li>
                                <li><a href="#"><i class="fa fa-ambulance"></i> Secours universitaire</a></li>
                                <li><a href="#"><i class="fa fa-cubes"></i> Frais de mémoires</a></li>

                            </ul>
                        </div>
                        <div class="col-sm-5 text-right">
                            <ul class="list-inline text-sm" id="hrs">
                                <li>Suivez-nous sur : </li>
                                <li><i class="fa fa-facebook"></i></li>
                                <li><i class="fa fa-youtube"></i></li>
                                <li><i class="fa fa-twitter"></i></li>
                                <li><i class="fa fa-rss"></i></li>
                                <!--<li class=""><i class="fa fa-phone-square"></i> 96 XX XX XX </li>-->
                            </ul>
                        </div>
                    </div>  
                </div>
            </div> 
            <header class="hidden">
                <nav id="nav" class="navbar navbar-default">
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                                <span class="sr-only">Menu</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="{{url('/')}}"><img id="logo" src="{{asset('assets/images/logo_cous_parakou.png')}}"</a>
                        </div>

                        <!-- Co                                                                                       llect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Accueil</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Présentation <span class="fa fa-chevron-down"></span></a>
                                    <ul id="present_menu" class="dropdown-menu">
                                        <li><a href="#">Historique</a></li>
                                        <li><a href="#">Attributions, missions</a></li>
                                        <li><a href="#">Conditions générales</a></li>
                                        <li><a href="#">Equipe de direction</a></li>                                        
                                        <li><a href="#">Organigramme</a></li>
                                    </ul>
                                </li>

                                <li class="dropdown dropdown-large">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services <b class="fa fa-chevron-down"></b></a>

                                    <ul class="dropdown-menu dropdown-menu-large row">
                                        <li class="col-sm-3">
                                            <ul>
                                                <li class="dropdown-header text-uppercases rs">Hébergement</li>
                                                <i class="fa fa-building-o pull-right dicone" ></i>

                                                <li><a href="#">Conditions générales</a></li>
                                                <li><a href="#">Coût des résidences</a></li>
                                                <li><a href="#">Pièces constitutives</a></li>

                                                <li class="divider"></li>

                                                <li class="dropdown-header text-uppercases rs">Restauration</li>
                                                <i class="fa fa-cutlery pull-right dicone" ></i>
                                                <li><a href="#">Conditions générales</a></li>
                                                <li><a href="#">Tarifs des repas</a></li>
                                                <li><a href="#">Dispositions particulières</a></li>
                                                <li><a href="#">Menus hebdomadaire</a></li>
                                            </ul>
                                        </li>
                                        <li class="col-sm-3">

                                            <ul>
                                                <li class="dropdown-header text-uppercases rs">Transport</li>
                                                <i class="fa fa-bus pull-right dicone" ></i>
                                                <li><a href="#">Matériels roulants</a></li>
                                                <li><a href="#">Conditions d'attribution</a></li>
                                                <li><a href="#">Gestion du matériel roulant </a></li>
                                                <!--<li><a href="#">Gestion des subventions</a></li>-->
                                                <li class="divider"></li>
                                                <i class="fa fa-futbol-o pull-right dicone" ></i>
                                                <li class="dropdown-header text-uppercases rs">Activités sportives</li>

                                                <li><a href="#">Objectifs</a></li>
                                                <li><a href="#">Activités proposées</a></li>
                                            </ul>
                                        </li>
                                        <li class="col-sm-3">
                                            <ul>
                                                <li class="dropdown-header text-uppercases rs">Santé et affaires sociales</li>
                                                <i class="fa fa-street-view pull-right dicone" ></i>
                                                <li><a href="#">Prestations offertes dans le domaine de la santé</a></li>
                                                <li><a href="#">Prestations offertes dans le domaine des affaires sociales</a></li>
                                                <li><a href="#">Conditions générales d'une aide</a></li>
                                                <li class="divider"></li>

                                                <li class="dropdown-header text-uppercases rs">Activités artistiques sportives</li>
                                                <i class="fa fa fa-american-sign-language-interpreting pull-right dicone" ></i>
                                                <li><a href="#">Objectifs</a></li>
                                                <li><a href="#">Activités proposées</a></li>
                                            </ul>
                                        </li>
                                        <li class="col-sm-3">
                                            <ul>
                                                <li class="dropdown-header rs">Allocations Universitaires<br>
                                                    <small class="text-muted text-xs" title="Bourses, Secours, frais d'équippement, de stage, de mémoire et de thèse">Bourses, Secours, frais d'équippement,...</small>
                                                </li>
                                                <i class="fa fa-handshake-o pull-right dicone" ></i>
                                                <li><a href="#">Contexte actuel</a></li>
                                                <li><a href="#">Rôles des acteurs</a></li>
                                                <li><a href="#">Critères d'attributions</a></li>
                                                <li><a href="#">Critères de renouvellement</a></li>
                                                <li><a href="#">Critères de rétablissement</a></li>
                                                <li><a href="#">Consulter les délibérations</a></li>
                                            </ul>
                                        </li>
                                    </ul>

                                </li>
                                <li><a href="#">Activités</a></li> 
                                <li><a href="#">Administration</a></li>                                                                                                                <li><a href="#">Contact</a></li>
                                <li>
                                    <a class="btn btn-default btn-outline btn-circle collapsed"  data-toggle="collapse" href="#nav-collapse1" aria-expanded="false" aria-controls="nav-collapse1">Categories</a>
                                </li>
                            </ul>
                            <ul class="collapse nav navbar-nav nav-collapse" id="nav-collapse1">
                                <li><a href="#">Web design</a></li>
                                <li><a href="#">Development</a></li>
                                <li><a href="#">Graphic design                                                                                                                                                                               </a></li>
                                <li><a href="#">Print</a></li>
                                <li><a href="#">Motion</a></li>
                                <li><a href="#">Mobile apps</a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container -->
                </nav><!-- /.navbar -->
            </header>
            <div  id="main" >
                @yield('content')
            </div>
            <div class="cfoot">
                <div class="container">
                    <div id="footer" class="row " >

                        <div class="col-sm-4">
                            <div class="headline"><h2 class="light text-uppercase text-center"><i class="icon-bubbles"></i> COUS Parakou</h2></div>                         

                            <ul class="list-unstyled">
                                <li><a href="{{url('/')}}"><i class="fa fa-angle-right"></i>  Accueil du COUS</a></li>
                                <li><a href="{{route('pageContentPresentation',['slug'=>'historique'])}}"><i class="fa fa-angle-right"></i> Historique</a></li>
                                <li><a href="{{route('pageContentPresentation',['slug'=>'attributions-et-missions'])}}"><i class="fa fa-angle-right"></i> Attributions, missions</a></li>
                                <li><a href="{{route('pageContentPresentation',['slug'=>'conditions-generales-pour-beneficier-des-oeuvres'])}}"><i class="fa fa-angle-right"></i> Conditions générales pour bénéficier des Oeuvres</a></li>
                                <li><a href="{{route('pageContentPresentation',['slug'=>'organigramme'])}}"><i class="fa fa-angle-right"></i> Organigramme</a></li>
                                <li><a href="{{route('pageContentPresentation',['slug'=>'equipe-de-direction'])}}"><i class="fa fa-angle-right"></i> Equipe de direction</a></li>                                        

                            </ul>
                        </div>
                        <div class="col-sm-4">
                            <div class="headline"><h2 class="light text-uppercase text-center"><i class="icon-bubbles"></i> Nos actions</h2></div>                         

                            <ul class="list-unstyled">
                                <li><a href="{{route('pageContentServices',['slug'=>'hebergement-condition-generales'])}}"><i class="fa fa-angle-right"></i> Hébergement</a></li>
                                <li><a href="{{route('pageContentServices',['slug'=>'transport-materiels-roulants'])}}"><i class="fa fa-angle-right"></i> Transport</a></li>
                                <li><a href="{{route('pageContentServices',['slug'=>'restauration-conditions-generales'])}}"><i class="fa fa-angle-right"></i> Restauration</a></li>
                                <li><a href="{{route('pageContentServices',['slug'=>'activites-sportives-objectifs'])}}"><i class="fa fa-angle-right"></i> Activités sportives</a></li>                                        
                                <li><a href="{{route('pageContentServices',['slug'=>'sante-et-affaires-sociales-prestations-offertes-dans-le-domaine-de-la-sante'])}}"><i class="fa fa-angle-right"></i> Service de Santé et Affaires sociales</a></li>
                                <li><a href="{{route('pageContentServices',['slug'=>'activites-artistiques-et-culturelles-objectifs'])}}"><i class="fa fa-angle-right"></i> Activités artistiques & sportives</a></li>
                                <li><a href="{{route('pageContentServices',['slug'=>'allocations-universitaires-contexte-actuel'])}}"><i class="fa fa-angle-right"></i> Allocations universitaires</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-4">

                            <div class="headline"><h2 class="light text-uppercase"><i class="icon-bubbles"></i> Contactez nous</h2></div>                         
                            <p class="text-sm text-muted">
                                Le Centre des Oeuvres Universtaires et Sociales de Parakou est à la disposition de toute la communauté
                            </p>
                            <address class="bold text-sm">

                                <i class="fa fa-phone"></i> : <a href="tel:+229 00 00 00" >(+229) 00 00 00 00</a><br>
                                <i class="fa fa-envelope-open-o"></i> : <a href="mailto:" >contacts@cous-parakou.bj</a>
                            </address>
                            <h5 class="text-muted">Rejoingnez nous sur les réseaux sociaux</h5>
                            <ul class="footer-socials list-inline">
                                <li>
                                    <a href="https://www.facebook.com/UPBENIN/?fref=ts" target="_blanck" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
                                        <i class="fa fa-facebook"></i><span class="visible-xs huge-h-4 pull-right">Facebook</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/UnivParakou" target="_blanck" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google Plus">
                                        <i class="fa fa-twitter"></i> <span class="visible-xs huge-h-4 pull-right">Twitter</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/channel/UCUGd99fMztwt-Ulv8XYhwjA " target="_blanck" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google Plus">
                                        <i class="fa fa-youtube-play"></i> <span class="visible-xs huge-h-4 pull-right">YouTube</span>
                                    </a>
                                </li>

                            </ul>
                        </div>
                        <div class="col-sm-12">
                            <div class="logo-footer" style="   ">
                                <a original-title="CCM Benchmark Group" href="{{url('/')}}">
                                    <hgroup>
                                        <span>COUS Parakou</span>
                                    </hgroup>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div  class="copyright text-xs text-center">
            <br>
            &copy; COUS Parakou {{date('Y')}}, Tous droits réservés

        </div>

    </div>
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>

    <script src="{{asset('assets/plugins/pjax/jquery.pjax.js')}}"></script> 
    <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"  ></script>

    <link rel="stylesheet" href="{{asset('assets/vendor/lightbox/css/lightbox.min.css')}}">
    <script src="{{asset('assets/vendor/lightbox/js/lightbox.min.js')}}"></script>
    <script type="text/javascript">

                                           !function ($, n, e) {

                                               lightbox.option({
                                                   'resizeDuration': 200,
                                                   'wrapAround': true
                                               });

                                               var o = $();
                                               $.fn.dropdownHover = function (e) {
                                                   return "ontouchstart" in document ? this : (o = o.add(this.parent()), this.each(function () {
                                                       function t(e) {
                                                           o.find(":focus").blur(), h.instantlyCloseOthers === !0 && o.removeClass("open"), n.clearTimeout(c), i.addClass("open"), r.trigger(a)
                                                       }
                                                       var r = $(this),
                                                               i = r.parent(),
                                                               d = {
                                                                   delay: 100,
                                                                   instantlyCloseOthers: !0
                                                               },
                                                               s = {
                                                                   delay: $(this).data("delay"),
                                                                   instantlyCloseOthers: $(this).data("close-others")
                                                               },
                                                               a = "show.bs.dropdown",
                                                               u = "hide.bs.dropdown",
                                                               h = $.extend(!0, {}, d, e, s),
                                                               c;
                                                       i.hover(function (n) {
                                                           return i.hasClass("open") || r.is(n.target) ? void t(n) : !0
                                                       }, function () {
                                                           c = n.setTimeout(function () {
                                                               i.removeClass("open"), r.trigger(u)
                                                           }, h.delay)
                                                       }), r.hover(function (n) {
                                                           return i.hasClass("open") || i.is(n.target) ? void t(n) : !0
                                                       }), i.find(".dropdown-submenu").each(function () {
                                                           var e = $(this),
                                                                   o;
                                                           e.hover(function () {
                                                               n.clearTimeout(o), e.children(".dropdown-menu").show(), e.siblings().children(".dropdown-menu").hide()
                                                           }, function () {
                                                               var t = e.children(".dropdown-menu");
                                                               o = n.setTimeout(function () {
                                                                   t.hide()
                                                               }, h.delay)
                                                           })
                                                       })
                                                   }))
                                               }, $(document).ready(function () {
                                                   $('[data-hover="dropdown"]').dropdownHover()
                                               })

                                           }(jQuery, this);
    </script>
    <script>
        jQuery(document).ready(function ($) {
            // Auto-handle direct jpeg links
            $('a[href$="jpg"]').not('.gallery a').each(function () {
                $(this).lightbox();
            });
        });
    </script>
    @yield('script')

</body>
</html>
