<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <link rel="icon" href="favicone.ico" type="image/x-icon" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title',"COUS-UP | Centre des Oeuvres Universitaire de Parakou")</title>
        <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">

        <link rel="stylesheet" href="{{asset('assets/css/template-cms.odace.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
        <link rel="stylesheet" href="{{asset('assets/vendor/font-awesome/css/font-awesome.min.css')}}">
        <!-- Styles -->
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        @yield('css')
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
        <!--        
                <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">-->

        <!-- Styles -->
        <style>
            .rs{
                 font-family:'Roboto Slab', 'Trebuchet MS', serif;
            }
            html, body {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
                /*background-color: #F7F8FB;*/
                height: 100%;
                -webkit-font-smoothing: antialiased;
                font-family:'Roboto', 'Open Sans', sans-serif;
                padding: 0;
                margin: 0;
                /*font-family: Roboto,sans-serif;*/
                font-weight: 300;
                line-height: 1.5;
                color: #000;
                background: #fff;
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
            }
            #present_menu>li>a{
                padding: 8px 25px;
            }
            .dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover {
                color: #ffffff;
                text-decoration: none;
                background-color: #e5251b;
            }
            .dropdown-menu-large > li ul > li > a:hover, .dropdown-menu-large > li ul > li > a:focus {
                color: #ffffff;
                text-decoration: none;
                background-color: #e5251b;
            }
            .navbar-default .navbar-toggle{border-width: 0px}
            .navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle:hover{
                background: none;
                border-radius: 0;
            }
            .navbar-default .navbar-toggle .icon-bar{
                background: #e12119;
            }
            a{
                color: #03a9f4;
            }
            .dropdown-menu{
                -moz-box-shadow: -3px 11px 12px rgba(0,0,0,.175);
                -webkit-box-shadow: -3px 11px 12px rgba(0,0,0,.175);
                box-shadow: -3px 11px 12px rgba(0,0,0,.175);
            }
            .dicone{
                font-size: 60px;
                right:0;
                color: #8c8c8a;
                position: absolute;
                clear: both;
                z-index: -50;
                background: #fff;
            }
            .navbar{
                border-width: 0px;
                border-bottom: 1px solid #fff;
                border-radius: 0px;
                -moz-box-shadow:3px 3px 4px #ddd;
                -webkit-box-shadow:3px 3px 4px #ddd;
                box-shadow:3px 3px 4px #ddd;
            }
            .navbar-nav>li>.dropdown-menu{
                border-top: 1px solid #fff;
                /*margin-right:-1px;*/
            }
            #present_menu{margin-right:-1px;}
            .navbar-default .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:focus, .navbar-default .navbar-nav>.open>a:hover{
                color: #fff;
                background-color: #e5201a;
            }
            #home-nav-tab{
                padding: 25px 0;
                background: #f8f8f8;
            }
            #home-nav-tab .nav-tabs{
                border-bottom: 0px;
            }
            #home-nav-tab .nav-tabs>li a {
                padding-top: 15px;
                padding-bottom: 15px;
                font-size: 17px;
                color: #000000;
                border: 0;
                border-radius: 0;
                text-shadow:-1px -1px 0px rgba(255, 255, 255, 0.61);
            }
            #home-nav-tab .nav-tabs>li,#home-nav-tab .nav-tabs>li.active a,#home-nav-tab .nav-tabs>li.active{
                -moz-border-radius: 3px;
                -webkit-border-radius: 3px;
                border-radius: 3px;
            }
            #home-nav-tab .nav-tabs>li.active a{
                    border: 0px;
                    text-shadow:-1px -1px 0px rgba(0,0,0,.8);
                     background: #e5201a;color: #ffffff;} 
            #home-nav-tab .nav-tabs>li {
                width: 32%;
                background: #fbdf0c;
                /*padding: 10px 30px;*/
                text-align: center;
                margin-right: 1%;

            }
            #home-nav-tab .nav-tabs>li.active a::after{
                display: block;
                content: "";
                position: absolute;
                width: 0px;
                height: 0px;
                top: 100%;
                left: 50%;
                border-left: 0.7rem solid rgba(255, 255, 255, 0);
                border-right: 0.7rem solid rgba(255, 255, 255, 0);
                margin: 0px 0px 0px -0.25rem;
            }
            #home-nav-tab .nav-tabs>li.active a::after{
                    border-top: 0.7rem solid #e5201a;
            }
            #hmenu>li> a{
                padding:0 15px 0 5px;
                border-right: 1px solid #9c9292;
                text-decoration: none;
                color: #f5f5f5;
            }
            #hmenu>li:last-child> a{border-right-width: 0px}
            #hrs>li {
                padding: 0 8px;
                color: #ffffff;
            }
            #hrs>li a {color: #eee;}
            #hrs>li .fa {color: #eee;}

            .navbar-nav > li > a{
                padding-bottom: 25px;
                padding-top: 25px;
            }
            #logo{
                height: auto;
                margin-top: -15px;
                max-height: 70px;
            }



            /*#main{font-family:'Open Sans', 'Segoe UI', serif;line-height: 180%;}*/
            .pad0{padding: 0!important}
            body{
                /*background: url('{{asset("assets/images/app/body_bg2.jpg")}}') #fff  fixed;*/
                background-size: auto 100% 
            }


            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .container{max-width: 1045px}
            #top-img{
                min-height: 80px;
                height: 160px;
                background: url('{{asset("assets/images/app/baniere_LERF.png")}}') #333 0 50% no-repeat;
                background-size: 100% auto;
            } 
            .navbar{min-height: 20px}
            .logo{max-height: 100px;margin-top: 10%;}  
            #main {
                min-height: 650px;
                background: #fff;
                /*margin-bottom: 20px;*/

            }

            #top-link{border-top: 0px solid #2d2d2d;padding: 2px 0}
            #top-link .list-inline{
                padding: 2px 0 ;
                margin-bottom: 2px;


            }
            #top-link .list-inline a{color: #cecece;/*#f9ec3d*/;}
            .mini-rs {
                height: 30px;
                border-radius: 100%;
            }
            #top-link{ 
                background: #ffffff;
                background: #333333;
                border-bottom: 4px solid #e12119;
                font-size: 13px;
            }
            .stylish-input-group {
                /*margin: 5px 0;*/

            }
            .stylish-input-group .input-group-addon{
                background: white !important;
                border-radius: 0;
                border-color:#eee;
            }
            .stylish-input-group .form-control{
                border-right:0; 
                box-shadow:0 0 0; 
                border-color:#eee;
                border-radius: 0;
            }
            .stylish-input-group button{
                border-radius: 0;
                border-color:#eee;
                border:0;
                background:transparent;
            }
            .stylish-input-group button{font-size: 12px}
            .pheader{
                font-weight: 300;
                text-transform: uppercase;
                color: #0ec523;
                font-size: 29px;
                border-bottom: 1px solid #ddd;
                padding-bottom: 10px
            }
            .actu_title{
                font-family:'Open Sans Light','Open Sans',"Segoe UI", 'Roboto Slab','Open Sans',Arial;
                font-size: 20px;
                font-weight: 100;
                margin:10px 0 5px ;
                font-size: 1.28571em;
                padding-top: 5px;
                color: forestgreen;

            }
            a:hover{color: #eb212e}
            a {
                -webkit-transition: color .3s ease;
                -o-transition: color .3s ease;
                transition: color .3s ease;
                color: #33475c;
                color: #4caf50;
                text-decoration: none;
            }
            .actu_line:hover{
                /*box-shadow: 0 2px 4px #ddd inset;*/
                transition: all ease .2s;
            }
            .actu_cover{

            }
            .actu_line{
                transition: all ease .2s;
                border: 1px solid #eee;
                padding: 10px 15px ;
                border-left: 0px;

                border-right: 0px;               
                border-top: 0px;
                /*box-shadow: 0 0 5px #eee*/
                /*height: 240px;*/

            }
            .Rpanel{
                box-shadow: 5px 1px 4px 0px #ddd inset;;
                padding: 0px !important; 
            }
            .hentete{
                font-size: 25px;
                line-height: 1.25;
                color: #333;
                color: #333;
                display: block;
                margin-top: 20px;
                padding: 0px 10px;
                text-transform: uppercase;
                /*border-left: 5px solid #13ef5c;*/ 
                border-left: 0px solid #4caf50;
                /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#b7e2c9+0,ffffff+100&1+0,0+100 */
                background: -moz-linear-gradient(top, rgb(216, 222, 217) 0%, rgba(255,255,255,0) 100%); /* FF3.6-15 */
                background: -webkit-linear-gradient(top, rgb(216, 222, 217) 0%,rgba(255,255,255,0) 100%); /* Chrome10-25,Safari5.1-6 */
                background: linear-gradient(to bottom, rgb(216, 222, 217) 0%,rgba(255,255,255,0) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d8ded9', endColorstr='#00ffffff',GradientType=0 ); /* IE6-9 */
            }
            .lcard{
                padding: 10px 15px;
                /*                border: 1px solid #ddd;
                                border-left: 4px solid #ddd;
                                border-right: 0px solid #ddd;*/
            }
            .scard{
                /*border: 1px solid #ddd;*/
                margin-top: 10px;
                margin-right: 5px;
                /*box-shadow: 0 0 5px #eee;*/
                padding-top: 15px !important;
                border-bottom: 1px solid #ddd
            }
            .scard .form-control{
                background: #fbfbfb;
                border-radius: 0;
                box-shadow: none;
            }
            .hentete h3{
                padding: 5px;margin: 0;font-size: 18px;
                font-weight: 900;
                font-family: 'Roboto Slab','Open Sans',Arial;
                /*min-height: 56px;*/
                color: #000;

            }
            .text-sm.text-muted.clearfix{
                clear: both;
                font-size: 15px;
            }
            .service-block-v4{
                padding: 15px  20px;
                margin: 30px 0;
                /*background: #f5f8f9*/
            }
            .service-desc .icone{
                font-size: 80px;
                display: block;
                text-align: center;
            }
            .service-desc h3{font-weight: bold}
            .cactu_image img.news-image{
                max-width: 200px !important;
                max-height: 200px !important;
                margin-top: 5% !important;
            }
            .dropdown-header{white-space: normal}
        </style>
        <style>
            /*
Code snippet by maridlcrmn for Bootsnipp.com
Follow me on Twitter @maridlcrmn
            */

            .navbar-brand { position: relative; z-index: 2; }

            .navbar-nav.navbar-right .btn { position: relative; z-index: 2; padding: 4px 20px; margin: 10px auto; transition: transform 0.3s; }

            /*.navbar .navbar-collapse { position: relative; overflow: hidden !important; }*/
            .navbar .navbar-collapse .navbar-right > li:last-child { padding-left: 22px; }

            .navbar .nav-collapse { position: absolute; z-index: 1; top: 0; left: 0; right: 0; bottom: 0; margin: 0; padding-right: 120px; padding-left: 80px; width: 100%; }
            .navbar.navbar-default .nav-collapse { background-color: #f8f8f8; }
            .navbar.navbar-inverse .nav-collapse { background-color: #222; }
            .navbar .nav-collapse .navbar-form { border-width: 0; box-shadow: none; }
            .nav-collapse>li { float: right; }

            .btn.btn-circle { border-radius: 50px; }
            .btn.btn-outline { background-color: transparent; }

            .navbar-nav.navbar-right .btn:not(.collapsed) {
                background-color: rgb(111, 84, 153);
                border-color: rgb(111, 84, 153);
                color: rgb(255, 255, 255);
            }

            .navbar.navbar-default .nav-collapse,
            .navbar.navbar-inverse .nav-collapse {
                height: auto !important;
                transition: transform 0.3s;
                transform: translate(0px,-50px);
            }
            .navbar.navbar-default .nav-collapse.in,
            .navbar.navbar-inverse .nav-collapse.in {
                transform: translate(0px,0px);
            }


            @media screen and (max-width: 767px) {
                .navbar .navbar-collapse .navbar-right > li:last-child { padding-left: 15px; padding-right: 15px; } 

                .navbar .nav-collapse { margin: 7.5px auto; padding: 0; }
                .navbar .nav-collapse .navbar-form { margin: 0; }
                .nav-collapse>li { float: none; }

                .navbar.navbar-default .nav-collapse,
                .navbar.navbar-inverse .nav-collapse {
                    transform: translate(-100%,0px);
                }
                .navbar.navbar-default .nav-collapse.in,
                .navbar.navbar-inverse .nav-collapse.in {
                    transform: translate(0px,0px);
                }

                .navbar.navbar-default .nav-collapse.slide-down,
                .navbar.navbar-inverse .nav-collapse.slide-down {
                    transform: translate(0px,-100%);
                }
                .navbar.navbar-default .nav-collapse.in.slide-down,
                .navbar.navbar-inverse .nav-collapse.in.slide-down {
                    transform: translate(0px,0px);
                }
            }

            .dropdown-large {
                position: static !important;
            }
            .dropdown-menu-large {
                margin-left: 16px;
                margin-right: 16px;
                padding: 20px 0px;
            }
            .dropdown-menu-large > li > ul {
                padding: 0;
                margin: 0;
            }
            .dropdown-menu-large > li > ul > li {
                list-style: none;
            }
            .dropdown-menu-large > li > ul > li > a {
                display: block;
                padding: 3px 20px;
                clear: both;
                font-weight: normal;
                line-height: 1.428571429;
                color: #333333;
                white-space: normal;
            }
            .dropdown-menu-large > li ul > li > a:hover,
            .dropdown-menu-large > li ul > li > a:focus {
                text-decoration: none;
                /*color: #262626;*/
                /*background-color: #f5f5f5;*/
            }
            .dropdown-menu-large .disabled > a,
            .dropdown-menu-large .disabled > a:hover,
            .dropdown-menu-large .disabled > a:focus {
                color: #999999;
            }
            .dropdown-menu-large .disabled > a:hover,
            .dropdown-menu-large .disabled > a:focus {
                text-decoration: none;
                background-color: transparent;
                background-image: none;
                filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
                cursor: not-allowed;
            }
            .dropdown-menu-large .dropdown-header {
                color: #ea1710;
                font-size: 16px;
            }

            @media (max-width: 768px) {
                .dropdown-menu-large {
                    margin-left: 0 ;
                    margin-right: 0 ;
                }
                .dropdown-menu-large > li {
                    margin-bottom: 30px;
                }
                .dropdown-menu-large > li:last-child {
                    margin-bottom: 0;
                }
                .dropdown-menu-large .dropdown-header {
                    padding: 3px 15px !important;
                }
            }

        </style>
    </head>
    <body id="body">
        <div id="inner-wrapper" class="container-fluids">
            
            <div id="top-link hidden">
                <div class="container">
                    <div class="row" >
                        <div class="col-sm-7" >
                            <ul class="list-inline hidden-xs " id="hmenu">
                                <li><a href="#"><i class="fa fa-sellsy"></i> Bourses d'études</a></li>
                                <li><a href="#"><i class="fa fa-ambulance"></i> Secours universitaire</a></li>
                                <li><a href="#"><i class="fa fa-cubes"></i> Frais de mémoires</a></li>

                            </ul>
                        </div>
                        <div class="col-sm-5 text-right">
                            <ul class="list-inline text-sm" id="hrs">
                                <li>Suivez-nous sur : </li>
                                <li><i class="fa fa-facebook"></i></li>
                                <li><i class="fa fa-youtube"></i></li>
                                <li><i class="fa fa-twitter"></i></li>
                                <li><i class="fa fa-rss"></i></li>
                                <!--<li class=""><i class="fa fa-phone-square"></i> 96 XX XX XX </li>-->
                            </ul>
                        </div>
                    </div>  
                </div>
            </div> 
            <header class="hidden">
                <nav id="nav" class="navbar navbar-default">
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="{{url('/')}}"><img id="logo" src="{{asset('assets/images/logo_cous_parakou.png')}}"</a>
                        </div>

                        <!-- Co                                                                                       llect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Accueil</a></li>
                                <li class="dropdown open">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Présentation <span class="fa fa-chevron-down"></span></a>
                                    <ul id="present_menu" class="dropdown-menu">
                                        <li><a href="#">Historique</a></li>
                                        <li><a href="#">Attributions, missions</a></li>
                                        <li><a href="#">Conditions générales</a></li>
                                        <li><a href="#">Equipe de direction</a></li>                                        
                                        <li><a href="#">Organigramme</a></li>
                                    </ul>
                                </li>

                                <li class="dropdown dropdown-large">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services <b class="fa fa-chevron-down"></b></a>

                                    <ul class="dropdown-menu dropdown-menu-large row">
                                        <li class="col-sm-3">
                                            <ul>
                                                <li class="dropdown-header text-uppercases rs">Hébergement</li>
                                                <i class="fa fa-building-o pull-right dicone" ></i>

                                                <li><a href="#">Conditions générales</a></li>
                                                <li><a href="#">Coût des résidences</a></li>
                                                <li><a href="#">Pièces constitutives</a></li>

                                                <li class="divider"></li>

                                                <li class="dropdown-header text-uppercases rs">Restauration</li>
                                                <i class="fa fa-cutlery pull-right dicone" ></i>
                                                <li><a href="#">Conditions générales</a></li>
                                                <li><a href="#">Tarifs des repas</a></li>
                                                <li><a href="#">Dispositions particulières</a></li>
                                                <li><a href="#">Menus hebdomadaire</a></li>
                                            </ul>
                                        </li>
                                        <li class="col-sm-3">

                                            <ul>
                                                <li class="dropdown-header text-uppercases rs">Transport</li>
                                                <i class="fa fa-bus pull-right dicone" ></i>
                                                <li><a href="#">Matériels roulant</a></li>
                                                <li><a href="#">Conditions d'attribution</a></li>
                                                <li><a href="#">Gestion du matériel roulant </a></li>
                                                <li><a href="#">Gestion des subventions</a></li>
                                                <li class="divider"></li>
                                                <i class="fa fa-futbol-o pull-right dicone" ></i>
                                                <li class="dropdown-header text-uppercases rs">Activités sportives</li>

                                                <li><a href="#">Objectifs</a></li>
                                                <li><a href="#">Activités proposées</a></li>
                                            </ul>
                                        </li>
                                        <li class="col-sm-3">
                                            <ul>
                                                <li class="dropdown-header text-uppercases rs">Santé et affaires sociales</li>
                                                <i class="fa fa-street-view pull-right dicone" ></i>
                                                <li><a href="#">Prestations offertes dans le domaine de la santé</a></li>
                                                <li><a href="#">Prestations offertes dans le domaine des affaires sociales</a></li>
                                                <li><a href="#">Conditions générales d'une aide</a></li>
                                                <li class="divider"></li>

                                                <li class="dropdown-header text-uppercases rs">Activités artistiques sportives</li>
                                                <i class="fa fa fa-american-sign-language-interpreting pull-right dicone" ></i>
                                                <li><a href="#">Objectifs</a></li>
                                                <li><a href="#">Activités proposées</a></li>
                                            </ul>
                                        </li>
                                        <li class="col-sm-3">
                                            <ul>
                                                <li class="dropdown-header">Allocations Universitaires<br>
                                                    <small class="text-muted text-xs" title="Bourses, Secours, frais d'équippement, de stage, de mémoire et de thèse">Bourses, Secours, frais d'équippement,...</small>
                                                </li>
                                                <i class="fa fa-handshake-o pull-right dicone" ></i>
                                                <li><a href="#">Contexte actuel</a></li>
                                                <li><a href="#">Rôles des acteurs</a></li>
                                                <li><a href="#">Critères d'attributions</a></li>
                                                <li><a href="#">Critères de renouvellement</a></li>
                                                <li><a href="#">Critères de rétablissement</a></li>
                                                <li><a href="#">Consulter les délibérations</a></li>
                                                <li><a href="#">Réclammations</a></li>
                                            </ul>
                                        </li>
                                    </ul>

                                </li>
                                <li><a href="#">Activités</a></li> 
                                <li><a href="#">Administration</a></li>                                                                                                                <li><a href="#">Contact</a></li>
                                <li>
                                    <a class="btn btn-default btn-outline btn-circle collapsed"  data-toggle="collapse" href="#nav-collapse1" aria-expanded="false" aria-controls="nav-collapse1">Categories</a>
                                </li>
                            </ul>
                            <ul class="collapse nav navbar-nav nav-collapse" id="nav-collapse1">
                                <li><a href="#">Web design</a></li>
                                <li><a href="#">Development</a></li>
                                <li><a href="#">Graphic design                                                                                                                                                                               </a></li>
                                <li><a href="#">Print</a></li>
                                <li><a href="#">Motion</a></li>
                                <li><a href="#">Mobile apps</a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container -->
                </nav><!-- /.navbar -->
            </header>
            <div  id="main" >
                @yield('content')
            </div>
            <div class="">
                <div id="footer" class="row " >
                    <div class="">
                        <div class="col-sm-7 bBotp ">
                            <ul class="list-inline  text-uppercase">
                                <li>
                                    <a href="{{url('lerf/objectifs-et-missions')}}">Mission </a>
                                </li>
                                <li><a href="{{url('personnels')}}">Personnel</a></li>
                                <li><a href="{{url('lerf/axes-de-recherche')}}">Axes de recherche</a></li>
                                <li><a href="{{url('seminaires')}}">Séminiares</a></li>
                                <li><a href="{{url('activites')}}">Activités</a></li>
                                <li><a href="{{url('publications')}}">Publications</a></li>
                            </ul>

                        </div>
                        <div class="col-sm-5 bBotp text-right">
                            <ul class="list-inline ">
                                <li>
                                    <a href="mailto:">Email: </a>
                                </li>
                                <li class="hidden-xs">|</li>
                                <li>
                                    <a href="mailto:">Tél: </a>
                                </li>
                                <li class="hidden-xs">|</li>

                                <li>
                                    Réseaux sociaux  
                                </li>
                                <li>
                                    <a href="{{url('login')}}">Connexion</a> 
                                </li>
                                <li>
                                    <a href="{{url('register')}}">Créer un compte</a> 
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-12 text-xs text-center">
                            <br>
                            &copy; LERF-UP 2017, Tous droits réservés

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{asset('assets/js/jquery.min.js')}}"   ></script>
        <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"  ></script>
        <script src="{{asset('assets/plugins/pjax/jquery.pjax.js')}}"></script>
        @yield('script')
    </body>
</html>
