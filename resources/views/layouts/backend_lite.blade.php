<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{asset('assets/css/template-cms.odace.css')}}">
        <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link href="/css/app.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        @yield('css')

        <!-- Scripts -->
        <script>
            window.Laravel = <?php
echo json_encode([
    'csrfToken' => csrf_token(),
]);
?>
        </script>
        <style>
            .form-control{
                box-shadow: none;
                border-radius: 0;
            }
        </style>
    </head>
    <body>

        @if (Session::has('flash_message'))
        <div class="container">
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_message') }}
            </div>
        </div>
        @endif

        @yield('content')

        <div class="f11 text-xs mtop30 text-center">
            &copy; COUS Parakou {{date('Y')}}
        </div>
        <br><br>

        <!-- Scripts -->
        <script src="/js/app.js"></script>
        <script src="{{asset('assets/js/jquery.min.js')}}"   ></script>

        <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"  ></script>
        <script type="text/javascript">
            $(function () {
                // Navigation active
                $('ul.navbar-nav a[href="{{ "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}"]').closest('li').addClass('active');
            });
        </script>
        <script src="{{asset('assets/plugins/pjax/jquery.pjax.js')}}"></script>    
        @yield('script')
    </body>
</html>
