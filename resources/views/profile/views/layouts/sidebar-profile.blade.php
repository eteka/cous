<?php
$r = '/' . Request::path();
$murl = Request::path();
$tburl = explode('/', $murl);
if (count($tburl) > 0) {
    $r = '/' . $tburl[1];
}

//$r = str_ireplace('/', '', Request::path());
?>
<div class="panel panel-default panel-flush">
    <div class="panel-heading">
        <i class="fa fa-cogs"></i>    Gestion du compte
    </div>
    <ul class="list-group rond0">
        <a href="{{url('profile')}}" class="<?= ($r == '/profil') ? 'active' : '' ?> list-group-item ">{{ trans('profileLang::profile.sidebar.account') }}</a>
        <a href="{{url('/profile/information')}}"  class="list-group-item <?= ($r == '/information') ? 'active' : '' ?>">{{ trans('profileLang::profile.sidebar.information') }}</a>
        @if(config()->get('profile.activity'))
        <a href="{{url('/profile/activity')}}" class="list-group-item <?= ($r == '/activity') ? 'active' : '' ?>">{{ trans('profileLang::profile.sidebar.activity') }}</a>
        @endif
        <a href="{{ route('logout') }}"
           class="list-group-item"
           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            {{ trans('profileLang::profile.sidebar.logout') }}
        </a>
    </ul>
</div>
