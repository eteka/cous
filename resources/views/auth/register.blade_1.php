@extends('layouts.backend_lite')

@section('content')
<div class="container bgwhite margin-top-5s0 mt ">
    <div class="row">
        <div class="col-md-4  col-md-offset-4">
            
            <div class="">

                <div class="panel shadow1 panel-default">
                    <div class="text-center bgf7 pad15">
                        <div class="center"> <br><a href="{{url('/')}}">
                                <img alt="LERF-UP" src="{{asset('assets/images/logo.png')}}" class="img-circle img-thumbnail"> </a></div>
                <div class="lock-wrapper">
                    <h4> Bienvenue  !</h4>
                </div>
                <!--<p class="text-center text-muted">Créez votre compte aujourd'hui</p>-->
            </div>
                    <h3 class="panel-headings text-center light col-md-12 ">Création de compte</h3>

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}

                            <div class="form-group {{ $errors->has('nom') ? ' has-error' : ''}}">
                                <!--{!! Form::label('nom', 'Nom: ', ['class' => 'col-md-4  control-label']) !!}-->
                                <div class="col-md-12">
                                    {!! Form::text('nom', null, ['class' => 'form-control text-uppercase','placeholder'=>'Nom', 'required' => 'required']) !!}
                                    {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('prenom') ? ' has-error' : ''}}">
                                <!--{!! Form::label('prenom', 'Prénom: ', ['class' => 'col-md-4 control-label']) !!}-->
                                <div class="col-md-12">
                                    {!! Form::text('prenom', null, ['class' => 'form-control','placeholder'=>'Prénom(s)', 'required' => 'required']) !!}
                                    {!! $errors->first('prenom', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('sexe') ? ' has-error' : ''}}">
                                <!--{!! Form::label('sexe', 'Sexe: ', ['class' => 'col-md-4 control-label']) !!}-->
                                <div class="col-md-12">
                                    {!! Form::select('sexe',[''=>'-Sélectionnez-','H'=>'Homme','F'=>'Femme'], null, ['class' => 'form-control','placeholder'=>'Sexe', 'required' => 'required']) !!}
                                    {!! $errors->first('sexe', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('telephone') ? ' has-error' : ''}}">
                                <!--{!! Form::label('telephone', 'Téléphone: ', ['class' => 'col-md-4 control-label']) !!}-->
                                <div class="col-md-12">
                                    {!! Form::text('telephone', null, ['class' => 'form-control', 'placeholder'=>'Téléphone','required' => 'required']) !!}
                                    {!! $errors->first('telephone', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : ''}}">
                                <!--{!! Form::label('email', 'Email: ', ['class' => 'col-md-4 control-label']) !!}-->
                                <div class="col-md-12">
                                    {!! Form::email('email', null, ['class' => 'form-control','placeholder'=>'Email (Courier électronique)', 'required' => 'required']) !!}
                                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('password') ? ' has-error' : ''}}">
                                <!--{!! Form::label('password', 'Mot de passe: ', ['class' => 'col-md-4 control-label']) !!}-->
                                <div class="col-md-12">
                                    {!! Form::password('password', ['class' => 'form-control','placeholder'=>'Mot de passe', 'required' => 'required']) !!}
                                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <!--<label for="password-confirm" class="col-md-4 control-label">Confirmation de mot de passe:</label>-->

                                <div class="col-md-12">
                                    {!! Form::password('password', ['class' => 'form-control','placeholder'=>'Confimation du mot de passe', 'required' => 'required']) !!}
                                    {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-12 col-md-offset-0">
                                    <button type="submit" class="btn  btn-success btn-block rond0">
                                        Creér mon compte
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div>
                    <div class="padd-all text-center">ou<br><br></div>
                    <a class="btn btn-primary btn-block" href="{{url('login')}}">Connectez vous</a>
                    <br><br>
                    <div class="f11 text-xs text-center">
                        &copy;LERF Parakou {{date('Y')}}
                    </div>
                    <br><br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
