@extends('layouts.web')

@section('content')
<style>
    body{
        background: #fff;
    }
    *, *:before, *:after {
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
    }

    body {
        font-family: 'Nunito', sans-serif;
        color: #384047;
    }

    #main-login-form {
        /*max-width: 300px;*/
        /*margin: 10px auto;*/
        /*padding: 10px 20px;*/
        background: #f4f7f8;
        /*border-radius: 8px;*/
    }

    h1 {
        margin: 0 0 30px 0;
        text-align: center;
    }

    input[type="text"],
    input[type="password"],
    input[type="date"],
    input[type="datetime"],
    input[type="email"],
    input[type="number"],
    input[type="search"],
    input[type="tel"],
    input[type="time"],
    input[type="url"],
    textarea,
    select,.form-control {
        background: rgba(255,255,255,0.1);
        border: none;
        font-size: 16px;
        height: auto;
        margin: 0;
        outline: 0;
        padding: 15px;
        width: 100%;
        background-color: #e8eeef;
        color: #8a97a0;
        box-shadow: 0 1px 0 rgba(0,0,0,0.03) inset;
        margin-bottom: 30px;
    }
    .form-control:focus{
        background: #ffffff;
        box-shadow: none;
        border: 1px solid #dddddd;
    }
    input[type="radio"],
    input[type="checkbox"] {
        margin: 0 4px 8px 0;
    }

    select {
        padding: 6px;
        height: 32px;
        border-radius: 2px;
    }

    button {
        padding: 19px 39px 18px 39px;
        color: #FFF;
        background-color: #4bc970;
        font-size: 18px;
        text-align: center;
        font-style: normal;
        border-radius: 5px;
        width: 100%;
        border: 1px solid #3ac162;
        border-width: 1px 1px 3px;
        box-shadow: 0 -1px 0 rgba(255,255,255,0.1) inset;
        margin-bottom: 10px;
    }

    fieldset {
        margin-bottom: 30px;
        border: none;
    }

    legend {
        font-size: 1.4em;
        margin-bottom: 10px;
    }

    label {
        display: block;
        margin-bottom: 8px;
    }

    label.light {
        font-weight: 300;
        display: inline;
    }

    .number {
        background-color: #5fcf80;
        color: #fff;
        height: 30px;
        width: 30px;
        display: inline-block;
        font-size: 0.8em;
        margin-right: 4px;
        line-height: 30px;
        text-align: center;
        text-shadow: 0 1px 0 rgba(255,255,255,0.2);
        border-radius: 100%;
    }

    @media screen and (min-width: 480px) {

        form {
            max-width: 480px;
        }

    }
</style>
<div class="container bgwhite margin-top-5s0 mt ">
    <div class="row">
        <div class="col-md-8 " id="main-login-form">
            <h3 class="light  page-header up-page-header">Création de compte</h3>
            <div class="">

                <div class="panel_  shadow1__ panel-default">
                    <!--                    <div class="text-center  pad15">
                                            <div class="center"> <br><a href="{{url('/')}}">
                                                    <img alt="LERF-UP" src="{{asset('assets/images/logo.png')}}" class="img-circle_ img-thumbnail"> </a></div>
                                    
                                    <p class="text-center text-muted">Créez votre compte aujourd'hui</p>
                                </div>-->

                    <div class="panel-body">
                        <form id="form" class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}

                            <div class="form-group {{ $errors->has('nom') ? ' has-error' : ''}}">
                                <!--{!! Form::label('nom', 'Nom: ', ['class' => 'col-md-4  control-label']) !!}-->
                                <div class="col-md-12">
                                    {!! Form::text('nom', null, ['class' => 'form-control text-uppercases','placeholder'=>'Votre nom de famille', 'required' => 'required']) !!}
                                    {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('prenom') ? ' has-error' : ''}}">
                                <!--{!! Form::label('prenom', 'Prénom: ', ['class' => 'col-md-4 control-label']) !!}-->
                                <div class="col-md-12">
                                    {!! Form::text('prenom', null, ['class' => 'form-control','placeholder'=>'Prénom(s)', 'required' => 'required']) !!}
                                    {!! $errors->first('prenom', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('sexe') ? ' has-error' : ''}}">
                                <!--{!! Form::label('sexe', 'Sexe: ', ['class' => 'col-md-4 control-label']) !!}-->
                                <div class="col-md-12">
                                    {!! Form::select('sexe',[''=>'-Sélectionnez-','H'=>'Homme','F'=>'Femme'], null, ['class' => 'form-control','placeholder'=>'Sexe', 'required' => 'required']) !!}
                                    {!! $errors->first('sexe', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('telephone') ? ' has-error' : ''}}">
                                <!--{!! Form::label('telephone', 'Téléphone: ', ['class' => 'col-md-4 control-label']) !!}-->
                                <div class="col-md-12">
                                    {!! Form::text('telephone', null, ['class' => 'form-control', 'placeholder'=>'Téléphone','required' => 'required']) !!}
                                    {!! $errors->first('telephone', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : ''}}">
                                <!--{!! Form::label('email', 'Email: ', ['class' => 'col-md-4 control-label']) !!}-->
                                <div class="col-md-12">
                                    {!! Form::email('email', null, ['class' => 'form-control','placeholder'=>'Email (Courier électronique)', 'required' => 'required']) !!}
                                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('password') ? ' has-error' : ''}}">
                                <!--{!! Form::label('password', 'Mot de passe: ', ['class' => 'col-md-4 control-label']) !!}-->
                                <div class="col-md-12 ">
                                    {!! Form::password('password', ['class' => 'form-control','placeholder'=>'Mot de passe', 'required' => 'required']) !!}
                                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <!--<label for="password-confirm" class="col-md-4 control-label">Confirmation de mot de passe:</label>-->

                                <div class="col-md-12">
                                    {!! Form::password('password_confirmation', ['class' => 'form-control','placeholder'=>'Confimation du mot de passe', 'required' => 'required']) !!}
                                    {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-12 col-md-offset-0">
                                    <button type="submit" class="btn btn-lg  btn-primary btn-block rond0">
                                        Creér mon compte
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
         <div class="col-lg-4 col-sm-4   ">
            @include("partials.right");
        </div>
    </div>
</div>
@endsection
